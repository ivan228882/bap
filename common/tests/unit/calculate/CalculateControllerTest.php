<?php

namespace common\tests\unit\calculate;

use Yii;
use common\models\LoginForm;
use api\modules\v1\controllers\CalculateController;
use common\fixtures\UserFixture as UserFixture;
use common\components\Helpers;
use common\components\calculate\MinChange;
use common\components\calculate\MinFee;
use common\components\calculate\MinPiece;
use common\components\calculate\BigPiece;

/**
 * Login form test
 */
class CalculateControllerTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
     public function methodSumm($testArrOut,$testSumm,$error) 
    {

        if ($testArrOut['sums'] !== $testSumm || $testArrOut['sums'] < $testArrOut['sum']) {
            $error['sums'] = 'Summ its bad';
        } 

        return $error;
    }

    public function methodSumsWithFee($testArrOut,$testSumm,$error) 
    {

        if ($testArrOut['sumsWithFee'] !== round($testSumm+$testArrOut['fees'],8)) {
            $error['sumsWithFee'] = 'sumsWithFee its bad';
        } 

        return $error;
    }

    public function methodSurrenderr($testArrOut,$error) 
    {

        $t['surr'] = round(($testArrOut['sums'] - $testArrOut['sum']),8);
        if ($testArrOut['sums'] < $testArrOut['sum'] || $testArrOut['surrender'] !== $t['surr']) {
            $error['surrender'] = 'surrender its bad';
        }

        return $error;
    }

     public function methodSurrenderrWithoutFee($testArrOut,$error) 
     {

        if ($testArrOut['surrenderWithoutFee'] < 0 || $testArrOut['surrenderWithoutFee'] !== round(($testArrOut['surrender'] - $testArrOut['fees']),8) ) {
            $error['surrenderWithoutFee'] = 'surrender its bad';
        } 

        return $error;
    }

    public function goTesting($testArrOut) 
    {

       $error = false;

       $testSumm = 0;
       foreach ($testArrOut['ins'] as $key => $value) {
           $testSumm += $value->amount;
        }
        $testSumm = round($testSumm,8);

       $error = $this->methodSumm($testArrOut,$testSumm,$error);
       $error = $this->methodSumsWithFee($testArrOut,$testSumm,$error);
       $error = $this->methodSurrenderr($testArrOut,$error);
       $error = $this->methodSurrenderrWithoutFee($testArrOut,$error);
       
       return $error;
    }

    public function testMinFee()
    {

        $outAmount = 1;
        $minChange = new MinFee( $outAmount );
        $testArrOut = $minChange->calculate();
        
        $error = $this->goTesting($testArrOut);
        if(!$error) {
            $test = true;
        } else {
            $test = false;
        }

        return $this->assertTrue($test);
    }

     public function testMinPiece()
    {

        $outAmount = 1;
        $minChange = new MinPiece( $outAmount );
        $testArrOut = $minChange->calculate();
        
        $error = $this->goTesting($testArrOut);
        if(!$error) {
            $test = true;
        } else {
            $test = false;
        }

        return $this->assertTrue($test);
    }

     public function testBigPiece()
    {   
        
        $outAmount = 1;

        $minChange = new BigPiece( $outAmount );

        $testArrOut = $minChange->calculate();
        
        $error = $this->goTesting($testArrOut);
        if(!$error) {
            $test = true;
        } else {
            $test = false;
        }

        //return $this->assertTrue($test);
    }

     public function testMinChange()
    {

        $outAmount = 1;
        $minChange = new MinChange( $outAmount );
        $testArrOut = $minChange->calculate();
        
        $error = $this->goTesting($testArrOut);
        if(!$error) {
            $test = true;
        } else {
            $test = false;
        }

        return $this->assertTrue($test);
    }

    
}
