    function l(data) {
        console.log(data); 
    }

    function isFunction(functionToCheck)  {
        var getType = {};
        return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
    }

    var course = {
        interval: 6,
        current: 0,
        old: 0,
        direction: 'none',
        selector: "",
        colorDirection: "#676A6D",
        colorDirectionDef:"#676A6D",
        status: {
            drawTemplate: false,
            refreshCourse: false,
            getBicoinCourse: false,
        },
        setBicoinCourse: function(callback) {

            var that = this;
            var usd;
            $.ajax({
                url: 'https://api.coinmarketcap.com/v1/ticker/?limit=1',             // указываем URL и
                dataType : "json",                     // тип загружаемых данных
                async: false,//норм тип
                success: function (data, textStatus) { // вешаем свой обработчик на функцию success
                    usd = parseFloat(data[0].price_usd);
                    if (textStatus === 'success' && typeof usd === "number") {
                        that.current = usd;
                    }

                }               
            });
        },
        getBicoinCoursePHP: function(callback) {

            var that = this;
            var usd;
            $.ajax({
                url: 'https://api.coinmarketcap.com/v1/ticker/?limit=1',             // указываем URL и
                dataType : "json",                     // тип загружаемых данных
                async: false,//норм тип
                success: function (data, textStatus) { // вешаем свой обработчик на функцию success
                    usd = parseFloat(data[0].price_usd);
                    if (textStatus === 'success' && typeof usd === "number") {
                        this.current = usd;
                        that.status.getBicoinCourse = true;
                    }

                }               
            });

            if(isFunction(callback)&&this.status.getBicoinCourse) {
                callback();
            }

            return usd;
        },


        saveCourse: function(course) {
            localStorage.setItem('course', this.current);
            localStorage.setItem('direction', this.direction);
        },

        setOldCourse: function () {
            if (localStorage.course !== undefined) {
                this.old = localStorage.course;
                return true;
            } else {
                this.old = false;
                return false;
            }
        },

        setDirectionOfCourse: function () {

            if (this.old != false) {
                if (this.old < this.current) {
                    this.direction = 'up';
                    this.colorDirection = "#00AAFF";
                } 
                if (this.old > this.current) {
                    this.direction = 'down';
                    this.colorDirection = "#FF00FF";
                }
                if (this.old == this.current) {
                    if (localStorage.direction !== undefined) {
                        this.direction = localStorage.direction;
                    } 
                    this.colorDirection = "#676A6D";
                }

            }
        },
        setData: function() {

            this.setBicoinCourse();
            this.setOldCourse();
            this.setDirectionOfCourse();
            this.saveCourse();
        },
        drawTemplate: function() {
   
            var template = {};
            template.up = '<span class="glyphicon glyphicon-upload" aria-hidden="true"></span>';
            template.down = '<span class="glyphicon glyphicon-download" aria-hidden="true"></span>';
            template.none = '<span class="glyphicon glyphicon-download" aria-hidden="true"></span>';

            if (this.direction != 'none') {
              template.icon = template[this.direction];  
            } else {
               template.icon = '<span class="glyphicon glyphicon-pause" aria-hidden="true"></span>' ;
            }

            template.final = this.current+' '+template.icon;

            if (typeof template.final === 'string') {
                this.status.drawTemplate = true;
                return template.final;
            }
        },
        blums: function() {
            //мерцание 
            var that = this;
            setTimeout(function(){
                $(that.selector).find('span').css('color',that.colorDirection);

            },0);
            setTimeout(function(){
                $(that.selector).find('span').css('color',that.colorDirectionDef);
            },1500);
        },
        refreshCoursePHP: function(callback) {

            this.setData();//set and get data for resfresh
            this.drawTemplate();

            if(this.status.drawTemplate) {

                if($('*').is(this.selector)) {  
                    $(this.selector).html(this.drawTemplate());
                    this.blums();
                    this.status.refreshCourse = true;
                }

                if(isFunction(callback)&&this.status.refreshCourse) {
                    callback();
                }
            }

        },
        packToRun: function() {

            this.setData();
            this.drawTemplate();
            if($('*').is(this.selector)) {
                 $(this.selector).html(this.drawTemplate());
                 this.blums();
                 console.log('success');
            } else {
                l('selector no founded!');
            }
        },
        firstRun :function(i) {
            this.packToRun();  
        },
        run: function() {
            this.firstRun();
            var sec = parseFloat(this.interval);
            setInterval(this.packToRun.bind(this),sec*1000);
        },
    }
    //function with PHP has callback