<?php
namespace common\widgets;

use Yii;
use yii\base\Widget;
use frontend\assets\CourseAsset;
use yii\helpers\Html;

class BitcoinCourse extends Widget
{
    public $selector = "";//str
    public $interval = 6;//int

    public function init()
    {
        parent::init();
    }

    public function run()
    {	

    	$errors = false;

    	if ($this->interval < 6 && !is_integer($this->interval)) {
    		$errors[] = "Не корретный интервал";
    	} 
    	if ($this->selector == "") {
    		$errors[] = "Selector не указан";
    	}
    	if (is_array($errors)) {
    		return $errors[0];
    	}

    	$str = "course.selector = '". $this->selector ."';";
    	$str .= "course.interval = ". $this->interval .";";
    	$str .= "course.run();";

    	CourseAsset::register($this->view);
    	$this->view->registerJs($str);
    }
}
