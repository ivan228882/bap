<?php

namespace common\components;

use Yii;

use stdClass;

class Transaction
{

	protected $transaction;

	public function __construct(stdClass $transaction)
	{
		$this->transaction = $transaction;
	}

	public function getInputs()
	{
		return $this->transaction->vin;
	}

	public function getInputsInfo()
	{
		$info = [];
		$amounts = [];
		$addresses = [];
		$inputs = $this->getInputs();
		foreach ($inputs as $i => $input) {
			$sum = 0;
			$addrs = [];

			$command = new \Nbobtc\Command\Command('getrawtransaction', [$input->txid, true]);
			$response = Yii::$app->bitcoinClient->sendCommand($command);
			$getrawtransaction = json_decode($response->getBody()->getContents());

			if ($getrawtransaction->result) {
				$info[$i] = $getrawtransaction->result;
				
				if (isset($getrawtransaction->result->vout)) {
					foreach ($getrawtransaction->result->vout as $vout) {
						if ($vout->n == $input->vout) {
							$sum += $vout->value;
							if (isset($vout->scriptPubKey)) {
								foreach ($vout->scriptPubKey->addresses as $address) {
									$addrs[] = $address;
								}
							}
						}
					}
				}
				
				$amounts[$i] = $sum;
				$addresses[$i] = $addrs;
			}
		}
		return compact('info', 'amounts', 'addresses');
	}

	public function getOutputs()
	{
		return $this->transaction->vout;
	}

	public function getSize()
	{
		return $this->transaction->size;
	}

	public function getVSize()
	{
		return $this->transaction->vsize;
	}

	public function getFee()
	{
		$transaction = $this->transaction;

		$sumInputs = 0;
		$sumOutputs = 0;

		$inputs = $this->getInputs();
		$outputs = $this->getOutputs();

		foreach ($inputs as $input) {
			$sum = 0;

			$command = new \Nbobtc\Command\Command('getrawtransaction', [$input->txid, true]);
			$response = Yii::$app->bitcoinClient->sendCommand($command);
			$getrawtransaction = json_decode($response->getBody()->getContents());

			if (isset($getrawtransaction->result) and !empty($getrawtransaction->result)) {
				foreach ($getrawtransaction->result->vout as $vout) {
					if ($vout->n == $input->vout) {
						$sum += $vout->value;
					}
				}
			}
			$sumInputs += $sum;
		}

		foreach ($outputs as $output) {
			$sumOutputs += $output->value;
		}

		return [
			'sumInputs' => $sumInputs,
			'sumOutputs' => $sumOutputs,
			'fee' => round($sumInputs - $sumOutputs, 8),
		];
	}

}