<?php

namespace common\components;

class RecommendFee
{

	public static $data = [];

	public static function fetch($is_return = false)
	{
		if (empty(static::$data)) {
			$ch = curl_init('https://bitaps.com/api/fee');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
			curl_setopt($ch, CURLOPT_TIMEOUT, 3);
			$data = curl_exec($ch);
			if ($data) {
				$respond = json_decode($data, true);
				if ($is_return) {
					return $respond;
				}
				static::$data = $respond;
			}
		}
	}

	public static function getHigh()
	{
		static::fetch();
		return @static::$data['high'];
	}

	public static function getMedium()
	{
		static::fetch();
		return @static::$data['medium'];
	}

	public static function getLow()
	{
		static::fetch();
		return @static::$data['low'];
	}

}