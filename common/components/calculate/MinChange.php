<?php

namespace common\components\calculate;

use Yii;
use common\components\Helpers;
use common\components\Size;
use common\components\RecommendFee;

class MinChange extends AbstractCalculate
{

	public $sum;
	public $outs;

	public function __construct(float $sum, int $outs = 0)
	{
		$this->sum = $sum;
		$this->outs = $outs;
	}

	public function calculate()
	{
		$unspent = $this->getListUnspent();

		if (isset($unspent->result) and $unspent->result !== null) {

			$ins = $unspent->result;

			/* Сортировка массива входов по балансу start */
			uasort($ins, function ($a, $b) {
				if ($a->amount == $b->amount) {
					return 0;
				}
				return ($a->amount < $b->amount) ? -1 : 1;
			});
			$keys = array_keys($ins);
			if (isset($ins[$keys[key($keys)]])) {
				$minIn = $ins[key($ins)]->amount;
			}
			end($keys); // Перебрасываем массив на конец
			if (isset($ins[$keys[key($keys)]])) { // И получаем его ключ, то-есть ключ последнего элемента, это и есть максимальная сумма
				$maxIn = $ins[$keys[key($keys)]]->amount;
			}

			$sum = 0;
			$sums = [];
			$sumsWithFee = [];
			$fees = [];
			$array = [];

			$i = 1;//var_dump($ins);
			while ($i <= 10) { // 10 вариаций массива, после выберем найменьшую сумму
				$sums[$i] = 0;
				$array[$i] = []; // Массив варианта
				foreach ($ins as $index => $in) {

					if ($sum < $this->sum) { // Если сумма варианта меньше нужной суммы
						if (!isset($array[$i][$index])) { // Если такой вход(с таким индексом) не существует в массиве варианта
							$continue = 0;
							$array[$i][$index] = $in; // Прибавляем в массив варианта вход под его индексом
							$sum += $in->amount; // Прибавляем к сумме баланс входа (чисто для прохода массива 1 раз)
			
						
							$sums[$i] += $in->amount; // Прибавляем к сумме варианта баланс входа

						} else {
							continue; // Иначе скачем дальше и ищем тот, который не найден в массиве варианта
						}
					} else { // Иначе выходим, чтобы лишний раз не гонять цикл по всем входам, т.к. мы уже нашли всё что нам нужно
						break;
					}
				}
				$sum = 0;
				$ins = Helpers::shuffleSaveKeys($ins); // Перемешиваем массив входов с сохранением ключей
				$i++;
			}

			foreach ($sums as $i => $sum) {
				if (isset($array[$i]) and !empty($array[$i])) {
					$inputs = count($array[$i]);
					$size = Size::calculate($inputs, $this->outs);
					$fee = (($size['max'] * RecommendFee::getMedium()) / 100000000);
					$sumsWithFee[$i] = $sum + $fee;
					$fees[$i] = $fee;
				}
			}
			
			return [
				'ins' => $ins,
				'array' => $array,
				'sums' => $sums,
				'sumsWithFee' => $sumsWithFee,
				'fees' => $fees,
				'min' => min($sumsWithFee),
				'sum' => $this->sum
			];


		} else {
			//throw new \Exception($unspent->error);
		}
	}

	public function calculate_2()
	{
		/**Минимум сдачи*/
		
		if (Yii::$app->request->isAjax) {
			$post = Yii::$app->request->post();
			echo json_encode($post['1']);
		}

		$unspent = $this->getListUnspent();

		if ( isset($unspent->result) and $unspent->result !== null ) {

			$ins = $unspent->result;
			$ins = $this->getAscSortArr($ins);
			$minIn = $this->getMinElemStart($ins);
			$maxIn = $this->getMaxElemStart($ins);

			$sum = 0;
			$sums = [];
			$sumsWithFee = [];
			$fees = [];
			$array = [];
			
			/*$obj = new ArrayObject($ins);
			$copyBusket = $obj->getArrayCopy();*/
			//не отработало (выяснить почему)

			$insClone = $ins;

			foreach ($insClone as $index => $in) {

				if ($sum < $this->sum) { // Если сумма варианта меньше нужной суммы

					$diff = $this->sum - $sum;

					if($diff>$maxIn) {

						$result = $this->getMaxElem($insClone);
						if (!isset($array[$result['id']])) {
							$array[$index] = $in;
							$sum += $result['max']; 
						}
						unset( $insClone[$result['id']] );

					} elseif($diff<$minIn) {

						$result = $this->getMinElem($insClone);
						if (!isset($array[$result['id']])) {
							$array[$index] = $in;
							$sum += $result['min']; 
						} 
						unset( $insClone[$result['id']] );
						break 1;

					} elseif($diff>$minIn) {
							
						$result = $this->getMinDiff($insClone,$diff);
						if (!isset($array[$result['id']])) { 
							$array[$result['id']] = $insClone[$result['id']]; 
							$sum += $insClone[$result['id']]->amount;
						} 
						unset( $insClone[$result['id']] );

					}
						
				} else { // Иначе выходим, чтобы лишний раз не гонять цикл по всем входам, т.к. мы уже нашли всё что нам нужно
					break;
				}
			}

			if (isset($array) and !empty($array)) {
				$inputs = count($array);
				$size = Size::calculate($inputs, $this->outs);
				$fee = (($size['max'] * RecommendFee::getMedium()) / 100000000);
				$sumsWithFee = $sum + $fee;
			}

			/**
				* Массив в результате перебора
				* 
				* @var $ins - финальный массив входов
				* @var $sumsWithFee - массив - сумма с коммисией
				* @var $fees - сдача
				* @var $min - минимальный элемент из $sumsWithFee
				* @var $this->sum - необходимая сумма
				* 
				* @return array
			*/
			return [
				'ins' => $insClone,
				'sums' => $sum,
				'sumsWithFee' => $sumsWithFee,
				'fees' => $fee,
				'sum' => $this->sum
			];

		} else {
			throw new \Exception($unspent->error);
		}
		
	}
}