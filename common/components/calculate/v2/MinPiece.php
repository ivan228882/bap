<?php

namespace common\components\calculate\v2;

use Yii;

class MinPiece extends AbstractCalculate
{

	public $sum;
	public $fee;

	public function __construct(float $sum, float $fee = 0)
	{
		$this->sum = $sum;
		$this->fee = $fee;
	}

	public function calculate()
	{
		$unspent = $this->getListUnspent();

		if (isset($unspent->result) and $unspent->result !== null) {

			$inputs = $unspent->result;
			$inputs = $this->clearOfUnconfirmed($inputs);
			$inputs = $this->getSortArrayAsc($inputs);

			$sum = 0;
			$array = [];

			foreach ($inputs as $index => $in) {

				if ($sum < ($this->sum + $this->fee + 0.0001)) {
						
					if (!isset($array[$index])) {
						$array[(int)$index] = $in;
						$sum += $in->amount;
					}
						
				} else {
					break;
				}

			}

			$this->cleanArray($array);
			
			return $this->getResponse($array);

		} else {
			throw new \Exception($unspent->error);
		}
		
	}

	private function cleanArray(&$array)
	{
		$newArray = $array;
		$newArray = $this->getSortArrayDesc($newArray);
		foreach ($newArray as $index => $in) {
			$sum = $this->getArraySum($array);
			if (($sum - $in->amount) > ($this->sum + $this->fee)) {
				if (isset($array[$index]))
					unset($array[$index]);
			}
		}
	}

}