<?php

namespace common\components\calculate\v2;

use Yii;

use common\models\Setting;

abstract class AbstractCalculate
{

	public function getListUnspent(int $minconf = 0, int $maxconf = 9999999, string $addresses = '')
	{
		$args = [$minconf, $maxconf];

		if (!empty($addresses)) {
			array_push($args, json_decode($addresses));
		}

		$command = new \Nbobtc\Command\Command('listunspent', $args);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		if (!empty($response))
			return json_decode($response->getBody()->getContents());
		else
			return [];
	}

	public function clearOfUnconfirmed($array)
	{
		$addressSmallChange = Setting::getValue('address_small_change');
		$addressBigChange = Setting::getValue('address_big_change');

		foreach ($array as $index => $in) {
			if ($in->confirmations == 0) {
				if ($in->address != $addressSmallChange and $in->address != $addressBigChange) {
					unset($array[$index]);
				}
			}
		}

		return $array;
	}

	public function getArraySum($array)
	{
		$sum = 0;
		if (isset($array) && is_array($array)) {
			foreach ($array as $key => $value) {
				$sum += $value->amount;
			}
		} else {
			return false;
		}
		return $sum;
	}

	public function getSortArrayAsc(array $array) : array
	{
		uasort($array, function ($a, $b) {
			if ($a->amount == $b->amount) {
				return 0;
			}
			return ($a->amount < $b->amount) ? -1 : 1;
		});

		return $array;
	}

	public function getSortArrayDesc(array $array) : array
	{
		uasort($array, function ($a, $b) {
			if ($a->amount == $b->amount) {
				return 0;
			}
			return ($a->amount < $b->amount) ? 1 : -1;
		});

		return $array;
	}

	public function getMinElement(array $array) : array
	{
		$keys = array_keys($array);
		$min = $array[$keys[0]];
		return ['amount' => $min->amount, 'id' => $keys[0]];
	}

	public function getMaxElement(array $array) : array
	{
		$keys = array_keys($array);
		$max = $array[end($keys)];
		return ['amount' => $max->amount, 'id' => end($keys)];
	}

	public function getSurrender($in, $out)
	{
		return $out - $in;
	}

	public function surrenderWithoutFee($surrender, $fee)
	{
		return $surrender - $fee;
	}

	abstract public function calculate();

	protected function getResponse($array)
	{
		$sum = $this->getArraySum($array);
		$sumWithoutFee = $sum - $this->fee;
		$surrender = $this->getSurrender($this->sum, $sum);
		$surrenderWithoutFee = $this->surrenderWithoutFee($surrender, $this->fee);

		return [
			'ins' => $array,
			'countIns' => count($array),
			'sum' => number_format($sum, 8),
			'sumWithoutFee' => number_format($sumWithoutFee, 8),
			'surrender' => number_format($surrender, 8),
			'surrenderWithoutFee' => number_format($surrenderWithoutFee, 8),
		];
	}

}