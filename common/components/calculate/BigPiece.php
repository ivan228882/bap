<?php
namespace common\components\calculate;

use Yii;
use common\components\Helpers;
use common\components\Size;
use common\components\RecommendFee;

class BigPiece extends AbstractCalculate
{

	public $sum;
	public $outs;

	public function __construct(float $sum, int $outs = 0)
	{
		$this->sum = $sum;
		$this->outs = $outs;
	}

	public function calculate()
	{
		/** Большие куски */

		$unspent = $this->getListUnspent();

		if ( isset($unspent->result) and $unspent->result !== null ) {

			$ins = $unspent->result;

			$ins = $this->getAscSortArr($ins);

			$sum = 0;
			$sums = 0;
			$sumsWithFee = 0;
			$fees = 0;
			$array = [];
			
			$insClone = $ins;
			$sumArr = $this->getSummArr($insClone);
			if(!$this->testGo($this->sum,$sumArr)) {
				return $error = '#1'; //сумма  выше суммы имеющихся
			}

			foreach ($insClone as $index => $in) {

				if ($sum < $this->sum) { // Если сумма варианта меньше нужной суммы
					
					$result = $this->getMaxElem($insClone);

					if (!isset( $array[$result['id']] ) ) {
						$array[$result['id']] = $insClone[$result['id']];
						$sum += $result['max'];
					}
					unset( $insClone[$result['id']] ); 
					
				} else { // Иначе выходим, чтобы лишний раз не гонять цикл по всем входам, т.к. мы уже нашли всё что нам нужно
					break;
				}
			}

			if (isset($array) and !empty($array)) {
				$inputs = count($array);
				$size = Size::calculate($inputs, $this->outs);
				$fee = (($size['max'] * RecommendFee::getMedium()) / 100000000);
				$sumsWithFee = $sum + $fee;
			}

			for ($i=0; $i < 5; $i++) { 

				$surrender = $this->getSurrender($sum,$this->sum);

				if ($this->surrenderWithoutFee($surrender, $fee) < 0) {
					$result = $this->getMinDiff($insClone,$surrender);
					if (!isset($array[$result['id']])) { 
						$array[$result['id']] = $insClone[$result['id']]; 
						$sum += $insClone[$result['id']]->amount;
					} 
					unset( $insClone[$result['id']] );
			
					if (isset($array) and !empty($array)) {
						$inputs = count($array);
						$size = Size::calculate($inputs, $this->outs);
						$fee = (($size['max'] * RecommendFee::getMedium()) / 100000000);
						$sumsWithFee = $sum + $fee;
					}
				}

			}

			/**
				* Массив в результате перебора
				* 
				* @var $ins - финальный массив входов
				* @var $sumsWithFee - массив - сумма с коммисией
				* @var $fees - комиссия
				* @var $surrender - сдача
				* @var $surrenderWithoutFee - сдача без коммисии
				* @var $min - минимальный элемент из $sumsWithFee
				* @var $this->sum - необходимая сумма
				* 
				* @return array
			*/
			return [
				'ins' => $array,
				'sums' => round($sum,8),
				'sumsWithFee' => round($sumsWithFee,8),
				'fees' => round($fee,8),
				'surrender' => round($surrender,8),
				'surrenderWithoutFee' => round($this->surrenderWithoutFee($surrender, $fee),8),
				'sum' => round($this->sum,8)
			];

		} else {
			throw new \Exception($unspent->error);
		}

	}

}