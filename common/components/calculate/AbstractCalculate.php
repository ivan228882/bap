<?php

namespace common\components\calculate;

use Yii;

abstract class AbstractCalculate
{

	public function testGo($need, $has) 
	{

		return $has>$need;
	}

	public function getSummArr($arr) {

		$sum = 0;
		if(isset($arr)&&is_array($arr)) {
			foreach ($arr as $key => $value) {
				$sum += $value->amount;
			}
		} else {
			return false;
		}
		return $sum;
	}

	public function getListUnspent(int $minconf = 1, int $maxconf = 9999999, string $addresses = '')
	{
		$args = [$minconf, $maxconf];

		if (!empty($addresses)) {
			array_push($args, json_decode($addresses));
		}

		$command = new \Nbobtc\Command\Command('listunspent', $args);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	public function getAscSortArr(array $arr) : array
	{
		uasort($arr, function ($a, $b) {
				if ($a->amount == $b->amount) {
					return 0;
				}
				return ($a->amount < $b->amount) ? -1 : 1;
		});

		return $arr;
	}

	public function getMinElem(array $arr) : array
	{
		$keys = array_keys($arr);
		$min = $arr[$keys[0]];
		return ['min' => $min->amount, 'id' => $keys[0]];
	}

	public function getMaxElem(array $arr) : array
	{
		$keys = array_keys($arr);
		$max = $arr[end($keys)];

		return ['max' => $max->amount, 'id' => end($keys)];
	}

	public function getMinElemStart(array $arr)
	{
		$keys = array_keys($arr);
		$min = $arr[$keys[0]];
		return $min->amount;
	}

	public function getMaxElemStart(array $arr)
	{
		$keys = array_keys($arr);
		$max = $arr[end($keys)];
		return $max->amount;
	}

	public function getMinDiff($arr, $diff)
	{
		
		/**
			* Получение оптимального элемента для заполнения выборки
			*
			* Выбираем Элемент с минимальной разницей сумм.
			* если >= 0 тогда выборка элемента что меньше либо равный
			* разнице сумм $diff (старт от мин к макс)
			* иначе выборка элемента что больше $diff (старт от мин к макс)
			* 
			* @param $diff - разница между имеющейся и нужной суммой amount
			*
			* @return id - идендификатор подобраного элемента
			* 
		*/

		$newArrLess = [];
		$newArrMore = [];

		foreach ($arr as $key => $value) {
			if ($diff-$value->amount >= 0) {
				$newArrLess[$key] = $diff - $value->amount;
			} else {
				$newArrMore[$key] = -($diff - $value->amount);
			}	
		}

		if (is_array($newArrLess) and !empty($newArrLess)) {
			$itemId = array_search(min($newArrLess) , $newArrLess);
		} else {
			$itemId = array_search(min($newArrMore) , $newArrMore);
		}
		
		return ['id' => $itemId];

	}

	public function getSurrender($ins, $outs)
	{
		return $ins - $outs;
	}

	public function surrenderWithoutFee($surr, $fee)
	{
		return $surr - $fee;
	}

	abstract public function calculate();

}