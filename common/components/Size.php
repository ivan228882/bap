<?php

namespace common\components;

class Size
{

	public static function calculate(int $inputs = 0, int $outputs = 0) : array
	{
		$min = (($inputs * 180) + ($outputs * 34) + 10);
		$max = (($inputs * 180) + ($outputs * 34) + 10);
		return compact('min', 'max');
	}

}