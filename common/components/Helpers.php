<?php

namespace common\components;

class Helpers
{

	/**
	 * Генерация рандомных float значений
	 *
	 * @param float $min 	Минимальное значение
	 * @param float $max 	Максимальное значение
	 * @param int 	$round 	Необязательное число десятичных цифр для округления. По умолчанию 0 означает не кругляем
	 * 
	 * @return float 		Рандомное значение
	 */
	public static function randFloat(float $min, float $max, int $round = 0) : float
	{
		//validate input
		if ($min > $max) {
			$min = $max;
			$max = $min;
		} else {
			$min = $min;
			$max = $max;
		}
		$randomFloat = $min + mt_rand() / mt_getrandmax() * ($max - $min);
		if ($round > 0) {
			$randomFloat = round($randomFloat, $round);
		}

		return $randomFloat;
	}

	/**
	 * Перемешивание массива случайным образом с сохранением ключей
	 * 
	 * @param  array  $array массив который надо перемешать
	 * @return array         перемешанный массив
	 */
	public static function shuffleSaveKeys(array $array) : array
	{ 
		if (!is_array($array)) {
			return $array;
		}

		$keys = array_keys($array);
		shuffle($keys);
		$random = [];
		foreach ($keys as $key) {
			$random[$key] = $array[$key];
		}
		return $random;
	}

	public static function dd($var) {
		echo '<pre>';
		var_dump($var);
		echo '<pre>';
	}

}