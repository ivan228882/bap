<?php

namespace common\components;

class Calculate
{

	public static function minChange(int $inputs = 0, int $outputs = 0) : array
	{
		$min = (($inputs * 148) + ($outputs * 33) + 10);
		$max = (($inputs * 148) + ($outputs * 33) + 10);
		return compact('min', 'max');
	}

}