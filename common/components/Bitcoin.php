<?php

namespace common\components;

use Nbobtc\Http\Client;
use Nbobtc\Command\Command;

class Bitcoin
{

	public $dsn;
	
	protected $client;

	public function __construct()
	{
		$client = new Client($this->dsn);
		$this->client = $client;
	}

	public function getInfo() : Command
	{
		$command = new Command('getinfo');

		/** @var \Nbobtc\Http\Message\Response */
		$response = $this->client->sendCommand($command);

		/** @var string */
		$contents = $response->getBody()->getContents();

		return $contents;
	}

	public function getBlock(string $hash) : Command
	{
		return new Command('getblock', $hash);
	}

	public function sendFrom(string $fromAccount, string $toAddress, double $amount) : Command
	{
		$command = new Command('sendfrom', [$fromAccount, $toAddress, $amount]);
		
		/** @var \Nbobtc\Http\Message\Response */
		$response = $this->client->sendCommand($command);

		/** @var string */
		$contents = $response->getBody()->getContents();

		return $contents;
	}

}