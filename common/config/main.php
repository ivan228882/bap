<?php
return [
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'components' => [
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'bitcoinDsn' => function () {
			$dsn = 'http://malina:bit0kpaSSw0rD19952018@138.201.20.221:8332';
			return $dsn;
		},
		'bitcoinClient' => function () {
			return new \Nbobtc\Http\Client(Yii::$app->bitcoinDsn);
		},
	],
];
