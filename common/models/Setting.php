<?php

namespace common\models;

use Yii;

use yii\behaviors\TimestampBehavior;

class Setting extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%settings}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'value'], 'string'],
            [['active', 'is_system'], 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Идентификатор'),
            'name' => Yii::t('app', 'Название'),
            'value' => Yii::t('app', 'Значение'),
            'description' => Yii::t('app', 'Описание'),
            'active' => Yii::t('app', 'Активность'),
            'is_system' => Yii::t('app', 'Системная настройка'),
            'created_at' => Yii::t('app', 'Добавлена'),
            'updated_at' => Yii::t('app', 'Изменена'),
        ];
    }

    public static function getValue($name)
    {
        $setting = Setting::find()->where(['name' => $name])->one();
        if ($setting) {
            return $setting->value;
        }
        return null;
    }

}
