<?php

namespace common\models;

use Yii;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%wallets}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $index
 * @property string $address
 * @property string $balance
 * @property integer $created_at
 * @property integer $updated_at
 */
class Wallet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%wallets}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['index', 'created_at', 'updated_at'], 'integer'],
            [['balance', 'index'], 'default', 'value' => 0],
            [['balance'], 'number'],
            [['address', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Идентификатор'),
            'name' => Yii::t('app', 'Название'),
            'index' => Yii::t('app', 'Индекс'),
            'address' => Yii::t('app', 'Адрес'),
            'balance' => Yii::t('app', 'Баланс'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'updated_at' => Yii::t('app', 'Изменён'),
        ];
    }

    /**
     * @inheritdoc
     * @return WalletQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WalletQuery(get_called_class());
    }
}
