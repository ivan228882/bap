<?php

namespace api\modules\v1\controllers;

use Yii;

use stdClass;

use Nbobtc\Command\Command;

class BlockController extends Controller
{

	public function actionIndex() : string
	{
		echo 'This is worked';
	}

	/**
	 * [Получает подробную информацию о блоке по его хешу]
	 * Установите значение 0, чтобы получить блок в формате последовательного блока;
	 * установлен на 1 (по умолчанию), чтобы получить декодированный блок как объект JSON;
	 * установить на 2, чтобы получить декодированный блок как объект JSON с подробным декодированием транзакции 
	 *
	 * @param  string $hash Хеш блока
	 *
	 * @return stdClass     Информация о блоке
	 */
	public function actionGet(string $hash = '')
	{
		$command = new \Nbobtc\Command\Command('getblock', [$hash]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Возвращает хеш заголовка блока на заданной высоте в локальной лучшей цепочке блоков]
	 *
	 * @param  int|integer $height Высота блока, чей хеш заголовка должен быть возвращен. Высота блока жестко кодированного генеза равна 0
	 *
	 * @return stdClass       	   Хеш блока на требуемой высоте, закодированный как шестнадцатеричный порядок байтов RPC, или JSON null, если произошла ошибка
	 */
	public function actionHash(int $height = 0) : stdClass
	{
		$command = new \Nbobtc\Command\Command('getblockhash', [$height]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Возвращает количество блоков]
	 *
	 * @return stdClass Количество блоков
	 */
	public function actionCount() : stdClass
	{
		$command = new \Nbobtc\Command\Command('getblockcount');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Возвращает хеш заголовка самого последнего блока в лучшей цепочке блоков]
	 *
	 * @return stdClass Хеш последнего блока
	 */
	public function actionBestHash() : stdClass
	{
		$command = new \Nbobtc\Command\Command('getbestblockhash');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Возвращает самый последний блок]
	 *
	 * @return stdClass блок
	 */
	public function actionLatest() : stdClass
	{
		$command = new \Nbobtc\Command\Command('getbestblockhash');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$latest = json_decode($response->getBody()->getContents());

		$command = new \Nbobtc\Command\Command('getblock', $latest->result);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$getblock = json_decode($response->getBody()->getContents());

		return $getblock->result;
	}

}


