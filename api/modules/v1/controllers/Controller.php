<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\filters\auth\QueryParamAuth;
use yii\rest\Controller as YiiRestController;

use Nbobtc\Command\Command;

class Controller extends YiiRestController
{
	
	public function behaviors()
	{	
		$behaviors = parent::behaviors();
		$behaviors['authenticator']['class'] = QueryParamAuth::className();

		$behaviors['corsFilter'] = [
			'class' => \yii\filters\Cors::className(),
			'cors' => [
				'Origin'                           	=> ["*"],
				'Access-Control-Allow-Origin'    	=> ['*'],
				'Access-Control-Request-Method'    	=> ['POST', 'GET'],
				'Access-Control-Allow-Credentials' 	=> true,
				'Access-Control-Max-Age'           	=> 3600,
			],
		];
		return $behaviors;
	}

}


