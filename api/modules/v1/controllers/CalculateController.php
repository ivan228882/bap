<?php

namespace api\modules\v1\controllers;

use Yii;

use common\components\Helpers;
use common\components\calculate\MinChange;
use common\components\calculate\MinFee;
use common\components\calculate\MinPiece;
use common\components\calculate\BigPiece;

use Nbobtc\Command\Command;

class CalculateController extends Controller
{

	public function actionIndex()
	{
		echo 'This is worked';
	}

	public function actionMinChange($sum = 0, $minconf = 1, $maxconf = 9999999)
	{
		$randSum = Helpers::randFloat(0.05, 18);

		$minChange = new MinChange($randSum);
		return $minChange->calculate();
	}

	public function actionMinFee($sum = 0, $minconf = 1, $maxconf = 9999999)
	{
		
		if (Yii::$app->request->isPost) {
			$post = Yii::$app->request->post();
			$post["outAmount"] = floatval( json_decode($post["outAmount"]) );
			$minChange = new MinFee( $post["outAmount"] );

			return $minChange->calculate();
		}

		if (Yii::$app->request->get('sum')) {
			$sum = Yii::$app->request->get('sum');
			$sum = floatval( json_decode($sum) );
			$minChange = new MinFee( $sum );

			return $minChange->calculate();
		}
		
	}

	public function actionMinPiece($sum = 0, $minconf = 1, $maxconf = 9999999)
	{

		if (Yii::$app->request->isPost) {
			$post = Yii::$app->request->post();
			$post["outAmount"] = floatval( json_decode($post["outAmount"]) );
			$minChange = new MinPiece( $post["outAmount"] );

			return $minChange->calculate();
		}

		if (Yii::$app->request->get('sum')) {
			$sum = Yii::$app->request->get('sum');
			$sum = floatval( json_decode($sum) );
			$minChange = new MinPiece( $sum );

			return $minChange->calculate();
		}

	}

	public function actionBigPiece($sum = 0, $minconf = 1, $maxconf = 9999999)
	{

		if (Yii::$app->request->isPost) {
			$post = Yii::$app->request->post();
			$post["outAmount"] = floatval( json_decode($post["outAmount"]) );
			$minChange = new BigPiece( $post["outAmount"] );

			return $minChange->calculate();
		}

		if (Yii::$app->request->get('sum')) {
			$sum = Yii::$app->request->get('sum');
			$sum = floatval( json_decode($sum) );
			$minChange = new BigPiece( $sum );

			return $minChange->calculate();
		}
		
	}



}


