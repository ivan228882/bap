<?php

namespace api\modules\v1\controllers;

use Yii;

use common\models\Setting;

use Nbobtc\Command\Command;

class NotificationController extends Controller
{
    
	const SECRET_KEY = 'welkhskfugiwert34893r,mfw952wjhow30-2e';

	public function actionIndex()
	{
		echo 'This is worked';
	}

	public function actionWallet(string $txid = '')
	{
		$db = \Yii::$app->db2;

		$command = new \Nbobtc\Command\Command('gettransaction', [$txid]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$gettransaction = json_decode($response->getBody()->getContents());

		$command = new \Nbobtc\Command\Command('getrawtransaction', [$txid]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$getrawtransaction = json_decode($response->getBody()->getContents());

		$command = new \Nbobtc\Command\Command('decoderawtransaction', [$getrawtransaction->result]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$decoderawtransaction = json_decode($response->getBody()->getContents());

		$transaction = $gettransaction->result;
		$rawTransaction = $decoderawtransaction->result;

		$details = $transaction->details;
		$txamount = $transaction->amount;
		//$txfee = $transaction->fee;
		$confirmations = $transaction->confirmations;

		$addresses = [];
		if (!$details)
			return ['error' => 'Details is empty'];
		
		$response = [];

		foreach ($details as $detail) {

			if ($detail->category == 'send')
				continue;

			$address = $detail->address;
			$amount = $detail->amount;

			$payment = $db->createCommand("SELECT * FROM bitcoin_payments WHERE address = '".$address."'")->queryOne();

			if ($payment) {
				//echo 'Payment is exists!';
				//echo PHP_EOL;

				if (is_array($payment)) {
					$payment = (object) $payment;
				}
				
				if (!$confirmations) {

					$sql = "UPDATE bitcoin_payments SET confirmations = 0, tx_check = 1, tx_balance = '".$amount."', txid = '".$txid."' WHERE address = '".$address."'";
					$update = $db->createCommand($sql)->execute();

					$response[] = compact('payment', 'update');

				} else if ($confirmations > 0) {

					$sql = "UPDATE bitcoin_payments SET confirmations = '".$confirmations."', tx_check = 1, txid = '".$txid."', is_check = 1, balance = '".$amount."' WHERE address = '".$address."'";
					$update = $db->createCommand($sql)->execute();

					$query = [
						'order_id' => $payment->order_id,
						'txid' => $txid,
						'balance' => $amount,
						'is_check' => 1,
						'address' => $address,
						'confirmations' => $confirmations,
						'secret' => self::SECRET_KEY,
						'access-token' => 'EFjko3OineBf8RQCth33wpC0dZqM4CyO',
					];
					
					$host = 'http://confirm.obmen.loc/'; // local host of the site
					if (strpos($_SERVER['SERVER_NAME'], '7money') !== false) {
						$host = 'https://confirm.7money.co/';
					}

					// Get cURL resource
					$curl = curl_init();
					// Set some options - we are passing in a useragent too here
					curl_setopt_array($curl, [
					    CURLOPT_RETURNTRANSFER => 1,
					    CURLOPT_URL => $host . 'bitcoin/confirm?' . http_build_query($query),
					]);
					// Send the request & save response to $resp
					$confirm = curl_exec($curl);
					// Close request to clear up some resources
					curl_close($curl);

					$confirm = file_get_contents($host . 'bitcoin/confirm?' . http_build_query($query));
					$response[] = compact('payment', 'confirm');

				}

			}

		}

		return $response;
	}

	public function actionBlock(string $hash = '')
	{
		$command = new \Nbobtc\Command\Command('getblock', [$hash]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	public function actionAlert(string $message = '')
	{
		echo $message;
	}

}


