<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Address;
use Nbobtc\Command\Command;

class AddressController extends Controller
{
    
	public function actionIndex()
	{
		echo 'This is worked';
	}

	public function actionNew($account = '')
	{
		$command = new \Nbobtc\Command\Command('getnewaddress', $account);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$getnewaddress = json_decode($response->getBody()->getContents());
		Address::create($getnewaddress->result);
		echo $getnewaddress->result;
	}

	public function actionValidate($address = '')
	{
		$command = new \Nbobtc\Command\Command('validateaddress', $address);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$validateaddress = json_decode($response->getBody()->getContents());

		return $validateaddress->result;
	}

	public function actionListAddressGroupings()
	{
		$command = new \Nbobtc\Command\Command('listaddressgroupings');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listaddressgroupings = json_decode($response->getBody()->getContents());

		return $listaddressgroupings->result;
	}

	public function actionGetReceivedByAddress($address, $confirmations = 1)
	{
		$command = new \Nbobtc\Command\Command('listreceivedbyaddress', [$confirmations, false, true]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listreceivedbyaddress = json_decode($response->getBody()->getContents());

		$received = 0;

		if ($listreceivedbyaddress->result) {

			foreach ($listreceivedbyaddress->result as $item) {
				if ($item->address == $address) {
					if ($item->confirmations >= $confirmations) {
						$received += $item->amount;
					}
				} else {
					continue;
				}
			}

		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		echo $received;
	}

}


