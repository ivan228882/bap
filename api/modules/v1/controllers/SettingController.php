<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Setting;

class SettingController extends ActiveController
{
	
	public $modelClass = 'api\modules\v1\models\Setting';

	public function actionIndex()
	{
		$modelClass = $this->modelClass;
		$query = $modelClass::find();
		return new ActiveDataProvider([
			'query' => $query,
		]);
	}

	/*public function actionCreate()
	{
		$model = new $this->modelClass();
		$model->load(Yii::$app->getRequest()->getBodyParams(), '');
		if (!$model->save()) {
			return array_values($model->getFirstErrors())[0];
		}
		return $model;
	}
	
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$model->load(Yii::$app->getRequest()->getBodyParams(), '');
		if (!$model->save()) {
			return array_values($model->getFirstErrors())[0];
		}
		return $model;
	}

	public function actionDelete($id)
	{
		return $this->findModel($id)->delete();
	}

	public function actionView($id)
	{
		return $this->findModel($id);
	}

	public function actionSearch($id)
	{
		return $this->findModel($id);
	}

	protected function findModel($id)
	{
		$modelClass = $this->modelClass;
		if (($model = $modelClass::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}*/

}


