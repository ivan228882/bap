<?php

namespace api\modules\v1\controllers;

use Yii;

use Nbobtc\Command\Command;

class RpcController extends Controller
{
    
	public function actionIndex()
	{
		echo 'This is worked';
	}

	public function actionSendCommand($commandName, array $commandParams = [])
	{
		$bitcoinClient = Yii::$app->get('bitcoinClient');

		$command = new Command($commandName);

		/** @var \Nbobtc\Http\Message\Response */
		$response = $bitcoinClient->sendCommand($command, $commandParams);

		/** @var string */
		$contents = $response->getBody()->getContents();

		return $contents;
	}

}


