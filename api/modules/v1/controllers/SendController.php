<?php

namespace api\modules\v1\controllers;

use Yii;

use Nbobtc\Command\Command;

class SendController extends Controller
{
	
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			[
				'class' => \yii\filters\ContentNegotiator::className(),
				'formats' => [
					'application/json' => \yii\web\Response::FORMAT_JSON,
				],
			],
		];
	}

	public function actionIndex()
	{
		echo 'This is worked';
	}

	public function actionToAddress($address, $amount, $comment = '', $comment_to = '')
	{
		$args = [
			$address,
			round($amount, 8)
		];

		if (!empty($comment)) {
			array_push($args, $comment);
		}
		if (!empty($comment_to)) {
			array_push($args, $comment_to);
		}

		$command = new \Nbobtc\Command\Command('sendtoaddress', $args);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$sendtoaddress = json_decode($response->getBody()->getContents());

		return $sendtoaddress->result;
	}

}


