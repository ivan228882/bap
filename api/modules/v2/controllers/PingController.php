<?php

namespace api\modules\v2\controllers;

use Yii;
use yii\rest\Controller;

class PingController extends Controller
{
    
	public function actionIndex()
	{
		return 'pong';
	}

}
