<?php

namespace api\modules\v2\controllers;

use Yii;

use common\models\Setting;

use Nbobtc\Command\Command;

class NotificationController extends Controller
{
    
	const SECRET_KEY = 'welkhskfugiwert34893r,mfw952wjhow30-2e';

	public function actionIndex()
	{
		echo 'This is worked';
	}

	public function actionWallet(string $txid = '')
	{
		$db = \Yii::$app->db2;

		$command = new \Nbobtc\Command\Command('gettransaction', [$txid]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$gettransaction = json_decode($response->getBody()->getContents());

		$command = new \Nbobtc\Command\Command('getrawtransaction', [$txid]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$getrawtransaction = json_decode($response->getBody()->getContents());

		$command = new \Nbobtc\Command\Command('decoderawtransaction', [$getrawtransaction->result]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$decoderawtransaction = json_decode($response->getBody()->getContents());

		$transaction = $gettransaction->result;
		$rawTransaction = $decoderawtransaction->result;

		$details = $transaction->details;
		$txamount = $transaction->amount;
		//$txfee = $transaction->fee;
		$confirmations = $transaction->confirmations;

		$addresses = [];
		if (!$details)
			return ['error' => 'Details is empty'];
		
		$response = [];

		foreach ($details as $detail) {

			if ($detail->category == 'send')
				continue;

			$address = $detail->address;
			$amount = $detail->amount;

			$payment = $db->createCommand("SELECT * FROM bitcoin_payments WHERE address = '".$address."'")->queryOne();

			if ($payment) {
				//echo 'Payment is exists!';
				//echo PHP_EOL;

				if (is_array($payment)) {
					$payment = (object) $payment;
				}
				
				if (!$confirmations) {

					$sql = "UPDATE bitcoin_payments SET confirmations = 0, tx_check = 1, tx_balance = '".$amount."', txid = '".$txid."' WHERE address = '".$address."'";
					$update = $db->createCommand($sql)->execute();

					$response[] = compact('payment', 'update');

				} else if ($confirmations > 0) {

					$sql = "UPDATE bitcoin_payments SET confirmations = '".$confirmations."', tx_check = 1, txid = '".$txid."', is_check = 1, balance = '".$amount."' WHERE address = '".$address."'";
					$update = $db->createCommand($sql)->execute();

					$array = [
						'order_id' => $payment->order_id,
						'address' => $address,
						'txid' => $txid,
						'balance' => $amount,
						'is_check' => 1,
						'confirmations' => $confirmations,
						'secret' => self::SECRET_KEY,
					];
					
					$host = 'http://confirm.obmen.loc/'; // local host of the site
					if (strpos($_SERVER['SERVER_NAME'], '7money') !== false) {
						$host = 'https://confirm.7money.co/';
					}

					$query = [];

					foreach ($array as $key => $value) {
						$query[] = $key . '=' . $value;
					}

					// Get cURL resource
					/*$ch = curl_init($host . '/bitcoin/confirm');
					// Set some options - we are passing in a useragent too here
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, implode('&', $query));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					// Send the request & save response to $resp
					$response = curl_exec($ch);
					// Close request to clear up some resources
					curl_close($ch);*/

					$confirm = file_get_contents($host . 'bitcoin/confirm?' . http_build_query($array));
					$response[] = compact('payment', 'confirm');

				}

			}

		}

		//$log = $db->createCommand("INSERT INTO `logs` (`name`, `value`) VALUES ('bitcoin-confirm-response', '".json_encode($response)."')")->execute();

		return $response;
	}

	public function actionBlock(string $hash = '')
	{
		$command = new \Nbobtc\Command\Command('getblock', [$hash]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	public function actionAlert(string $message = '')
	{
		echo $message;
	}

	public function actionTest()
	{
		echo file_get_contents('https://7money.co/site/ip');
	}

}


