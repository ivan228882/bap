<?php

namespace api\modules\v2\controllers;

use Yii;

use common\components\calculate\v2\MinChange;
use common\components\calculate\v2\MinPiece;
use common\components\calculate\v2\BigPiece;

use Nbobtc\Command\Command;

class CalculateController extends Controller
{

	public function actionIndex()
	{
		echo 'This is worked';
	}

	public function actionMinChange(float $sum, float $fee = 0)
	{
		$minChange = new MinChange($sum, $fee);
		return $minChange->calculate();
	}

	public function actionMinPiece(float $sum, float $fee = 0)
	{
		$minPiece = new MinPiece($sum, $fee);
		return $minPiece->calculate();
	}

	public function actionBigPiece(float $sum, float $fee = 0)
	{
		$bigPiece = new BigPiece($sum, $fee);
		return $bigPiece->calculate();
	}



}


