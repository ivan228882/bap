<?php

namespace api\modules\v2\controllers;

use Yii;

use stdClass;

use Nbobtc\Command\Command;

class TransactionController extends Controller
{

	public function actionIndex() : string
	{
		echo 'This is worked';
	}

	/**
	 * [Получает подробную информацию о транзакции в кошельке]
	 *
	 * @param  string $txid ID транзакции
	 *
	 * @return stdClass     Информация о транзакции в кошельке, объект
	 */
	public function actionGet(string $txid = '') : stdClass
	{
		$command = new \Nbobtc\Command\Command('gettransaction', [$txid]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Получает шестнадцатеричную сериализованную транзакцию или объект JSON, описывающий транзакцию]
	 *
	 * @param  string      $txid    ID транзакции
	 * @param  int|integer $format  Следует ли получать сериализованную или декодированную транзакцию. false - сериализаванную, true - декодированную
	 *
	 * @return stdClass             null - если не найдено, строка или объект
	 */
	public function actionGetRaw(string $txid = '', bool $format = false) : stdClass
	{
		$command = new \Nbobtc\Command\Command('getrawtransaction', [$txid, $format]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Возвращает все идентификаторы транзакций (TXID) в пуле памяти в виде массива JSON или подробную информацию о каждой транзакции в пуле памяти в качестве объекта JSON]
	 *
	 * @return stdClass Объект идентификатор транзакций
	 */
	public function actionGetRawMemPool() : stdClass
	{
		$command = new \Nbobtc\Command\Command('getrawmempool');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Список транзакций]
	 *
	 * @param  string      $account Аккаунт
	 * @param  int|integer $count   Количество
	 * @param  int|integer $from    От какой транзакции выводить
	 *
	 * @return stdClass             Объект транзакций
	 */
	public function actionList(string $account = '', int $count = 20, int $from = 0)
	{
		$command = new \Nbobtc\Command\Command('listtransactions', [$account, $count, $from]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Возвращает массив неизрасходованных выходов транзакций, принадлежащих этому кошельку]
	 *
	 * @param  int|integer $minconf   Мин подтверждений
	 * @param  int|integer $maxconf   Макс подтверждений
	 * @param  string 	   $addresses Адреса должны быть оплачены. Если присутствует, будут возвращены только те, которые оплачены в этом массиве
	 *
	 * @return stdClass               Объект входов
	 */
	public function actionListUnspent(int $minconf = 1, int $maxconf = 9999999, string $addresses = '')
	{
		$args = [$minconf, $maxconf];

		if (!empty($addresses)) {
			array_push($args, json_decode($addresses));
		}

		$command = new \Nbobtc\Command\Command('listunspent', $args);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Обновляет список временно непередаваемых выходов]
	 *
	 * @param  bool|boolean $unlock       Разблокировать(1) или заблокировать(0)?
	 * @param  string       $transactions JSON объект транзакций в строке
	 *
	 * @return stdClass                    Результат
	 */
	public function actionLockUnspent(bool $unlock = true, string $transactions = '') : stdClass
	{
		$transactions = json_decode($transactions);

		$command = new \Nbobtc\Command\Command('lockunspent', [(bool)$unlock, $transactions]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Возвращает список временно непередаваемых выходов]
	 *
	 * @return stdClass объект выходов
	 */
	public function actionListLockUnspent() : stdClass
	{
		$command = new \Nbobtc\Command\Command('listlockunspent');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Возвращает все транзакции указанного блока]
	 *
	 * @param  string      $blockhash     хэш блока
	 * @param  int|integer $confirmations количество подтверждений
	 *
	 * @return stdClass                   объект транзакций
	 */
	public function actionListSinceBlock(string $blockhash = '', int $confirmations = 3) : stdClass
	{
		$command = new \Nbobtc\Command\Command('listsinceblock', [$blockhash, $confirmations]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * Возвращает сведения о неиспользованном выводе транзакции]
	 *
	 * @param  string       $txid           id транзакции
	 * @param  int|integer  $vout           номер выхода
	 * @param  bool|boolean $includemempool включать из памяти
	 *
	 * @return stdClass                     объект транзакции
	 */
	public function actionTxOut(string $txid = '', int $vout = 0, bool $includemempool = true) : stdClass
	{
		$command = new \Nbobtc\Command\Command('gettxout', [$txid, $vout, $includemempool]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Возвращает шестнадцатеричное доказательство того, что одна или несколько указанных транзакций были включены в блок]
	 *
	 * @param  string $txids JSON массив идентификаторов транзакций в строке
	 * @param  string $hash  hash блока в котором нужно искать
	 *
	 * @return stdClass        шестнадцатеричное доказательство. использовать метод(actionVerifyTxOutProof) ниже для получения транзакций используя это значение
	 */
	public function actionTxOutProof(string $txids = '', string $hash = '') : stdClass
	{
		$txids = json_decode($txids);

		$command = new \Nbobtc\Command\Command('gettxoutproof', [$txids, $hash]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Проверяет, что доказательство указывает на одну или несколько транзакций в блоке, возвращая транзакции, которые подтверждают данные, и бросает ошибку RPC, если блок не находится в нашей лучшей цепочке блоков]
	 *
	 * @param  string $proof Шестнадцатеричное доказательство, созданное методом actionTxOutProof. Экран с шестнадцатеричным кодированием
	 *
	 * @return stdClass        Значение txid(s), которое завершается доказательством, или пустой массив, если доказательство недействительно
	 */
	public function actionVerifyTxOutProof(string $proof = '') : stdClass
	{
		$command = new \Nbobtc\Command\Command('verifytxoutproof', [$txid, $vout, $includemempool]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	/**
	 * [Возвращает статистику о подтвержденном неизрасходованном выводе транзакции (UTXO). Обратите внимание, что этот вызов может занять некоторое время и что он учитывает только выходы из подтвержденных транзакций - он не учитывает выходы из пула памяти]
	 *
	 * @return stdClass Статистика
	 */
	public function actionTxOutSetInfo() : stdClass
	{
		$command = new \Nbobtc\Command\Command('gettxoutsetinfo', [$txid, $vout, $includemempool]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	public function actionCreateRawTransaction() : stdClass
	{
		$inputs = Yii::$app->request->post('inputs');
		$outputs = Yii::$app->request->post('outputs');

		if (is_string($inputs)) {
			$inputs = json_decode($inputs);
		}
		if (is_string($outputs)) {
			$outputs = json_decode($outputs);
		}

		$command = new \Nbobtc\Command\Command('createrawtransaction', [$inputs, $outputs]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	public function actionDecodeRawTransaction(string $hex) : stdClass
	{
		$command = new \Nbobtc\Command\Command('decoderawtransaction', [(string) $hex]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	public function actionSignRawTransaction(string $hex) : stdClass
	{
		$command = new \Nbobtc\Command\Command('signrawtransaction', [(string) $hex]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

	public function actionSendRawTransaction(string $hex) : stdClass
	{
		$command = new \Nbobtc\Command\Command('sendrawtransaction', [(string) $hex]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		return json_decode($response->getBody()->getContents());
	}

}


