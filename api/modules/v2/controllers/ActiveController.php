<?php

namespace api\modules\v2\controllers;

use Yii;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController as YiiActiveController;
use yii\data\ActiveDataProvider;

class ActiveController extends YiiActiveController
{

	public function behaviors()
	{	
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => QueryParamAuth::className(),
		];
		$behaviors['corsFilter'] = [
			'class' => \yii\filters\Cors::className(),
			'cors' => [
				'Origin'                           	=> ["*"],
				'Access-Control-Allow-Origin'    	=> ['*'],
				'Access-Control-Request-Method'    	=> ['POST', 'GET'],
				'Access-Control-Allow-Credentials' 	=> true,
				'Access-Control-Max-Age'           	=> 3600,
			],
		];
		return $behaviors;
	}

	public function actionIndex()
	{
		$modelClass = $this->modelClass;
		$query = $modelClass::find();
		return new ActiveDataProvider([
			'query' => $query,
		]);
	}

	public function actionCreate()
	{
		$model = new $this->modelClass();
		$model->load(Yii::$app->getRequest()->getBodyParams(), '');
		if (!$model->save()) {
			return array_values($model->getFirstErrors())[0];
		}
		return $model;
	}
	
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$model->load(Yii::$app->getRequest()->getBodyParams(), '');
		if (!$model->save()) {
			return array_values($model->getFirstErrors())[0];
		}
		return $model;
	}

	public function actionDelete($id)
	{
		return $this->findModel($id)->delete();
	}

	public function actionView($id)
	{
		return $this->findModel($id);
	}

	public function actionSearch()
	{
		$conditions = Yii::$app->request->get();
		$modelClass = $this->modelClass;
		$query = $modelClass::find()->where($conditions);
		return new ActiveDataProvider([
			'query' => $query,
		]);
	}

	protected function findModel($id)
	{
		$modelClass = $this->modelClass;
		if (($model = $modelClass::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}


