<?php

namespace api\modules\v2\controllers;

use Yii;

use common\models\Setting;

use Nbobtc\Command\Command;

class AccountController extends Controller
{
    
	public function actionIndex()
	{
		echo 'This is worked';
	}

	public function actionBalance($account = null)
	{
		$command = new \Nbobtc\Command\Command('getbalance', $account);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$getbalance = json_decode($response->getBody()->getContents());

		return $getbalance->result;
	}

	public function actionBalances($account = null)
	{
		$command = new \Nbobtc\Command\Command('getwalletinfo');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$balanceObj = json_decode($response->getBody()->getContents());
		$balance = $balanceObj->result->balance;

		$confirmedBalance = $balance;
		$unconfirmedBalance = $balanceObj->result->unconfirmed_balance - $balanceObj->result->immature_balance;

		if ($confirmedBalance < 0) {
			$confirmedBalance = 0;
		}
		$balance = $confirmedBalance + $unconfirmedBalance;

		/* for other accounts */
		$command = new \Nbobtc\Command\Command('getbalance', ['blockchain']);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$blockchainBalance = json_decode($response->getBody()->getContents());
		if ($blockchainBalance->result > 0) {
			$balance = $balance - $blockchainBalance->result;
		}

		return compact('balance', 'confirmedBalance', 'unconfirmedBalance');
	}

	public function actionPass($timeout = 30)
	{
		$command = new \Nbobtc\Command\Command('walletpassphrase', [(string)Yii::$app->params['bitcoin.pass'], $timeout]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$walletpassphrase = json_decode($response->getBody()->getContents());

		return $walletpassphrase->result;
	}

}


