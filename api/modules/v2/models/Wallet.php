<?php

namespace api\modules\v2\models;

use \yii\db\ActiveRecord;

class Wallet extends ActiveRecord
{
	
	public static function tableName()
	{
		return 'wallets';
	}

    public function rules()
    {
        return [
            [['name'], 'required']
        ];
    }

}