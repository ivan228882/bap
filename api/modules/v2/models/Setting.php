<?php

namespace api\modules\v2\models;

use \yii\db\ActiveRecord;

class Setting extends ActiveRecord
{
	
	public static function tableName()
	{
		return 'settings';
	}

    public function rules()
    {
        return [
            [['name', 'value'], 'required'],
            [['active', 'is_system'], 'safe'],
            [['active', 'is_system'], 'default', 'value' => 0],
        ];
    }

}