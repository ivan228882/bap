var http = require('http');
//var fs = require('fs');
//var app = require('express')();

/*var options = {
	key: fs.readFileSync('key.pem'),
	cert: fs.readFileSync('cert.pem'),
	requestCert: false,
    rejectUnauthorized: false
};*/

var server = http.createServer(function (req, res) {
  	/*res.writeHead(200, { 
		'Access-Control-Allow-Origin': 'https://admin.7money.co' 
	});*/
	// Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
  	res.end("hello world\n");
}).listen(6379);

var io = require('socket.io').listen(server);

io.on('connection', function (socket) {
  
	socket.on('disconnect', function () {
		console.log('user disconnected');
	});

	socket.on('block-event', function (hash) {
		io.emit('new-block', hash);
	});

	socket.on('wallet-event', function (txid) {
		io.emit('new-wallet', txid);
	});

	socket.on('alert-event', function (message) {
		io.emit('new-alert', message);
	});

});