<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\components\AddressValidator;
use common\models\Setting;

class RawForm extends Model
{
    
    public $balance;
    public $fee;
    public $feeSatoshi;

    public $ins = [];

    public $outAddresses = [];
    public $outAmounts = [];

    public $sumIn;
    public $sumOut;

    public $limitSmallChange;
    
    public $addressSmallChange;
    public $addressBigChange;

    public $redirectUri;

    public function init()
    {
        $this->fee = Setting::find()->select('value')->where(['name' => 'fee'])->column()[0];
        $this->feeSatoshi = Setting::find()->select('value')->where(['name' => 'fee_satoshi'])->column()[0];

        $command = new \Nbobtc\Command\Command('getbalance');
        $response = Yii::$app->bitcoinClient->sendCommand($command);
        $getbalance = json_decode($response->getBody()->getContents());
        $this->balance = $getbalance->result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['addressSmallChange', 'addressBigChange'], 'string', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['addressSmallChange', 'addressBigChange'], 'validateAddress', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['fee'], 'double', 'min' => 0.0000001],
            [['feeSatoshi'], 'double'],
            [['ins'], 'safe'],
        ];
    }

    public function validateAddress($attribute, $params)
    {
        $isValid = AddressValidator::isValid($this->$attribute);
        if (!$isValid) {
            $this->addError($attribute, 'Адрес невалиден');
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'balance' => 'Баланс',
            'fee' => 'Комиссия',
            'feeSatoshi' => 'Сатоши',
            'limitSmallChange' => 'Лимит мелкой сдачи',
            'addressSmallChange' => 'Адрес мелкой сдачи',
            'addressBigChange' => 'Адрес крупной сдачи',
            'ins' => 'Вход',
            'outAddresses' => 'Адрес',
            'outAmounts' => 'Сумма',
        ];
    }

}
