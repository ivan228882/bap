<?php

namespace frontend\controllers;

use Yii;
use yii\{
    data\Pagination,
    data\ArrayDataProvider,
    data\Sort
};

class BlockController extends Controller
{

	public function actionIndex($page = null)
	{
		set_time_limit(-1);

		$blocksArray = [];

		$command = new \Nbobtc\Command\Command('getblockcount');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$count = json_decode($response->getBody()->getContents());

		// подключаем класс Pagination, выводим по 10 пунктов на страницу
		$pages = new Pagination(['totalCount' => $count->result, 'pageSize' => 10]);
		// приводим параметры в ссылке к ЧПУ
		$pages->pageSizeParam = false;

		$startIndex = $count->result;
		$endIndex = $count->result - 9;
		if ($page and $page >= 2) {
            $startIndex = $count->result - ($page * 10);
            $endIndex = $startIndex - 9;
		}

		$command = new \Nbobtc\Command\Command('getblockhash', $startIndex);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$hash = json_decode($response->getBody()->getContents());

		$blockHash = $hash->result;

		$blocks = [];
		for ($i = $startIndex; $i >= $endIndex; $i--) {

			$command = new \Nbobtc\Command\Command('getblock', $blockHash);
			$response = Yii::$app->bitcoinClient->sendCommand($command);
			$block = json_decode($response->getBody()->getContents());

			if (isset($block->result->previousblockhash)) {
                $blockHash = $block->result->previousblockhash;
			} else {
				break;
			}
			
			$blocks[] = $block->result;
		}

		foreach ($blocks as $i => $block) {

            $blocksArray[$i] = (array)$block;

            $diff = strtotime('now') - $block->time;

            $blocksArray[$i]['date'] = $diff;
            $blocksArray[$i]['blocktime'] = $block->time;
            $blocksArray[$i]['dateSort'] = $blocksArray[$i]['date'];
            $blocksArray[$i]['tx'] = Yii::$app->formatter->asInteger(count($block->tx));
            $blocksArray[$i]['size'] = Yii::$app->formatter->asDecimal($block->size);
            $blocksArray[$i]['weight'] = Yii::$app->formatter->asDecimal($block->weight);
            $blocksArray[$i]['height'] = Yii::$app->formatter->asDecimal($block->height);

        }

		$sortBlocks = new Sort([
			'defaultOrder' => [
				'date' => SORT_ASC,
			],
			'attributes' => [
				'date' => [
					'asc' => ['date' => SORT_ASC],
					'desc' => ['date' => SORT_DESC],
					'default' => SORT_ASC,
				],
				'tx' => [
					'asc' => ['tx' => SORT_ASC],
					'desc' => ['tx' => SORT_DESC],
					'default' => SORT_ASC,
				],
				'size' => [
					'asc' => ['size' => SORT_ASC],
					'desc' => ['size' => SORT_DESC],
					'default' => SORT_ASC,
				],
				'weight' => [
					'asc' => ['weight' => SORT_ASC],
					'desc' => ['weight' => SORT_DESC],
					'default' => SORT_ASC,
				],
				'height' => [
					'asc' => ['height' => SORT_ASC],
					'desc' => ['height' => SORT_DESC],
					'default' => SORT_ASC,
				],
			],
		]);

		$providerBlocks = new ArrayDataProvider([
			'allModels' => $blocksArray,
			'pagination' => [
				'pageSize' => 10,
			],
			'sort' => $sortBlocks,
			
		]);

		return $this->render('index', ['providerBlocks' => $providerBlocks, 'blocks' => $blocks, 'pages' => $pages]);
	}

	public function actionView($hash)
	{
		set_time_limit(-1);

		$command = new \Nbobtc\Command\Command('getblock', $hash);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$getBlock = json_decode($response->getBody()->getContents());

		$block = $getBlock->result;
		
		return $this->render('view', ['block' => $block]);
	}

	public function actionLatest()
	{
		set_time_limit(-1);

		$command = new \Nbobtc\Command\Command('getbestblockhash');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$latest = json_decode($response->getBody()->getContents());
		
		return $this->redirect(['view', 'hash' => $latest->result]);
	}

	public function actionListSinceBlock($hash)
	{
		set_time_limit(-1);

		$command = new \Nbobtc\Command\Command('getblock', $hash);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$getBlock = json_decode($response->getBody()->getContents());

		$block = $getBlock->result;

		$command = new \Nbobtc\Command\Command('listsinceblock', $hash);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listSinceBlock = json_decode($response->getBody()->getContents());
		$walletTransactions = $listSinceBlock->result;

		$transactions = [];
		if (isset($block->tx) and !empty($block->tx)) {
			foreach ($block->tx as $txid) {
				$command = new \Nbobtc\Command\Command('getrawtransaction', $txid);
				$response = Yii::$app->bitcoinClient->sendCommand($command);
				$getRawTransaction = json_decode($response->getBody()->getContents());

				if ($getRawTransaction->result) {
					$command = new \Nbobtc\Command\Command('decoderawtransaction', $getRawTransaction->result);
					$response = Yii::$app->bitcoinClient->sendCommand($command);
					$decodeRawTransaction = json_decode($response->getBody()->getContents());

					$transaction = $decodeRawTransaction->result;
					if ($transaction) {
                        $transactions[] = $transaction;
                    }
				}
			}
		}
		
		return $this->renderAjax('list-since-block', ['block' => $block, 'transactions' => $transactions, 'walletTransactions' => $walletTransactions]);
	}

}
