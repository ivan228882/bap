<?php

namespace frontend\controllers;

use Yii;
use yii\{
    data\ArrayDataProvider,
    web\NotFoundHttpException,
    data\Sort
};

class TransactionController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
	{
		set_time_limit(-1);

		$command = new \Nbobtc\Command\Command('getwalletinfo');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$walletInfo = json_decode($response->getBody()->getContents());

		$count = $walletInfo->result->txcount;
		$pageSize = 10;
		$skip = ((int) Yii::$app->request->get('page', 1)) * $pageSize - $pageSize;

		$command = new \Nbobtc\Command\Command('listtransactions', ['*', $pageSize, $skip]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listtransactions = json_decode($response->getBody()->getContents());

		$transactions = $listtransactions->result;

		if ($transactions) {
			foreach ($transactions as &$transaction) {
				$command = new \Nbobtc\Command\Command('getrawtransaction', [$transaction->txid, true]);
				$response = Yii::$app->bitcoinClient->sendCommand($command);
                $getRawTransaction = json_decode($response->getBody()->getContents());

				$transaction->raw = $getRawTransaction->result;
			}
		}
		
		$sortTransactions = new Sort([
			'defaultOrder' => [
				'time' => SORT_DESC,
			],
	        'attributes' => [
					'time' => [
						'asc' => ['time' => SORT_ASC],
		                'desc' => ['time' => SORT_DESC],
		                'default' => SORT_ASC,
	            	],
	            	'address' => [
						'asc' => ['address' => SORT_ASC],
		                'desc' => ['address' => SORT_DESC],
		                'default' => SORT_ASC,
	            	],
	            	'confirmations' => [
						'asc' => ['confirmations' => SORT_ASC],
		                'desc' => ['confirmations' => SORT_DESC],
		                'default' => SORT_ASC,
	            	],
	            	'category' => [
						'asc' => ['category' => SORT_ASC],
		                'desc' => ['category' => SORT_DESC],
		                'default' => SORT_ASC,
	            	],
	            	'label' => [
						'asc' => ['label' => SORT_ASC],
		                'desc' => ['label' => SORT_DESC],
		                'default' => SORT_ASC,
	            	],
	            	'amount' => [
						'asc' => ['amount' => SORT_ASC],
		                'desc' => ['amount' => SORT_DESC],
		                'default' => SORT_ASC,
	            	],
	            	'fee' => [
						'asc' => ['fee' => SORT_ASC],
		                'desc' => ['fee' => SORT_DESC],
		                'default' => SORT_ASC,
	            	],
	            	'vout' => [
						'asc' => ['vout' => SORT_ASC],
		                'desc' => ['vout' => SORT_DESC],
		                'default' => SORT_ASC,
	            	],
				],
	    ]);

        $providerTransations = new ArrayDataProvider([
			'allModels' => $transactions,
			'pagination' => [
				'totalCount' => $count,
				'pageSize' => $pageSize,
			],
			'sort' => $sortTransactions,
		]);

		$pagination = new \yii\data\Pagination(['totalCount' => $count, 'pageSize' => $pageSize]);

		return $this->render('index', ['providerTransations' => $providerTransations, 'pagination' => $pagination]);
	}

	public function actionLatest()
	{
		set_time_limit(-1);

		$command = new \Nbobtc\Command\Command('listtransactions');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listtransactions = json_decode($response->getBody()->getContents());
		$transactions = [];
		if (count($transactions) > 50) {
			$transactions = array_splice($listtransactions->result, 50);
		} else {
			$transactions = $listtransactions->result;
		}

		$transactions = array_reverse($transactions);
		
		$provider = new ArrayDataProvider([
			'allModels' => $transactions,
			'pagination' => [
				'pageSize' => 10,
			],
		]);
		$transactions = $provider->getModels();
		$pages = $provider->getPagination();

		return $this->render('latest', ['transactions' => $transactions, 'pages' => $pages]);
	}

    /**
     * @param $txid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($txid)
	{
		set_time_limit(-1);

		$transaction = $this->getTransactionById($txid);
		
		return $this->render('view', ['transaction' => $transaction]);
	}

	public function actionUnconfirmed()
	{
		
		$command = new \Nbobtc\Command\Command('listtransactions', ['*', 200]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listtransactions = json_decode($response->getBody()->getContents());

		$transactions = $listtransactions->result;
		
		if (!empty($transactions)) {
			foreach ($transactions as $key => $value) {
				if ($value->confirmations !== 0) {
					unset($transactions[$key]);
				}
			}
		}

		$provider = new ArrayDataProvider([
			'allModels' => $transactions,
			'pagination' => [
				'pageSize' => 10,
			],
		]);

		$transactions = $provider->getModels();
		$pages = $provider->getPagination();

		return $this->render('unconfirmed', ['transactions' => $transactions, 'pages' => $pages]);

	}

	public function actionUnspent()
	{
		$command = new \Nbobtc\Command\Command('listunspent', [0]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listunspent = json_decode($response->getBody()->getContents());
		
		$unspent = (array)$listunspent->result;

		$array = [];
		foreach ($unspent as $i => $row) {
			$array[$i] = (array) $row;
		}

		$sort = new \yii\data\Sort([
		    'attributes' => [
				'address',
				'txid',
		        'confirmations',
		        'amount',
		        'vout',
		    ],
		]);

		$dataProvider = new ArrayDataProvider([
		    'allModels' => (array)$array,
		    'sort' => $sort,
		    'pagination' => [
		    	'pageSize' => 50
		    ],
		]);

		return $this->render('unspent', compact('unspent', 'dataProvider'));
	}

	public function actionGet($txid = null)
	{
		if ($txid = Yii::$app->request->get('txid')) {
			$command = new \Nbobtc\Command\Command('getrawtransaction', [$txid, true]);
			$response = Yii::$app->bitcoinClient->sendCommand($command);
			$getrawtransaction = json_decode($response->getBody()->getContents());

			if ($getrawtransaction->result === null) {
				$getrawtransaction = $getrawtransaction->error;
			} else {
				$getrawtransaction = $getrawtransaction->result;
			}
			return $this->render('get-form', ['transaction' => $getrawtransaction]);
		}
		return $this->render('get-form');
	}

    /**
     * Получение транзакции по идентификатору
     * @param string $txid
     * @throws NotFoundHttpException
     * @return object
     */
	public function getTransactionById($txid)
	{
		$command = new \Nbobtc\Command\Command('getrawtransaction', [$txid, true]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$getrawtransaction = json_decode($response->getBody()->getContents());

		$transaction = $getrawtransaction->result;

		if (!isset($transaction->txid)) {
			throw new NotFoundHttpException('Not found transaction');
		}

		return $transaction;
	}

}
