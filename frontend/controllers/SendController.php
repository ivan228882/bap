<?php

namespace frontend\controllers;

use Yii;
use common\{
    models\User, models\Setting
};
use frontend\models\RawForm;
use yii\data\ArrayDataProvider;

class SendController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
	{
		return $this->render('index');
	}

    /**
     * @return string|\yii\web\Response
     */
    public function actionToAddress()
	{
		$fees = [];
		$blocks = [2, 5, 10, 20];
		foreach ($blocks as $count) {
			$command = new \Nbobtc\Command\Command('estimatefee', [$count]);
			$response = Yii::$app->bitcoinClient->sendCommand($command);
			$estimateFee = json_decode($response->getBody()->getContents());

			$fee = [
				'blocks' => $count,
				'fee' => $estimateFee->result
			];
			$fees[] = $fee;
		}

		if (Yii::$app->request->isPost) {
			$to = Yii::$app->request->post('to');
			if (empty($to)) {
				Yii::$app->session->addFlash('error', 'Укажите адрес получения!');
				return $this->redirectBack();
			}
			if (!empty($to)) {
				$command = new \Nbobtc\Command\Command('validateaddress', [$to]);
				$response = Yii::$app->bitcoinClient->sendCommand($command);
				$validateAddress = json_decode($response->getBody()->getContents());
				if ($validateAddress and isset($validateAddress->result)) {
					if ($validateAddress->result->isvalid == false) {
						Yii::$app->session->addFlash('error', 'Адрес получения не валиден!');
						return $this->redirectBack();
					}
				}
			}
			$amount = Yii::$app->request->post('amount');
			if (empty($amount) or $amount <= 0) {
				Yii::$app->session->addFlash('error', 'Сумма не может быть меньше или равна 0!');
				return $this->redirectBack();
			}

			$comment = Yii::$app->request->post('comment') ?: '';
			$comment_to = Yii::$app->request->post('comment_to') ?: '';

			$arguments = [
				$to,
				$amount
			];

			if (!empty($comment)) {
				array_push($arguments, $comment);
			}
			if (!empty($comment_to)) {
				array_push($arguments, $comment_to);
			}

			if ($_SERVER['SERVER_NAME'] != 'bitcoin.loc') {
				$command = new \Nbobtc\Command\Command('walletpassphrase', [(string)Yii::$app->params['bitcoin.pass'], 30]);
				Yii::$app->bitcoinClient->sendCommand($command);
			}

			$command = new \Nbobtc\Command\Command('sendtoaddress', $arguments);
			$response = Yii::$app->bitcoinClient->sendCommand($command);
			$sendToAddress = json_decode($response->getBody()->getContents());

			if ($sendToAddress and isset($sendToAddress->result) and !empty($sendToAddress->result)) {
				Yii::$app->session->addFlash('success', 'Транзакция успешно создана! Номер транзакции: ' . $sendToAddress->result);
				return $this->redirectBack();
			} else if ($sendToAddress and isset($sendToAddress->error)) {
				$error = $sendToAddress->error;
				if ($error->message == 'Insufficient funds') {
					Yii::$app->session->addFlash('error', 'Ошибка создания транзакции! Недостаточно средств');
				} else {
					Yii::$app->session->addFlash('error', 'Ошибка создания транзакции! Ошибка: ' . $error->message);
				}
				return $this->redirectBack();
			}

		}

		return $this->render('address', compact('fees'));
	}

    /**
     * @return string|\yii\web\Response
     */
    public function actionMove()
	{
		$command = new \Nbobtc\Command\Command('listaccounts', [1, true]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listAccounts = json_decode($response->getBody()->getContents());

		$accounts = $listAccounts->result;

		if (Yii::$app->request->isPost) {
			$from = Yii::$app->request->post('from');
			$to = Yii::$app->request->post('to');

			if ($from == $to) {
				Yii::$app->session->addFlash('error', 'Выберите разные аккаунты!');
				return $this->redirectBack();
			}

			$amount = Yii::$app->request->post('amount');
			if (empty($amount) or $amount <= 0) {
				Yii::$app->session->addFlash('error', 'Сумма не может быть меньше или равна 0!');
				return $this->redirectBack();
			}

			$comment = Yii::$app->request->post('comment');

			$arguments = [
				$from,
				$to,
				$amount
			];

			if (!empty($comment)) {
				array_push($arguments, 1);
				array_push($arguments, $comment);
			}

			if ($_SERVER['SERVER_NAME'] != 'bitcoin.loc') {
				$command = new \Nbobtc\Command\Command('walletpassphrase', [(string)Yii::$app->params['bitcoin.pass'], 30]);
				$response = Yii::$app->bitcoinClient->sendCommand($command);
			}

			$command = new \Nbobtc\Command\Command('move', $arguments);
            if (isset(Yii::$app->bitcoinClient)) {
                $response = Yii::$app->bitcoinClient->sendCommand($command);
            }
			$move = json_decode($response->getBody()->getContents());

			if ($move->result == true) {
				Yii::$app->session->addFlash('success', 'Успешно отправлено!');
				return $this->redirectBack();
			} else {
				Yii::$app->session->addFlash('error', 'Ошибка отправки! Попробуйте позже');
				return $this->redirectBack();
			}

		}

		return $this->render('move', compact('accounts'));
	}

    /**
     * @return string
     */
    public function actionRaw()
	{	
		$session = Yii::$app->session;

		$model = new RawForm();

		$command = new \Nbobtc\Command\Command('listtransactions');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listTransactions = json_decode($response->getBody()->getContents());

		$transactions = $listTransactions->result;

		$command = new \Nbobtc\Command\Command('getaccountaddress');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$getAccountAddress = json_decode($response->getBody()->getContents());

		$command = new \Nbobtc\Command\Command('listunspent', [0, $getAccountAddress->result]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listUnspent = json_decode($response->getBody()->getContents());

		$unspent = (array)$listUnspent->result;

        $addressSmallChange = '';
        $addressBigChange = '';

        if ($value = Setting::getValue('address_small_change')) {
            $addressSmallChange = $value;
        }
        if ($value = Setting::getValue('address_big_change')) {
            $addressBigChange = $value;
        }

		if (Yii::$app->request->isAjax) {

			$post = Yii::$app->request->post();
			$post = $post['RawForm'];

			$fee = (float)$post['fee'];
			if (empty($fee)) {
				$fee = $model->fee;
			}

			$limitSmallChange = (float)$post['limitSmallChange'];
			$addressSmallChange = $post['addressSmallChange'];
			$addressBigChange = $post['addressBigChange'];

            if (empty($limitSmallChange)) {
                Yii::$app->session->addFlash('error', 'Укажите лимит мелкой сдачи!');
                return $this->redirectBack();
            }

            if (empty($addressSmallChange)) {
                Yii::$app->session->addFlash('error', 'Укажите адрес мелкой сдачи!');
                return $this->redirectBack();
            }

            if (empty($addressBigChange)) {
                Yii::$app->session->addFlash('error', 'Укажите адрес крупной сдачи!');
                return $this->redirectBack();
            }

			$txs = $post['ins'];
			$outAddresses = $post['outAddresses'] ?: [];
			$outAmounts = $post['outAmounts'];
			$ins = [];
			$insAmount = 0;

			foreach ($unspent as $i => $tx) {
				if (is_array($txs) and !empty($txs) and array_key_exists($tx->txid.':'.$tx->vout, $txs)) {
					if ((int)$txs[$tx->txid.':'.$tx->vout] == 1) {
						$in = new \stdClass();
						$in->txid = $tx->txid;
						$in->vout = $tx->vout;
						$ins[] = $in;
						$insAmount += (float)$tx->amount;
					}
				}
			}

			$session->set('ins', $ins);

			$outs = [];
			$outsAmount = 0;

            foreach ($outAddresses as $i => $address) {
                if (array_key_exists($i, $outAmounts)) {
                    if (isset($outs[$address])) {
                        $outs[$address] += round($outAmounts[$i], 8);
                    } else {
                        $outs[$address] = round($outAmounts[$i], 8);
                    }
                    $outsAmount += round($outAmounts[$i], 8);
                }
            }

			if (round(($outsAmount + $fee), 8) > round($insAmount, 8)) {
				Yii::$app->session->addFlash('error', 'Сумма входов меньше суммы выходов с комиссией! Сумма входов: ' . $insAmount . '. Сумма выходов с комиссией: ' . ($outsAmount + $fee) . '.');
				return $this->redirectBack();
			} else {
				$changeWithoutFee = $insAmount - ($outsAmount + $fee); // Сдача с вычетом комиссии
				if ($changeWithoutFee < 0) {
					Yii::$app->session->addFlash('error', 'Сдача с вычетом комиссии меньше нуля!');
					return $this->redirectBack();
				}
				if (($changeWithoutFee and $changeWithoutFee > 0)) { // Проверка сдачи, и добавление выходов для сдачи

					if ($changeWithoutFee > 0 and $changeWithoutFee <= trim($limitSmallChange) and !empty(trim($addressSmallChange))) {
						if (isset($outs[$addressSmallChange])) {
							$outs[$addressSmallChange] += round($changeWithoutFee, 8);
						} else {
							$outs[$addressSmallChange] = round($changeWithoutFee, 8);
						}
					} else {
						if ($changeWithoutFee > 0 and !empty($addressBigChange)) {
							if (isset($outs[$addressBigChange])) {
								$outs[$addressBigChange] += round($changeWithoutFee, 8);
							} else {
								$outs[$addressBigChange] = round($changeWithoutFee, 8);
							}
						}
					}
				}
			}

			$session->set('outs', $outs);

			return json_encode(['modal' => 'start', 'outAddresses' => $outAddresses, 'countOuts' => count($outs)]);
		}

		$array = [];
		foreach ($unspent as $i => $row) {
			
			if ($row->confirmations === 0 and ($row->address != $addressSmallChange and $row->address != $addressBigChange)) {
				continue;
			}
			
			$array[$i] = (array) $row;
		}

		$sort = new \yii\data\Sort([
		    'attributes' => [
				'address',
				'txid',
		        'confirmations',
		        'amount',
		        'vout',
		    ],
		]);

		$dataProvider = new ArrayDataProvider([
		    'allModels' => (array)$array,
		    'sort' => $sort,
		    'pagination' => [
		    	'pageSize' => 100
		    ],
		]);

		$user = User::findByUsername('admin');
		$token = $user->auth_key;

		return $this->render('raw', compact('token', 'model', 'transactions', 'unspent', 'dataProvider'));
	}

	public function actionSend()
	{
		if (Yii::$app->request->isAjax) {

			$post = Yii::$app->request->post();

			if (isset($post['transactionSubcribe']) && $post['transactionSubcribe'] == true) {
				
				$ins = Yii::$app->session->get('ins');
				$outs = Yii::$app->session->get('outs');
				
				if (isset($ins) && is_array($ins) && isset($outs) && is_array($outs)) {

					$command = new \Nbobtc\Command\Command('createrawtransaction', [$ins, $outs]);
					$response = Yii::$app->bitcoinClient->sendCommand($command);
					$createRawTransaction = json_decode($response->getBody()->getContents());

					$hash = $createRawTransaction->result;
					if (!empty($hash)) {
						//Yii::$app->session->addFlash('success', 'Транзакция успешно создана!');
					} else {
						Yii::$app->session->addFlash('error', 'Транзакция не создана!');
						Yii::$app->session->addFlash('info', $createRawTransaction->error->message);
						return $this->redirectBack();
					}

					if ($_SERVER['SERVER_NAME'] != 'bitcoin.loc') {
						$command = new \Nbobtc\Command\Command('walletpassphrase', [(string)Yii::$app->params['bitcoin.pass'], 30]);
						Yii::$app->bitcoinClient->sendCommand($command);
					}

					$command = new \Nbobtc\Command\Command('signrawtransaction', [(string)$hash]);
					$response = Yii::$app->bitcoinClient->sendCommand($command);
                    $signRawTransaction = json_decode($response->getBody()->getContents());

					$sign = $signRawTransaction->result;

					if (isset($sign->complete) and $sign->complete == true) {
						Yii::$app->session->addFlash('success', 'Транзакция успешно подписана!');
						$command = new \Nbobtc\Command\Command('sendrawtransaction', [$sign->hex]);
						$response = Yii::$app->bitcoinClient->sendCommand($command);
						$sendRawTransaction = json_decode($response->getBody()->getContents());

						$send = $sendRawTransaction->result;

						if (!empty($send)) {
							Yii::$app->session->addFlash('success', 'Транзакция успешно отправлена!');
							Yii::$app->session->addFlash('info', 'TXID: ' . $send);
							return $this->redirect(['send/raw']);
						} else {
							Yii::$app->session->addFlash('error', 'Транзакция не отправлена!');
							Yii::$app->session->addFlash('info', 'Результат отправки: ' . $sendRawTransaction->error->message);
							return $this->redirect(['send/raw']);
						}
					} else {
						Yii::$app->session->addFlash('error', 'Транзакция не подписана!');
						Yii::$app->session->addFlash('info', 'Ошибка подписи: ' . $signRawTransaction->error->message);
						return $this->redirectBack();
					}

				}
			}
		}
	}

    /**
     * @return string
     */
    public function actionRawUnconfirmed()
	{	
		$session = Yii::$app->session;

		$model = new RawForm();

		$command = new \Nbobtc\Command\Command('listaccounts', [1, true]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listAccounts = json_decode($response->getBody()->getContents());

		$command = new \Nbobtc\Command\Command('listtransactions');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listTransactions = json_decode($response->getBody()->getContents());

		$accounts = (array)$listAccounts->result;
		$transactions = $listTransactions->result;

		$command = new \Nbobtc\Command\Command('listunspent', [0, 0]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listUnspent = json_decode($response->getBody()->getContents());

		$unspent = (array)$listUnspent->result;

		if (Yii::$app->request->isAjax) {

			$post = Yii::$app->request->post();
			$post = $post['RawForm'];

			$fee = (float)$post['fee'];
			if (empty($fee)) {
				$fee = $model->fee;
			}

			$limitSmallChange = (float)$post['limitSmallChange'];
			$addressSmallChange = $post['addressSmallChange'];
			$addressBigChange = $post['addressBigChange'];

			$txs = $post['ins'];
			$outAddresses = $post['outAddresses'] ?: [];
			$outAmounts = $post['outAmounts'];
			$ins = [];
			$insAmount = 0;

			foreach ($unspent as $i => $tx) {
				if (is_array($txs) and !empty($txs) and array_key_exists($tx->txid.':'.$tx->vout, $txs)) {
					if ((int)$txs[$tx->txid.':'.$tx->vout] == 1) {
						$in = new \stdClass();
						$in->txid = $tx->txid;
						$in->vout = $tx->vout;
						$ins[] = $in;
						$insAmount += (float)$tx->amount;
					}
				}
			}

			$session->set('ins', $ins);

			$outs = [];
			$outsAmount = 0;

            foreach ($outAddresses as $i => $address) {
                if (array_key_exists($i, $outAmounts)) {
                    if (isset($outs[$address])) {
                        $outs[$address] += round($outAmounts[$i], 8);
                    } else {
                        $outs[$address] = round($outAmounts[$i], 8);
                    }
                    $outsAmount += round($outAmounts[$i], 8);
                }
            }

			if (empty($limitSmallChange)) {
				Yii::$app->session->addFlash('error', 'Укажите лимит мелкой сдачи!');
				return $this->redirectBack();
			}

			if (empty($addressSmallChange)) {
				Yii::$app->session->addFlash('error', 'Укажите адрес мелкой сдачи!');
				return $this->redirectBack();
			}

			if (empty($addressBigChange)) {
				Yii::$app->session->addFlash('error', 'Укажите адрес крупной сдачи!');
				return $this->redirectBack();
			}

			if (round(($outsAmount + $fee), 8) > round($insAmount, 8)) {
				Yii::$app->session->addFlash('error', 'Сумма входов меньше суммы выходов с комиссией! Сумма входов: ' . $insAmount . '. Сумма выходов с комиссией: ' . ($outsAmount + $fee) . '.');
				return $this->redirectBack();
			} else {
				$changeWithoutFee = $insAmount - ($outsAmount + $fee); // Сдача с вычетом комиссии
				if ($changeWithoutFee < 0) {
					Yii::$app->session->addFlash('error', 'Сдача с вычетом комиссии меньше нуля!');
					return $this->redirectBack();
				}
				if (($changeWithoutFee and $changeWithoutFee > 0)) { // Проверка сдачи, и добавление выходов для сдачи

					if ($changeWithoutFee > 0 and $changeWithoutFee <= trim($limitSmallChange) and !empty(trim($addressSmallChange))) {
						if (isset($outs[$addressSmallChange])) {
							$outs[$addressSmallChange] += round($changeWithoutFee, 8);
						} else {
							$outs[$addressSmallChange] = round($changeWithoutFee, 8);
						}
					} else {
						if ($changeWithoutFee > 0 and !empty($addressBigChange)) {
							if (isset($outs[$addressBigChange])) {
								$outs[$addressBigChange] += round($changeWithoutFee, 8);
							} else {
								$outs[$addressBigChange] = round($changeWithoutFee, 8);
							}
						}
					}
				}
			}

			$session->set('outs', $outs);

			return json_encode(['modal' => 'start', 'outAddresses' => $outAddresses, 'countOuts' => count($outs)]);
		}

		$array = [];
		foreach ($unspent as $i => $row) {
			$item = (array) $row;
			if ($addressBigChange = Setting::getValue('address_big_change') and $item['address'] == $addressBigChange) {
				$array[$i] = $item;
			}
		}

		$sort = new \yii\data\Sort([
		    'attributes' => [
				'address',
				'txid',
		        'confirmations',
		        'amount',
		        'vout',
		    ],
		]);

		$dataProvider = new ArrayDataProvider([
		    'allModels' => (array)$array,
		    'sort' => $sort,
		    'pagination' => [
		    	'pageSize' => 100
		    ],
		]);

		$user = User::findByUsername('admin');
		$token = $user->auth_key;

		return $this->render('raw-unconfirmed', compact('token', 'model', 'accounts', 'transactions', 'unspent', 'dataProvider'));
	}

}
