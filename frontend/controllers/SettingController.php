<?php

namespace frontend\controllers;

use Yii;
use common\models\Setting;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class SettingController extends Controller
{

	public function actionIndex()
	{
		$dataProvider = new ActiveDataProvider([
			'query' => Setting::find(),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	public function actionCreate()
	{
		$model = new Setting();

		if ($model->load(Yii::$app->request->post())) {
			if ($model->validate()) {
				if ($model->save()) {
					return $this->redirect(['view', 'id' => $model->id]);
				}
			} else {
				foreach ($model->errors as $error) {
					Yii::$app->session->addFlash('error', $error);
					return $this->redirect(['create']);
				}
			}
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		$redirect = false;
		if (Yii::$app->request->post('redirect')) {
			$redirect = Yii::$app->request->post('redirect');
		}

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			
			Yii::$app->session->set('setting.' . $model->name, $model->value);

			if ($redirect)
				return $this->redirect($redirect);
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	protected function findModel($id)
	{
		if (($model = Setting::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
