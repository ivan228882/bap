<?php

namespace frontend\controllers;

use Yii;
use common\models\Wallet;
use frontend\models\Address;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class AddressController extends Controller
{

	public function actionIndex()
	{
		$command = new \Nbobtc\Command\Command('getaddressesbyaccount', ['']);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$getaddressesbyaccount = $response->getBody()->getContents();
		$addresses = $getaddressesbyaccount->result;

		return $this->render('index', compact('addresses'));
	}

	public function actionTest()
	{
		var_dump(Address::create2('test', time()));
	}

	public function actionNew()
	{
		$address = null;

		if (Yii::$app->request->isPost) {
			$post = Yii::$app->request->post();
			
			$command = new \Nbobtc\Command\Command('getnewaddress', ['']);
			$response = Yii::$app->bitcoinClient->sendCommand($command);
			$getnewaddress = json_decode($response->getBody()->getContents());
			$address = $getnewaddress->result;

			if (isset($post['as_wallet']) && $post['as_wallet'] == 1) {
				$wallet = new Wallet();
				$wallet->name = $post['wallet_name'];
				$wallet->index = Wallet::find()->max('`index`') + 1;
				$wallet->address = $address;
				$wallet->save();
			}
		}

		return $this->render('new', compact('address'));
	}

	public function actionValidate()
	{
		$address = null;
		$result = null;
		if (Yii::$app->request->isPost) {
			$address = Yii::$app->request->post('address');
			$command = new \Nbobtc\Command\Command('validateaddress', $address);
			$response = Yii::$app->bitcoinClient->sendCommand($command);
			$result = json_decode($response->getBody()->getContents());
		}

		return $this->render('validate', compact('address', 'result'));
		
	}

}
