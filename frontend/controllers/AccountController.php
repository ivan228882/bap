<?php

namespace frontend\controllers;

use Yii;
//use common\models\Wallet;
use common\models\Address;
//use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
//use yii\web\NotFoundHttpException;
//use yii\filters\VerbFilter;
use yii\data\Sort;

class AccountController extends Controller
{

	public function actionIndex()
	{
		$command = new \Nbobtc\Command\Command('listaccounts');
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$listaccounts = json_decode($response->getBody()->getContents());
		$accounts = $listaccounts->result;

		$accountsTable = [];

		$i = 0;
		foreach ($accounts as $name => $balance) :
			$command = new \Nbobtc\Command\Command('getaccountaddress', [$name]);
			$response = Yii::$app->bitcoinClient->sendCommand($command);
			$getaccountaddress = json_decode($response->getBody()->getContents());

			$accountsTable[$i]['accountAddress'] = $getaccountaddress->result;
			$accountsTable[$i]['name'] = (empty($name) or $name == '_empty_') ? 'Без названия' : $name;
			$accountsTable[$i]['balance'] = $balance;

			$date = Address::find()
					->where(['address' => $accountsTable[$i]['accountAddress']])
					->one();
					
			if($date) {
				$accountsTable[$i]['dateSort'] = $date->created_at;
				$accountsTable[$i]['date'] =  gmdate("d.m.y\ H:i", $date);	
			} else {
				$accountsTable[$i]['date'] = 'Без даты';
				$accountsTable[$i]['dateSort'] = 0;	
			}
			
			$i++;
		endforeach;

		$sortAccounts = new Sort([
			'defaultOrder' => [
				'date' => SORT_DESC,
			],
	        'attributes' => [
					'name' => [
						'asc' => ['name' => SORT_ASC],
		                'desc' => ['name' => SORT_DESC],
		                'default' => SORT_ASC,
	            	],
	            	'balance' => [
						'asc' => ['balance' => SORT_ASC],
		                'desc' => ['balance' => SORT_DESC],
		                'default' => SORT_ASC,
	            	],
	            	'accountAddress' => [
						'asc' => ['accountAddress' => SORT_ASC],
		                'desc' => ['accountAddress' => SORT_DESC],
		                'default' => SORT_ASC,
	            	],
	            	'date' => [
						'asc' => ['dateSort' => SORT_ASC],
		                'desc' => ['dateSort' => SORT_DESC],
		                'default' => SORT_ASC,
	            	],
				],
	    ]);

		$providerAccounts = new ArrayDataProvider([
			'allModels' => $accountsTable,
			'pagination' => [
				'pageSize' => 10,
			],
			'sort' => $sortAccounts,
			
		]);

		return $this->render('index', compact('providerAccounts'/*, 'providerAddress'*/));
	}

	public function actionCreate()
	{
		if (Yii::$app->request->isPost) {
			$name = Yii::$app->request->post('name');

			$command = new \Nbobtc\Command\Command('getnewaddress', [$name]);
			$response = Yii::$app->bitcoinClient->sendCommand($command);
			$getnewaddress = json_decode($response->getBody()->getContents());
			$address = $getnewaddress->result;
			Yii::$app->session->addFlash('success', 'Адрес аккаунта: ' . $address);
			return $this->redirectBack();
		}

		return $this->render('create');
	}

	public function actionGet($address)
	{
		$command = new \Nbobtc\Command\Command('getaccount', $address);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$accounts = $response->getBody()->getContents();
		var_dump($accounts);

		//return $this->render('index');
	}

	public function actionAddresses($account = '')
	{
		$addresses = [];
		
		$command = new \Nbobtc\Command\Command('getaddressesbyaccount', [$account]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$getaddressesbyaccount = json_decode($response->getBody()->getContents());
		$addresses = $getaddressesbyaccount->result;

		$provider = new ArrayDataProvider([
			'allModels' => $addresses,
			'pagination' => [
				'pageSize' => 10,
			],
		]);
		$addresses = $provider->getModels();
		$pages = $provider->getPagination();

		if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_addresses', compact('addresses', 'pages'));
        } else {
            return $this->render('_addresses', compact('addresses', 'pages'));
        }
	}

}
