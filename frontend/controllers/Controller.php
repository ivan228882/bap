<?php

namespace frontend\controllers;

use Yii;
use common\models\Setting;
use yii\web\Controller as YiiController;
use yii\web\NotFoundHttpException;

class Controller extends YiiController
{

    public function init()
    {
        if (!Yii::$app->session->has('setting.fee')) {
            foreach (Setting::find()->all() as $setting)
                Yii::$app->session->set('setting.' . $setting->name, $setting->value);
        }
    }

    public function redirectBack()
    {
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

}
