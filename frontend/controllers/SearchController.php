<?php

namespace frontend\controllers;

use Yii;
use common\models\Wallet;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class SearchController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index');
    }

}
