<?php

namespace frontend\controllers;

use Yii;
use common\models\Wallet;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class InfoController extends Controller
{

    public function actionIndex()
    {
        if (Yii::$app->request->isPost) {
            $to = Yii::$app->request->post('to');
            $amount = Yii::$app->request->post('amount');

            $command = new \Nbobtc\Command\Command('sendtoaddress', [$to, $amount, 'Test send to address']);
            $response = Yii::$app->bitcoinClient->sendCommand($command);
            $sendtoaddress = json_decode($response->getBody()->getContents());

            var_dump($sendtoaddress->result);
            die;
        }

        return $this->render('index');
    }

    public function actionListUnspent()
    {
        $command = new \Nbobtc\Command\Command('listunspent');
        $response = Yii::$app->bitcoinClient->sendCommand($command);
        $listUnspent = json_decode($response->getBody()->getContents());

        return $this->render('list-unspent', compact('listUnspent'));
    }

}
