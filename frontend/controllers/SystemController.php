<?php

namespace frontend\controllers;

use Yii;

class SystemController extends Controller
{

    public function actionCpuLoad($index = 0) 
    {
        $id = 'cpu_load';
        $percent = $this->getServerLoad($index);
        return $this->render('progress-bar', compact('id', 'percent'));
    }

    private function getServerLoad($index = 0)
    {
        if (stristr(PHP_OS, 'win')) {
            $wmi = new \COM("Winmgmts://");
            $server = $wmi->execquery("SELECT LoadPercentage FROM Win32_Processor");
            $cpu_num = 0;
            $load_total = 0;
            foreach ($server as $cpu) {
                $cpu_num++;
                $load_total += $cpu->loadpercentage;
            }
            $load = round($load_total / $cpu_num);
        } else {
            $sys_load = sys_getloadavg();
            $load = $sys_load[($index > 3 or $index < 0) ? 0 : $index];
        }
        return (int) $load;
    }

}
