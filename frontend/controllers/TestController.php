<?php

namespace frontend\controllers;

use Yii;
use common\components\calculate\MinChange;
use common\components\calculate\MinFee;
use common\components\calculate\MinPiece;
use common\components\calculate\BigPiece;
/**
 * WalletController implements the CRUD actions for Wallet model.
 */
class TestController extends Controller
{

    /**
     * Lists all Wallet models.
     * @return mixed
     */


     public function methodSumm($testArrOut,$testSumm,$error) 
    {

        if ($testArrOut['sums'] !== $testSumm || $testArrOut['sums'] < $testArrOut['sum']) {
            $error['sums'] = 'Summ its bad';
        } 

        return $error;
    }

    public function methodSumsWithFee($testArrOut,$testSumm,$error) 
    {

        if ($testArrOut['sumsWithFee'] !== round($testSumm+$testArrOut['fees'],8)) {
            $error['sumsWithFee'] = 'sumsWithFee its bad';
        } 

        return $error;
    }

    public function methodSurrenderr($testArrOut,$error) 
    {

        $t['surr'] = round(($testArrOut['sums'] - $testArrOut['sum']),8);
        if ($testArrOut['sums'] < $testArrOut['sum'] || $testArrOut['surrender'] !== $t['surr']) {
            $error['surrender'] = 'surrender its bad';
        }

        return $error;
    }

     public function methodSurrenderrWithoutFee($testArrOut,$error) 
     {

        if ($testArrOut['surrenderWithoutFee'] < 0 || $testArrOut['surrenderWithoutFee'] !== round(($testArrOut['surrender'] - $testArrOut['fees']),8) ) {
            $error['surrenderWithoutFee'] = 'surrender its bad';
        } 

        return $error;
    }

    public function goTesting($testArrOut) 
    {

       $error = false;

       $testSumm = 0;
       foreach ($testArrOut['ins'] as $key => $value) {
           $testSumm += $value->amount;
        }
        $testSumm = round($testSumm,8);

       $error = $this->methodSumm($testArrOut,$testSumm,$error);
       $error = $this->methodSumsWithFee($testArrOut,$testSumm,$error);
       $error = $this->methodSurrenderr($testArrOut,$error);
       $error = $this->methodSurrenderrWithoutFee($testArrOut,$error);
       
       return $error;
    }

    public function testMinFee()
    {

        $outAmount = 1;
        $minChange = new MinFee( $outAmount );
        $testArrOut = $minChange->calculate();
        var_dump($testArrOut);
        $error = $this->goTesting($testArrOut);
        if(!$error) {
            $test = true;
        } else {
            $test = false;
        }
        var_dump($error);
    }

     public function testMinPiece()
    {

        $outAmount = 1;
        $minChange = new MinPiece( $outAmount );
        $testArrOut = $minChange->calculate();
        
        $error = $this->goTesting($testArrOut);
        if(!$error) {
            $test = true;
        } else {
            $test = false;
        }
        var_dump($test);
    }

     public function testBigPiece()
    {

        $outAmount = 1;
        $minChange = new BigPiece( $outAmount );
        $testArrOut = $minChange->calculate();
        
        $error = $this->goTesting($testArrOut);
    
        if(!$error) {
            $test = true;
        } else {
            $test = false;
        }
           
    }

     public function testMinChange()
    {

        $outAmount = 1;
        $minChange = new MinChange( $outAmount );
        $testArrOut = $minChange->calculate();
        
        $error = $this->goTesting($testArrOut);
        if(!$error) {
            $test = true;
        } else {
            $test = false;
        }

    }

    public function actionIndex()
    {
       $this->testMinChange();
        


        return $this->render('index', [
            'data' => $data,
        ]);
    }

}
