<?php
namespace frontend\assets;
use yii\web\AssetBundle;
class CourseAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets';
    public $css = [
        '',
    ];
    public $js = [
        'https://code.jquery.com/jquery-3.3.1.min.js',
        'js/course.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}