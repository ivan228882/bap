var clipboard = new Clipboard('.btn');

$(document).ready(function () {
	$('[data-toggle="popover"]').popover().click(function(e) {
	    $(this).popover('toggle');
	    e.stopPropagation();
	});
});

$(document).click(function(e) {
    if (!$(e.target).is('.popover, .popover-title, .popover-content')) {
        $('[data-toggle="popover"]').popover('hide');
    }
});

function calculateSize(inputs, outputs)
{
	var min = ((inputs * 180) + (outputs * 34) + 10);
	var max = ((inputs * 180) + (outputs * 34) + 10);
	return {
		"min": min,
		"max": max,
	}
}
function calcSatoshi()
{
    var fee = $('#input_fee').val();
    var sizes = calculateSize(countInputs, countOutputs);
    var satoshi = (fee * 100000000) / sizes['max'];
    $('#satoshi').text(satoshi.toFixed(8));
}

function toBTC(currency, value)
{
	var result = 0;
	
	$.ajax({
        url: 'https://blockchain.info/tobtc',
        data: {
        	currency: currency, 
        	value: value
        },
        type: 'get',
        dataType: 'html',
        async: false,
        success: function(data) {
            result = data;
        }
    });

	return result;
}

$('#calculate_rates #amount').on('input', function (e) {
	var amount = $(this).val();

	var currency = $('#calculate_rates #currency option:selected').val();

	var inBTC = toBTC(currency, amount);

	$('#calculate_rates #inBTC').text(inBTC);
});

$('#calculate_rates #currency').on('change', function (e) {
	var amount = $('#calculate_rates #amount').val();

	var currency = $('#calculate_rates #currency option:selected').val();

	var inBTC = toBTC(currency, amount);

	$('#calculate_rates #inBTC').text(inBTC);
});