#!/bin/bash
echo "Testscript run succesful"

cd /var/www/bap

/usr/bin/git config --global user.name "Vlad Malinichev"
/usr/bin/git config --global user.email "malinichev.ru@gmail.com"

/usr/bin/git checkout -f
/usr/bin/git pull origin master
#/usr/bin/git fetch --all
#/usr/bin/git checkout --force "origin/master"

rm -R /var/www/bap/frontend/runtime/cache/*
rm -R /var/www/bap/api/runtime/cache/*

sudo chown vlad:www-data /var/www/bap/frontend/web/index.php
sudo chown vlad:www-data /var/www/bap/api/web/index.php

#composer self-update
#composer global require "fxp/composer-asset-plugin"
#composer clear-cache
#sudo composer update