var btcAccessToken = 'lT_LpXRMjSN6MCpsWgbXtEvMveopl7aR';

$(document).ready(function() {

	/*-----------------------------------/
	/*	TOP NAVIGATION AND LAYOUT
	/*----------------------------------*/

	$('.btn-toggle-fullwidth').on('click', function() {
		if (!$('body').hasClass('layout-fullwidth')) {
			$('body').addClass('layout-fullwidth');

		} else {
			$('body').removeClass('layout-fullwidth');
			$('body').removeClass('layout-default'); // also remove default behaviour if set
		}

		$(this).find('.lnr').toggleClass('lnr-arrow-left-circle lnr-arrow-right-circle');

		if ($(window).innerWidth() < 1025) {
			if(!$('body').hasClass('offcanvas-active')) {
				$('body').addClass('offcanvas-active');
			} else {
				$('body').removeClass('offcanvas-active');
			}
		}
	});

	$(window).on('load', function() {
		if($(window).innerWidth() < 1025) {
			$('.btn-toggle-fullwidth').find('.icon-arrows')
			.removeClass('icon-arrows-move-left')
			.addClass('icon-arrows-move-right');
		}

		// adjust right sidebar top position
		$('.right-sidebar').css('top', $('.navbar').innerHeight());

		// if page has content-menu, set top padding of main-content
		if($('.has-content-menu').length > 0) {
			$('.navbar + .main-content').css('padding-top', $('.navbar').innerHeight());
		}

		// for shorter main content
		if($('.main').height() < $('#sidebar-nav').height()) {
			$('.main').css('min-height', $('#sidebar-nav').height());
		}
	});


	/*-----------------------------------/
	/*	SIDEBAR NAVIGATION
	/*----------------------------------*/

	$('.sidebar a[data-toggle="collapse"]').on('click', function() {
		if($(this).hasClass('collapsed')) {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	});

	if( $('.sidebar-scroll').length > 0 ) {
		$('.sidebar-scroll').slimScroll({
			height: '95%',
			wheelStep: 2,
		});
	}


	/*-----------------------------------/
	/*	PANEL FUNCTIONS
	/*----------------------------------*/

	// panel remove
	$('.panel .btn-remove').click(function(e){

		e.preventDefault();
		$(this).parents('.panel').fadeOut(300, function(){
			$(this).remove();
		});
	});

	// panel collapse/expand
	var affectedElement = $('.panel-body');

	$('.panel .btn-toggle-collapse').clickToggle(
		function(e) {
			e.preventDefault();

			// if has scroll
			if( $(this).parents('.panel').find('.slimScrollDiv').length > 0 ) {
				affectedElement = $('.slimScrollDiv');
			}

			$(this).parents('.panel').find(affectedElement).slideUp(300);
			$(this).find('i.lnr-chevron-up').toggleClass('lnr-chevron-down');
		},
		function(e) {
			e.preventDefault();

			// if has scroll
			if( $(this).parents('.panel').find('.slimScrollDiv').length > 0 ) {
				affectedElement = $('.slimScrollDiv');
			}

			$(this).parents('.panel').find(affectedElement).slideDown(300);
			$(this).find('i.lnr-chevron-up').toggleClass('lnr-chevron-down');
		}
	);


	/*-----------------------------------/
	/*	PANEL SCROLLING
	/*----------------------------------*/

	if ($('.panel-scrolling').length > 0) {
		$('.panel-scrolling .panel-body').slimScroll({
			height: '430px',
			wheelStep: 2,
		});
	}

	if ($('#panel-scrolling-demo').length > 0) {
		$('#panel-scrolling-demo .panel-body').slimScroll({
			height: '175px',
			wheelStep: 2,
		});
	}

	/*-----------------------------------/
	/*	TODO LIST
	/*----------------------------------*/

	$('.todo-list input').change(function() {
		if ($(this).prop('checked')) {
			$(this).parents('li').addClass('completed');
		} else {
			$(this).parents('li').removeClass('completed');
		}
	});


	/*-----------------------------------/
	/* TOASTR NOTIFICATION
	/*----------------------------------*/

		toastr.options.timeOut = 10000;
		toastr.options.showDuration = true;
		toastr.options.closeDuration = 500;
		toastr.options.newestOnTop = false;
		toastr.options.closeButton = true;
		toastr.options.progressBar = true;

		$('.btn-toastr').on('click', function() {
			$context = $(this).data('context');
			$message = $(this).data('message');
			$position = $(this).data('position');

			if ($context == '') {
				$context = 'info';
			}

			if ($position == '') {
				$positionClass = 'toast-left-top';
			} else {
				$positionClass = 'toast-' + $position;
			}

			//toastr.remove();
			toastr[$context]($message, '' , { positionClass: $positionClass });
		});

		$('#toastr-callback1').on('click', function() {
			$message = $(this).data('message');

			toastr.options = {
				"timeOut": "300",
				"onShown": function() { alert('onShown callback'); },
				"onHidden": function() { alert('onHidden callback'); }
			}

			toastr['info']($message);
		});

		$('#toastr-callback2').on('click', function() {
			$message = $(this).data('message');

			toastr.options = {
				"timeOut": "10000",
				"onclick": function() { alert('onclick callback'); },
			}

			toastr['info']($message);

		});

		$('#toastr-callback3').on('click', function() {
			$message = $(this).data('message');

			toastr.options = {
				"timeOut": "10000",
				"closeButton": true,
				"onCloseClick": function() { alert('onCloseClick callback'); }
			}

			toastr['info']($message);
		});
});

// toggle function
$.fn.clickToggle = function( f1, f2 ) {
	return this.each( function() {
		var clicked = false;
		$(this).bind('click', function() {
			if (clicked) {
				clicked = false;
				return f2.apply(this, arguments);
			}

			clicked = true;
			return f1.apply(this, arguments);
		});
	});

}

//frontend select method

$(document).ready(function(){   

	var selectMethod = undefined;
	var routeMethod = undefined;
	var body = $("body");
	var countIns = 0;
	var countOuts = 1;
	var addressIN = [];
	var addressOUT = [];

	getAjaxRecomended();

	function countElements()
	{
		countIns = ($('table#table-ins tbody tr input[type=checkbox]:checked').length > 0) ? $('table#table-ins tbody tr input[type=checkbox]:checked').length : 0;
		countOuts = ($('.outs > div').length > 0) ? $('.outs > div').length : 0;
		if (parseFloat($('#changeSumWithFee').text()) > 0) {
			countOuts += 1;
		}
		$('#countIns').text(countIns);
		$('#countOuts').text(countOuts);
	}

	function sumIns()
	{
		countElements();
		var inSum = 0;
		$('#table-ins tbody tr input[type=checkbox]:checked').each(function(index, input) {
			var tr = $(input).parent().parent().parent().parent().parent();
			var balanceTd = parseFloat($(tr).find('td:eq(4)').text());
			inSum += balanceTd;
		});
		$('#inSum').text(inSum);
	}

	function sumOuts()
	{
		countElements();
		outAmount = 0;
		$.each($('.out-amount'), function (i, element) {
			if ($(element).find('input').val() !== "") {
				outAmount += parseFloat($(element).find('input').val());	
			}
		});
		$('#outSum').text(outAmount);
	}

	function calculateSize(inputs, outputs)
	{
		$('#size').text(((inputs * 180) + (outputs * 34) + 10));
		return {
			"min": ((inputs * 180) + (outputs * 34) + 10),
			"max": ((inputs * 180) + (outputs * 34) + 10),
		}
	}

	function calculateSurrender()
	{
		var outAmount = 0;
		var inAmount = 0;
		var surrender = 0;
		inAmount = parseFloat($('#inSum').text()).toFixed(8);
		outAmount = parseFloat($('#outSum').text()).toFixed(8);
		if (inAmount > 0 && outAmount >= 0) {
			surrender = inAmount - outAmount;
			return surrender.toFixed(8);
		} else {
			console.log('Сумма меньше');
			return (inAmount - outAmount);
		}
	}

	function calcSatoshi()
	{
		countElements();
		var fee = $('#rawform-fee').val();
		if (countOuts > 0 && countIns > 0) {
			var sizes = calculateSize(countIns, countOuts);
			var satoshi = (fee * 100000000) / sizes['min'];
			$('#satoshi').text(satoshi.toFixed(8));
			$('#rawform-feesatoshi').val(satoshi.toFixed(8));
		} else {
			$('#satoshi').text(0);
			$('#rawform-feesatoshi').val(0);
		}
	}

	function calcFee()
	{
		var size = calculateSize(countIns, countOuts);
		var fee = ((parseFloat($('#rawform-feesatoshi').val() * size['min'])) / 100000000).toFixed(8);
		$('#rawform-fee').val(fee);
		$('#fee').text(fee);
		surrender = calculateSurrender();
		$('#changeSum').text(surrender);
		var WithFee = (surrender - fee).toFixed(8);
		$('#changeSumWithFee').text(WithFee);
	}

	function getFee()
	{
		countElements();
		var satoshi = $('#rawform-feesatoshi').val();
		if (satoshi) {
			var size = calculateSize(countIns, countOuts);
			var fee = ((satoshi * size['min']) / 100000000).toFixed(8);
		} else {
			var fee = $('#rawform-fee').val();
		}
		return fee;
	}

	$(document).on('change', '#rawform-fee', function (e) {
		countElements();
		$('#fee').text($(this).val());
		calcSatoshi();
		surrender = calculateSurrender();
		$('#changeSum').text(surrender);
		var WithFee = (surrender - $(this).val()).toFixed(8);
		$('#changeSumWithFee').text(WithFee);
	});

	$(document).on('change', '#rawform-feesatoshi', function (e) {
		countElements();
		$('#satoshi').text($(this).val());
		var size = calculateSize(countIns, countOuts);
		var fee = ((parseFloat($(this).val() * size['min'])) / 100000000).toFixed(8);
		$('#rawform-fee').val(fee);
		$('#fee').text(fee);
		calcFee();
		surrender = calculateSurrender();
		$('#changeSum').text(surrender);
		var WithFee = (surrender - fee).toFixed(8);
		$('#changeSumWithFee').text(WithFee);
	});

	$(document).on('click', 'table tbody input[type=checkbox]', function (e) {
		sumIns();
		calcFee();
		surrender = calculateSurrender();
		$('#changeSum').text(surrender);
		var WithFee = (surrender - parseFloat($('#rawform-fee').val())).toFixed(8)
		$('#changeSumWithFee').text(WithFee);
	});

	function getMethodRoute() {
		switch (selectMethod) {
		  case 'manual':
			selectMethod = 'manual';
			$('#table-ins tr').show();
			$('#table-ins tr input').attr("disabled", false);
			break;
		  case 'auto_min_change':
			selectMethod = 'min-change';
			break;
		  case 'auto_small_piece':
			selectMethod = 'min-piece';
			break;
		  case 'auto_big_piece':
			selectMethod = 'big-piece';
			break;
		  default:
			selectMethod = false;
		}

		if (selectMethod !== false) {
			if (location.hostname == 'bitcoin.loc') {
				var urlStr = 'http://api.bitcoin.loc/v2/calculate/'+selectMethod+'?_format=json';
			} else {
				var urlStr = 'https://api.torbithammer2216.7money.co/v2/calculate/' + selectMethod + '?access-token=' + btcAccessToken + '&_format=json';
			}
		} else {
			var urlStr = '404';
		}
		
		return urlStr;

	}
			
	if ($("#selection_method").length > 0) {
		selectMethod = 'manual';
	}

	$("#selection_method").change(function () {
		selectMethod = $(this).val();
		routeMethod = getMethodRoute();//param selectMethod
		countElements();
		sumIns();
		sumOuts();
		calculateSurrender();
		goFinalAjax();
	});

	function address()
	{

		function cloneOut()
		{

			if ($('.outs').find('div.out').hasClass('out')) {
				var clone = $('.out:first').clone();
				clone.find('.outaddresses-input input').val(0);
				clone.find('input').val('');
				clone.appendTo($('.outs'));
				$("html, body").animate({scrollTop: $('.outs div:last').offset().top - 30});
			}

		}

		function removeAddress()
		{

			$(document).on('click', '.remove-out', function (e) {
				$(this).parent().parent().remove();
				countOuts--;
				calculateInAmount()
				calculateOutAmount();
				countElements();
				calcSatoshi();
			});

		}
		
		function addAddress()
		{
			
			$(document).on('click', '#add-out-address', function (e) {
				e.preventDefault();
				countOuts++;
				cloneOut();
				calcSatoshi();
				countElements();
				calculateOutAmount();
			});

		}
		
		addAddress();
		removeAddress();
		countElements();
		calcSatoshi();
	}

	function calculateOutAmount()
	{
		outAmount = 0;
		$.each($('.out-amount'), function (i, element) {
			if ($(element).find('input').val() !== "") {
				outAmount += parseFloat($(element).find('input').val());	
			}
		});
		$('#outSum').text(outAmount);
		return outAmount;
	}

	function checkTableTransaction()
	{
		var checkVal;
		var objVal;
		var thisChek;

		function findInArrTxid()
		{

			for (key in objIns) {
				if (checkVal == objIns[key].txid+':'+objIns[key].vout) {
					
					return checkVal;
				} 
			}

			return false;	

		}

		$('table tbody tr').each(function(index, tr) {
			function goCheckByTxid()
			{
				thisChek = $(tr).find('input[type="checkbox"]').data('txid',checkVal);
				thisChek.closest('div').find('input[type="hidden"]').val("1");
				thisChek.prop('checked', true);
				thisChek.attr('checked', true);
				thisChek.val(1);
				//thisChek.attr("disabled", true);
				$(tr).css('display', 'table-row');
				$(tr).find('td').css('background', 'lightblue');
			}

			function goUnCheckByTxid()
			{
				thisChek = $(tr).find('input[type="checkbox"]').data('txid',checkVal);
				thisChek.prop('checked', false);
				//thisChek.attr("disabled", true);
				$(tr).css('display', 'none');
				$(tr).find('td').css('background', 'none');
			}

			if ($(tr).find('input[type="checkbox"]').data('txid')) {

				checkVal = $(tr).find('input[type="checkbox"]').data('txid');
				
				if (findInArrTxid()) {
					goCheckByTxid();
				} else {
					goUnCheckByTxid();
				}

			}

		});
	}

	function validAddressesAndAmount()
	{
		if (classValidName == "") {
			console.log('classValidName Note found');
			return undefined;
		} else {
			var validVal = true;
			$.each($(classValidName), function (i, element) {
				if ($(element).find('input').val() == "") {
					validVal = false;
				}
			});
			return validVal;
		}
	}

	//address();

	var objIns;//массив входов(объект)
	var surrender = 0;
	var dataGlobal;
	var outAmount;
	var needSum;
	var classValidName = '';

	function goAjaxCalculate()
	{
		$.ajax({
			url: routeMethod, 
			type:'GET',            
			dataType : "json",
			data: "sum=" + outAmount + '&fee=' + getFee(),
			success: function (data, textStatus) {
				console.log(data);
				if (data == "#1") {
					alert("Необходимая сумма превышает сумму в наличии!")
				} else {
					dataGlobal = data;
					$("#inSum").text(data.sum);
					var fee = $("#rawform-fee").val();
					surrender = calculateSurrender();
					$('#changeSum').text(surrender);
					var WithFee = (surrender - fee).toFixed(8);
					$('#changeSumWithFee').text(WithFee);

					objIns = data.ins;
					checkTableTransaction();
					calcFee();
					countElements();
					sumIns();
					sumOuts();
				}
			}               
		}); 
	}


	function getAjaxRecomended()
	{
		$.ajax({
			url: 'https://bitaps.com/api/fee',
			dataType : "json",
			success: function (data, textStatus) {
				var str ='<span data-id="fee"><span style="color: #6299D3">'+data.high+'</span> / ';
				str +='<span style="color: #4CAF50">'+data.medium+'</span> / '+data.low+'</span>';
				$("#recommendFee").html(str);
			}               
		});
	}

	function goFinalAjax()
	{
		var vaildo = true;
		outAmount = parseFloat($('#outSum').text()).toFixed(8);
	
		if (vaildo) {
			if (selectMethod == 'manual') {
				goAjaxCalculate();
				getAjaxRecomended();
			} else {
				goAjaxCalculate();
				getAjaxRecomended();
			}
			sumIns();
			sumOuts();
		}
	}

	$('button[name="send-raw-button"]').click(function (e) {  
		e.preventDefault();
		if (routeMethod == 'manual') {

			function validate_manual() {

				var testManual;

				testManual = $('input[name="RawForm[limitSmallChange]"]');
				if (testManual.val() <=0 || isNaN(testManual) || testManual === undefined || testManual != "") {
					alert('Введите Лимит мелкой сдачи');
					return false;
				}
				
				testManual = $('input[name="RawForm[addressSmallChange]"]');
				if (testManual === undefined || isNaN(testManual) || testManual != "") {
					alert('Введите Адрес мелкой сдачи');
					return false;
				}

				testManual = $('input[name="RawForm[addressBigChange]"]');
				if (testManual === undefined || isNaN(testManual) || testManual != "") {
					alert('Введите Адрес крупной сдачи');
					return false;
				}
				return true;
			}
		} else {
			goFinalAjax();
		}
	}); 

	$(document).on('input change', '.out-amount input', function (e) {
		
		selectMethod = $('#selection_method option:selected').val();
		routeMethod = getMethodRoute();//param selectMethod

		countElements();
		calculateSize(countIns, countOuts);
		sumIns();
		sumOuts();
		calcFee();
		surrender = calculateSurrender();
		$('#changeSum').text(surrender);
		var WithFee = (surrender - parseFloat($('#rawform-fee').val())).toFixed(8)
		$('#changeSumWithFee').text(WithFee);

		$('#outSum').text(calculateOutAmount().toFixed(8));

		if (selectMethod != 'manual') {
			goAjaxCalculate();
		}
		
	});

	var urll = window.location.origin;
	$('.send0btn').click(function(e){
		e.preventDefault();
		var i = 0;
		$('table#table-ins tbody tr input[type=checkbox]:checked').each(function(index, input) {
			var tr = $(input).parent().parent().parent().parent().parent();
			addressIN[i] = $(tr).find('td:eq(1)').html();
			i++;
		});

		var data = $('#raw-form').serialize();
		
		$.ajax({
			url: urll + '/send/raw',
			type: 'post',
			data: data,
			success: function (data, textStatus) {
				data = $.parseJSON(data);

				if (data.modal == 'start') {

					var tdPaste = $("#myModalBox").find('.ins-block tbody');
					tdPaste.html('');

					for (var i = addressIN.length - 1; i >= 0; i--) {
						tdPaste.append('<tr><td>'+addressIN[i]+'</td></tr>');
					}

					tdPaste = $("#myModalBox").find('.outs-block tbody');
					tdPaste.html('');

					for (var key in data.outAddresses) {
						tdPaste.append('<tr><td>'+data.outAddresses[key]+'</td></tr>');
					}

					var size = calculateSize(addressIN.length, data.countOuts);

					$(".outSum-m").text($("#outSum").text());
					$(".fee-m").text($("#fee").text());
					$(".satoshi-m").text($("#satoshi").text());
					$(".changeSum-m").text($("#changeSum").text());
					$(".changeSumWithoutFee-m").text($("#changeSumWithFee").text());
					$(".size-m").text(size['min']);

					$("#myModalBox").modal('show');

					$('input[name="RawForm[transactionSubcribe]"]').val(true);
				} else {
					console.log('Что то пошло не так...');
				}
			}               
		});
	});

	$('.send0btn-unconfirmed').click(function(e){
		e.preventDefault();
		var i = 0;
		$('table#table-ins tbody tr input[type=checkbox]:checked').each(function(index, input) {
			var tr = $(input).parent().parent().parent().parent().parent();
			addressIN[i] = $(tr).find('td:eq(1)').html();
			i++;
		});

		var data = $('#raw-form').serialize();
		
		$.ajax({
			url: urll + '/send/raw-unconfirmed',
			type: 'post',
			data: data,
			success: function (data, textStatus) {
				data = $.parseJSON(data);

				if (data.modal == 'start') {

					var tdPaste = $("#myModalBox").find('.ins-block tbody');
					tdPaste.html('');

					for (var i = addressIN.length - 1; i >= 0; i--) {
						tdPaste.append('<tr><td>'+addressIN[i]+'</td></tr>');
					}

					tdPaste = $("#myModalBox").find('.outs-block tbody');
					tdPaste.html('');

					for (var key in data.outAddresses) {
						tdPaste.append('<tr><td>'+data.outAddresses[key]+'</td></tr>');
					}

					var size = calculateSize(addressIN.length, data.countOuts);

					$(".outSum-m").text($("#outSum").text());
					$(".fee-m").text($("#fee").text());
					$(".satoshi-m").text($("#satoshi").text());
					$(".changeSum-m").text($("#changeSum").text());
					$(".changeSumWithoutFee-m").text($("#changeSumWithFee").text());
					$(".size-m").text(size['min']);

					$("#myModalBox").modal('show');

					$('input[name="RawForm[transactionSubcribe]"]').val(true);
				} else {
					console.log('Что то пошло не так...');
				}
			}               
		});
	});

	$('#send-btn').click(function(e){
		e.preventDefault();
		$.ajax({
			url: urll + '/send/send',
			type: 'post',
			data: {
				transactionSubcribe: true,
				redirectUri: $('#rawform-redirecturi').val()
			},
		});
	});

});