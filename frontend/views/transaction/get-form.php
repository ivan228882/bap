<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Поиск транзакции по ID';
?>
	<?php
	$form = ActiveForm::begin([
		'id' => 'get-by-txid',
		'options' => ['class' => 'form-horizontal col-lg-6'],
		'method' => 'GET',
		'action' => ['transaction/get']
	]);

	?>
		<h1><?=$this->title?></h1>
		<div class="form-group has-success">
			<div class="col-xs-12">
			  <label for="">Поиск по ID транзакции</label>
			</div>
			<div class="col-xs-12">
			  <input type="text" name="txid" value="<?=Yii::$app->request->get('txid')?>" class="form-control">
			</div>
		</div>

		<div class="form-group">
			<div class="col-xs-12">
				<?= Html::submitButton('Поиск транзакции', ['class' => 'btn btn-primary']) ?>
			</div>
		</div>
	<?php ActiveForm::end() ?>
	<div class="clearfix"></div>
<?php if (isset($transaction)) : ?>
	<pre><?=json_encode($transaction, JSON_PRETTY_PRINT) ?></pre>
<?php endif; ?>
