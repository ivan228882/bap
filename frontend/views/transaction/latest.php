<?php

// подключаем виджет постраничной разбивки
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

$this->title = 'Транзакции';

?>

<?php Pjax::begin(['id' => 'transactions', 'timeout' => 5000]) ?>

<h1><?=$this->title?></h1>

<table class="table table-hovered table-striped">
	<thead>
		<tr>
			<th>
				Дата
			</th>
			<th>
				Адрес
			</th>
			<th>
				Подтверждения
			</th>
			<th>
				Тип
			</th>
			<th>
				Сумма
			</th>
			<th>
				Метка
			</th>
			<th>
				
			</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($transactions as $transaction) : ?>
		<?php
			$diff = strtotime('now') - $transaction->time;
		?>
		<tr style="background: <?=($transaction->category == 'receive') ? '#7DE2EA' : ($transaction->category == 'move' ? '#E8d39E' : '#E8A39E') ?>">
			<td>
				<?=($diff < 60*60*24*2) ? Yii::$app->formatter->asRelativeTime($transaction->time) : Yii::$app->formatter->asDateTime($transaction->time)?>
			</td>
			<td>
				<?php if (!isset($transaction->address)) : ?>
					<?php if (isset($transaction->category) and $transaction->category == 'move') : ?>
						Между аккаунтами
					<?php endif; ?>
				<?php else : ?>
				<?=$transaction->address?>
				<?php endif; ?>
			</td>
			<td>
				<?=$transaction->confirmations?>
			</td>
			<td>
				<?=($transaction->category == 'receive') ? 'Получено' : 'Отправлено'?>
			</td>
			<td>
				<?=Yii::$app->formatter->asDecimal($transaction->amount, 8)?>
			</td>
			<td>
				<?=isset($transaction->label) ? $transaction->label : '' ?>
			</td>
			<td>
				<a href="/transaction/<?=$transaction->txid?>" title="Просмотр" aria-label="Просмотр" data-pjax="0">
					<span class="glyphicon glyphicon-eye-open"></span>
				</a>

				<a href="https://www.blocktrail.com/tBTC/tx/<?=$transaction->txid?>" title="Просмотр" aria-label="Просмотр" data-pjax="0">
					<span class="glyphicon glyphicon glyphicon-star"></span>
				</a>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<?php
// отображаем постраничную разбивку
echo LinkPager::widget([
    'pagination' => $pages,
]);
?>

<?php Pjax::end() ?>