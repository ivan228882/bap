<?php

$result = $block->result;

//var_dump($result);

$this->title = 'Блок: ' . $result->hash;

?>

<div class="">
	<a href="/block/get?hash=<?=$result->previousblockhash?>" class="btn btn-success pull-left">Предыдущий блок</a>
	<a href="/block/get?hash=<?=$result->nextblockhash?>" class="btn btn-success pull-right">Следующий блок</a>
</div>

<div class="clearfix"></div>

<div class="panel panel-headline">
	<div class="panel-heading">
		<h3 class="panel-title"><?=$this->title?></h3>
		<p class="panel-subtitle">Предыдущий блок: <?=$result->previousblockhash?></p>
		<p class="panel-subtitle">Следующий блок: <?=$result->nextblockhash?></p>
		<p class="panel-subtitle">Время: <?=Yii::$app->formatter->asDateTime($result->time, 'short')?></p>
		<p class="panel-subtitle">Среднее время: <?=Yii::$app->formatter->asDateTime($result->mediantime, 'short')?></p>
		<p class="panel-subtitle">Размер: <?=$result->size?> (в байтах)</p>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="metric">
					<span class="icon"><i class="fa fa-bell"></i></span>
					<p>
						<span class="number"><?=$result->confirmations?></span>
						<span class="title">Подтверждения</span>
					</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="metric">
					<span class="icon"><i class="fa fa-shopping-bag"></i></span>
					<p>
						<span class="number"><?=count($result->tx) ?></span>
						<span class="title">Транзакции</span>
					</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="metric">
					<span class="icon"><i class="fa fa-eye"></i></span>
					<p>
						<span class="number"><?=$result->weight?></span>
						<span class="title">Вес</span>
					</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="metric">
					<span class="icon"><i class="fa fa-bar-chart"></i></span>
					<p>
						<span class="number"><?=$result->height?></span>
						<span class="title">Высота</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

