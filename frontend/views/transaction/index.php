<?php

// подключаем виджет постраничной разбивки
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Html;

use common\components\RecommendFee;
use common\components\Transaction;

$this->title = 'Транзакции';

?>

<h1><?=$this->title?></h1>

<?php Pjax::begin(['id' => 'transactions']) ?>

<?= GridView::widget([
	'dataProvider' => $providerTransations,
	'summary' => '',
	'rowOptions' => function ($model) {
		return ['style' => 'background: ' . (($model->category == 'receive') ? 'rgba(125, 226, 234, .5)' : 'rgba(232, 163, 158, .5)')];
	},
	'columns' => [
		//['class' => 'yii\grid\SerialColumn'],
		[
			'attribute' => 'time',
			'format' => 'raw',
			'label' => 'Время',
			'value' => function ($model) {
				$html = Yii::$app->formatter->asRelativeTime($model->time);
				$html .= '<br />';
				$html .= ($model->category == 'receive') ? '<span class="label label-primary">Получение</span>' : '<span class="label label-warning">Отправка</span>';
				return $html;
			}
		],
		[
			'attribute' => 'address',
			'label' => 'Адрес и TXID',
			'format' => 'raw',
			'value' => function ($model) {
				$html = $model->address;
				$html .= '<br />';
				$html .= '<small>'.$model->txid.'</small>';
				return $html;
			}
		],
		[
			'attribute' => 'confirmations',
			'label' => 'Подтверждения',
			'value' => function ($model) {
				return $model->confirmations;
			}
		],
		[
			'attribute' => 'amount',
			'label' => 'Сумма',
			'value' => function ($model) {
				return number_format($model->amount, 8);
			}
		],
		[
			'attribute' => 'fee',
			'label' => 'Комиссия',
			'format' => 'raw',
			'value' => function ($model) {
				if ($model->raw !== null and $model->raw instanceof \stdClass) {
					$transaction = new Transaction($model->raw);
					$fee = $transaction->getFee();
					$html = number_format($fee['fee'], 8);
					$html .= '<br />';
					$html .= '<strong>' . ceil((($fee['fee'] * 100000000) / $transaction->getVSize())) . ' сатоши/байт</strong>';
					$html .= '<br />';
					$html .= 'Размер: ' . $transaction->getVSize();
					return $html;
				} else {
					return isset($model->fee) ? number_format($model->fee, 8) : '';
				}
			}
		],
		// 'created_at',
		// 'updated_at',

		[
			'class' => 'yii\grid\ActionColumn',
			'template' => '{view} {viewInExplorer}',
			'buttons' => [
				'view' => function ($url, $model) {
					return Html::a(
						'<span class="glyphicon glyphicon-eye-open"></span>', 
						'/transaction/'.$model->txid,
						[
							'data' => [
								'pjax' => 0
							],
							'aria' => [
								'label' => 'Просмотр',
							],
							'title' => 'Просмотр',
						]
					);
				},
				'viewInExplorer' => function ($url, $model) {
					return Html::a(
						'<span class="glyphicon glyphicon glyphicon-star"></span>', 
						'https://blockchain.info/tx/'.$model->txid,
						[
							'data' => [
								'pjax' => 0
							],
							'aria' => [
								'label' => 'Просмотр на внешнем сайте',
							],
							'title' => 'Просмотр на внешнем сайте',
							'target' => '_blank'
						]
					);
				}
			],
		],
	],
]); ?>

<?php  // пагинация
echo LinkPager::widget([
	'pagination' => $pagination,
]);
?>

<?php Pjax::end() ?>

					

