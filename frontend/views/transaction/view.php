<?php

use common\models\Setting;

use common\components\Transaction;

$this->title = 'Транзакция: ' . $transaction->txid;

$transactionObject = new Transaction($transaction);

$ins = $transactionObject->getInputsInfo();
$fee = $transactionObject->getFee();

//echo '<pre>';
//var_dump($transaction, $ins);
//echo '</pre>';
//die;

?>

<div class="panel panel-headline">
	<div class="panel-heading">
		<h3 class="panel-title"><?=$this->title?></h3>
		<?php if (isset($transaction->blockhash)) : ?>
		<p class="panel-subtitle">Блок: <?=$transaction->blockhash?></p>
		<?php else : ?>
		<p><span class="label label-danger">Ещё не подтверждена</span></p>
		<?php endif; ?>
		<p class="panel-subtitle">Хэш: <?=$transaction->hash?></p>
		<p class="panel-subtitle">Версия: <?=$transaction->version?></p>
		<?php if (isset($transaction->time)) : ?>
		<p class="panel-subtitle">Время: <?=Yii::$app->formatter->asDateTime($transaction->time, 'short')?></p>
		<?php endif; ?>
		<?php if (isset($transaction->size)) : ?>
		<p class="panel-subtitle">Размер транзакции: <?=$transaction->size?> (в байтах)</p>
		<?php endif; ?>
		<?php if (isset($transaction->size)) : ?>
		<p class="panel-subtitle">Размер виртуальной транзакции: <?=$transaction->vsize?> (в байтах)</p>
		<?php endif; ?>
		<?php if (isset($transaction->locktime)) : ?>
		<p class="panel-subtitle">Время блокировки транзакции: <?=$transaction->locktime?></p>
		<?php endif; ?>
	</div>
	<div class="panel-body">
		<div class="row">
			<?php if (isset($transaction->confirmations)) : ?>
			<div class="col-md-3">
				<div class="metric">
					<span class="icon"><i class="fa fa-bell"></i></span>
					<p>
						<span class="number"><?=$transaction->confirmations?></span>
						<span class="title">Подтверждения</span>
					</p>
				</div>
			</div>
			<?php endif; ?>
			<?php if (isset($fee) and isset($fee['fee'])) : ?>
			<div class="col-md-3">
				<div class="metric">
					<span class="icon"><i class="fa fa-btc"></i></span>
					<p>
						<span class="number"><?=number_format($fee['fee'], 8)?></span>
						<span class="title">Комиссия</span>
					</p>
				</div>
			</div>
			<?php endif; ?>
			<div class="col-md-3">
				<div class="metric">
					<span class="icon"><i class="fa fa-eye"></i></span>
					<p>
						<span class="number"><?=$transaction->size?></span>
						<span class="title">Размер (в байтах)</span>
					</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="metric">
					<span class="icon"><i class="fa fa-bar-chart"></i></span>
					<p>
						<span class="number"><?=$transaction->vsize?></span>
						<span class="title">Размер виртуальной транзакции (в байтах)</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-headline col-md-5">
	<div class="panel-heading">
		<h3 class="panel-title">Входы</h3>
	</div>
	<div class="panel-body">
	<?php foreach ($transaction->vin as $i => $in) : ?>
		<?php $color = 'red'; ?>
		<?php if (isset($ins) and !empty($ins) and isset($ins['addresses']) and !empty($ins['addresses']) and isset($ins['addresses'][$i]) and !empty($ins['addresses'][$i])) : ?>
			<?php foreach ($ins['addresses'][$i] as $address) : ?>
				<?php
					$amount = (isset($ins['amounts']) and isset($ins['amounts'][$i])) ? $ins['amounts'][$i] : 0;
				?>
				<span><?=$address?> -> <span style="color:<?=$color?>">(<?=$amount?>)</span></span>
			<?php endforeach; ?>
		<?php else : ?>
			<span><?=$in->txid?></span>
		<?php endif; ?>
	<?php endforeach; ?>
	</div>
</div>

<div class="col-md-2">

</div>

<div class="panel panel-headline col-md-5">
	<div class="panel-heading">
		<h3 class="panel-title">Выходы</h3>
	</div>
	<div class="panel-body">
		<?php foreach ($transaction->vout as $out) : ?>
			<?php $color = 'blue'; ?>
			<?php if (isset($out->scriptPubKey) and isset($out->scriptPubKey->addresses)) : ?>
			<?php foreach ($out->scriptPubKey->addresses as $address) : ?>
				<?php
					$style = '';
					if ($value = Setting::getValue('address_small_change')) {
						if ($value == $address)
							$style = 'color: red';
					}
					if ($value = Setting::getValue('address_big_change')) {
						if ($value == $address)
							$style = 'color: red';
					}
				?>
				<span style="<?=$style?>"><?=$address?> -> <span style="color:<?=$color?>">(<?=$out->value?>)</span></span>
			<?php endforeach; ?>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
</div>

