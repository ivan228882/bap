<?php

// подключаем виджет постраничной разбивки
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = 'Список доступных транзакций';

?>

<h1><?=$this->title?></h1>

<?php Pjax::begin(['id' => 'list-unspent']) ?>

<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'tableOptions' => [
		'class' => 'table table-striped table-bordered table-responsive',
		'id' => 'table-ins'
	],
	'columns' => [
		[
			'attribute' => 'address',
			'label' => 'Адрес'
		],
		[
			'attribute' => 'txid',
			'label' => 'Транзакция'
		],
		[
			'attribute' => 'confirmations',
			'label' => 'Подтверждения'
		],
		[
			'attribute' => 'amount',
			'label' => 'Баланс',
			'value' => function ($row) {
				return number_format($row['amount'], 8);
			}
		],
		[
			'attribute' => 'vout',
			'label' => 'Индекс выхода'
		]
	]
]); ?>

<?php Pjax::end() ?>

					

