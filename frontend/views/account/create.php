<?php

// подключаем виджет постраничной разбивки
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = 'Создать аккаунт';

?>

<?php Pjax::begin(['id' => 'create-account']) ?>

<h1><?=$this->title?></h1>

<h4>Если ввести название существуюшего аккаунта - вернётся его адрес!</h4>

<form action="" method="POST" class="form">

	<?php echo Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>

	<div class="row">

		<div class="form-group col-md-4">
			<label for="name">Название</label>
			<input type="text" name="name" id="name" class="form-control" required />
		</div>

		<div class="form-group col-md-2">
			<label for="to">&nbsp;</label>
			<input type="submit" name="create_account" class="btn btn-primary btn-flat btn-block">
		</div>

	</div>
	
</form>

<?php Pjax::end() ?>