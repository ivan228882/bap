<?php

// подключаем виджет постраничной разбивки
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

?>
				<?php Pjax::begin() ?>
					<table class="table table-hovered table-striped">
						<thead>
							<tr>
								<th>
									Аккаунт
								</th>
								<th>
									Адрес
								</th>
								<th>
									Баланс
								</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($addresses as $address) : ?>
							<?php
								$command = new \Nbobtc\Command\Command('getreceivedbyaddress', [$address]);
								$response = Yii::$app->bitcoinClient->sendCommand($command);
								$getreceivedbyaddress = json_decode($response->getBody()->getContents());
							?>
							<tr>
								<td>
									<?=(empty($account) or $account == '_empty_') ? 'Без названия' : $account?>
								</td>
								<td>
									<?=$address?>
								</td>
								<td>
									<?=$getreceivedbyaddress->result?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
					<?php
					// отображаем постраничную разбивку
					echo LinkPager::widget([
					    'pagination' => $pages,
					]);
					?>
				<?php Pjax::end() ?>