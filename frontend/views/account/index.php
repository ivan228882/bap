<?php

// подключаем виджет постраничной разбивки
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\grid\GridView;
use common\components\Helpers;
/* @var $this yii\web\View */

$this->title = 'Информация об аккаунте';

$this->registerJs(<<<JS

$(document).on('click', '.get-account-addresses', function (e) {
	e.preventDefault();

	$.get('/account/addresses', {account: $(this).data('account')}, function (response) {
		console.log(response);
		$('.addresses').html(response);
	});
});

JS
);

?>
<div class="site-index">

	<div class="row">

		<div class="col-md-6">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Аккаунты</h3>
					<div class="right">
						<a href="/account/create"><button type="button" class="btn-create"><i class="lnr lnr-plus-circle"></i></button></a>
						<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
						<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
					</div>
				</div>
				<div class="panel-body">
					<?= GridView::widget([
					    'dataProvider' => $providerAccounts,
					    'columns' => [
					    	['label'=> 'Название',
					        'attribute' => 'name'],
					        ['label'=> 'Баланс',
					        'attribute' =>'balance'],
					        ['label'=> 'Адрес',
					        'attribute' =>'accountAddress'],
					        ['label'=> 'Дата',
					        'attribute' =>'date'],
					    ],
					]) ?>
				</div>
			</div>
		</div>
		<?php if (isset($providerAddress)) : ?>
		<div class="col-md-6">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Адреса</h3>
					<div class="right">
						<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
						<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
					</div>
				</div>
				<div class="panel-body addresses">
					<?// Helpers::dd($providerAddress) ?>
					<?= GridView::widget([
					    'dataProvider' => $providerAddress,
					    'columns' => [
					    	['label'=> 'Аккаунт',
					        'attribute' => 'account'],
					        ['label'=> 'Адрес',
					        'attribute' =>'address'],
					        ['label'=> 'Баланс',
					        'attribute' =>'receive'],
					    ],
					]) ?>
				</div>
			</div>
		</div>
		<?php endif; ?>

	</div>
</div>
