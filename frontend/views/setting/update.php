<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Wallet */

$this->title = Yii::t('app', 'Редактирование настройки: ' . $model->name);

?>
<div class="container-fluid">
    
    <h3 class="page-title"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
