<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Настройки');

?>
<div class="container-fluid">
    
    <h3 class="page-title"><?= Html::encode($this->title) ?></h3>

    <div class="row-fluid">

        <p>
            <?= Html::a(Yii::t('app', 'Добавить настройку'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'name',
                'value:ntext',
                'description:ntext',
                'active:boolean',
                'is_system:boolean',
                'created_at:datetime',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

    </div>

</div>