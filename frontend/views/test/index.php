<?php

use yii\helpers\Html;
use common\widgets\BitcoinCourse;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Курс');

?>
<div class="container-fluid">
    
    <h3 class="page-title"><?= Html::encode($this->title) ?></h3>

   <div id="course" class="course">
    <?=BitcoinCourse::widget(['selector' => '#course','interval' => 6]) ?>
   </div>
 
</div>
