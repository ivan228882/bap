<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

use common\models\Setting;

AppAsset::register($this);

$script = <<<JS
$('#search_tx_form').on('submit', function (e) {
	e.preventDefault();
	var txid = $('#search_tx_form input').val();
	if (txid.length) {
		location.href = '/transaction/' + txid;
	}
});
JS;

$this->registerJs($script);

?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">>

<head>
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<?php $this->head() ?>
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/vendor/linearicons/style.css">
	<link rel="stylesheet" href="/vendor/chartist/css/chartist-custom.css">
	<link rel="stylesheet" href="/vendor/toastr/toastr.min.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/img/favicon.png">
</head>
<body> 
<?php $this->beginBody() ?>

	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="/"><img src="/img/logo.png" alt="Klorofil Logo" class="img-responsive logo"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<form class="navbar-form navbar-left" id="search_tx_form">
					<div class="input-group">
						<input type="text" value="" class="form-control" name="txid" placeholder="TXID..." style="width: 220px !important">
						<span class="input-group-btn">
							<button type="submit" class="btn btn-primary">Искать</button>
						</span>
					</div>
				</form>
				<?php if ($fee = Setting::find()->where(['name' => 'fee'])->one()) : ?>
				<form class="navbar-form navbar-left col-md-1" action="/setting/update?id=<?=$fee->id?>" method="POST">
					<?php echo Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>
					<div class="input-group col-md-1">
						<input type="text" name="Setting[value]" value="<?=\Yii::$app->session->get('setting.fee')?>" class="form-control" placeholder="Установите комиссию" style="min-width: 50px; width: 100px !important" />
						<input type="hidden" name="redirect" value="<?=$_SERVER['REQUEST_URI']?>" />
						<span class="input-group-btn"><button type="submit" class="btn btn-primary">BTC</button></span>
					</div>
				</form>
				<?php endif; ?>
				<?php if ($fee_satoshi = Setting::find()->where(['name' => 'fee_satoshi'])->one()) : ?>
				<form class="navbar-form navbar-left" action="/setting/update?id=<?=$fee_satoshi->id?>" method="POST">
					<?php echo Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>
					<div class="input-group">
						<input type="text" name="Setting[value]" value="<?=\Yii::$app->session->get('setting.fee_satoshi')?>" class="form-control" placeholder="Установите комиссию в сатоши" style="min-width: 50px; width: 80px !important" />
						<input type="hidden" name="redirect" value="<?=$_SERVER['REQUEST_URI']?>" />
						<span class="input-group-btn"><button type="submit" class="btn btn-primary">Satoshi</button></span>
					</div>
				</form>
				<?php endif; ?>
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" style="font-size: 18px">
								Баланс:
								<?php
									$command = new \Nbobtc\Command\Command('getbalance');
									$response = Yii::$app->bitcoinClient->sendCommand($command);
									$balance = $response->getBody()->getContents();
									$balance = json_decode($balance);
								?> 
								<strong><?=Yii::$app->formatter->asDecimal($balance->result, 8)?></strong>
							</a>
						</li>
						<li class="dropdown">
							<?php
								$command = new \Nbobtc\Command\Command('getwalletinfo');
								$response = Yii::$app->bitcoinClient->sendCommand($command);
								$info = $response->getBody()->getContents();
								$json = json_decode($info);
							?>
							<a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
								<i class="lnr lnr-question-circle"></i> <span>Кошелёк</span> <i class="icon-submenu lnr lnr-chevron-down"></i>
								<?php if (isset($json->error)) : ?>
									<span class="badge bg-danger" style="margin-top: -7px">!</span>
								<?php endif; ?>
							</a>
							<ul class="dropdown-menu" style="padding: 5px">
								<?php if (isset($json->error)) : ?>
								<li>Ошибка: <?= $json->error->message ?></li>
								<li>Код ошибки: <?= $json->error->code ?></li>
								<?php elseif (isset($json->result)) : ?>
								<li>Название кошелька: <?= $json->result->walletname ?></li>
								<li>Версия кошелька: <?= $json->result->walletversion ?></li>
								<li>Баланс: <?= $json->result->balance ?></li>
								<li>Баланс (не подтверждённый): <?= $json->result->unconfirmed_balance ?></li>
								<li>Баланс (не зрелый): <?= $json->result->immature_balance ?></li>
								<li>Количество транзакций: <?= $json->result->txcount ?></li>
								<li>HD ключ: <?= $json->result->hdmasterkeyid ?></li>
								<li>Минимальная плата: <?= printf("%.08f", $json->result->paytxfee) ?></li>
								<?php endif; ?>
							</ul>
						</li>
						<li class="dropdown">
							<?php
								$command = new \Nbobtc\Command\Command('getnetworkinfo');
								$response = Yii::$app->bitcoinClient->sendCommand($command);
								$info = $response->getBody()->getContents();
								$json = json_decode($info);
							?>
							<a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
								<i class="lnr lnr-question-circle"></i> <span>Сеть</span> <i class="icon-submenu lnr lnr-chevron-down"></i>
								<?php if (isset($json->error)) : ?>
									<span class="badge bg-danger" style="margin-top: -7px">!</span>
								<?php endif; ?>
							</a>
							<ul class="dropdown-menu" style="padding: 5px">
								<?php if (isset($json->error)) : ?>
								<li>Ошибка: <?= $json->error->message ?></li>
								<li>Код ошибки: <?= $json->error->code ?></li>
								<?php elseif (isset($json->result)) : ?>
								<li>Версия: <?= $json->result->version ?></li>
								<li>Субверсия: <?= $json->result->subversion ?></li>
								<li>Версия протокола: <?= $json->result->protocolversion ?></li>
								<li>Смещение: <?= round($json->result->timeoffset / 60, 2) ?> минут</li>
								<li>Активность сети: <?= Yii::$app->formatter->asBoolean($json->result->networkactive == true) ?></li>
								<li>Количество соединений: <?= $json->result->connections ?></li>
								<li>Сети:
									<ul>
									<?php foreach ($json->result->networks as $network) : ?>
										<li>
											<?=$network->name?>
											<ul>
												<li>Проски: <?=Yii::$app->formatter->asBoolean($network->proxy)?></li>
											</ul>
										</li>
									<?php endforeach; ?>
									</ul>
								</li>
								<li>Минимальная плата: <?= printf("%.08f", $json->result->relayfee) ?></li>
								<li>Предупреждения: <?= $json->result->warnings ?></li>
								<?php endif; ?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="/" <?=($this->context->id == 'site') ? 'class="active"' : ''?>><i class="fa fa-home"></i> <span>Главная</span></a></li>
						<li><a href="/wallet" <?=($this->context->id == 'wallet') ? 'class="active"' : ''?>><i class="fa fa-book"></i> <span>Кошельки</span></a></li>
						<li>
							<a href="#sendPages" data-toggle="collapse" class="collapsed" <?=($this->context->id == 'send') ? 'class="active" aria-expanded="true"' : ''?>><i class="fa fa-send"></i> <span>Отправка</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="sendPages" <?=($this->context->id == 'send') ? 'class="collapse in" aria-expanded="true"' : 'class="collapse"'?>>
								<ul class="nav">
									<li><a href="/send/to-address" class="<?=($this->context->action->id == 'to-address') ? 'active' : '' ?>">На адрес</a></li>
									<li><a href="/send/move" class="<?=($this->context->action->id == 'move') ? 'active' : '' ?>">Между аккаунтами</a></li>
									<li><a href="/send/raw" class="<?=($this->context->action->id == 'raw') ? 'active' : '' ?>">Ручное создание</a></li>
									<li><a href="/send/raw-unconfirmed" class="<?=($this->context->action->id == 'raw-unconfirmed') ? 'active' : '' ?>">Ручное создание (Неподтв.)</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#accountPages" data-toggle="collapse" class="collapsed" <?=($this->context->id == 'account') ? 'class="active" aria-expanded="true"' : ''?>><i class="fa fa-user"></i> <span>Аккаунт</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="accountPages" <?=($this->context->id == 'account') ? 'class="collapse in" aria-expanded="true"' : 'class="collapse"'?>>
								<ul class="nav">
									<li><a href="/account" class="">Информация</a></li>
									<li><a href="/account/addresses" class="">Адреса</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#blockPages" data-toggle="collapse" class="collapsed" <?=($this->context->id == 'block') ? 'class="active" aria-expanded="true"' : ''?>><i class="fa fa-th"></i> <span>Блоки</span> <i class="icon-submenu lnr lnr-chevron-left"></i><span class="badge bg-danger" id="count_new_blocks"></span></a>
							<div id="blockPages" <?=($this->context->id == 'block') ? 'class="collapse in" aria-expanded="true"' : 'class="collapse"'?>>
								<ul class="nav">
									<li><a href="/block" class="">Все</a></li>
									<li><a href="/block/latest" class="">Последний</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#transactionPages" data-toggle="collapse" class="collapsed" <?=($this->context->id == 'transaction') ? 'class="active" aria-expanded="true"' : ''?>><i class="fa fa-exchange"></i> <span>Транзакции</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="transactionPages" <?=($this->context->id == 'transaction') ? 'class="collapse in" aria-expanded="true"' : 'class="collapse"'?>>
								<ul class="nav">
									<li><a href="/transaction" class="">Все</a></li>
									<li><a href="/transaction/latest" class="">Последние</a></li>
									<li><a href="/transaction/unconfirmed" class="">Не подтверждённые</a></li>
									<li><a href="/transaction/unspent" class="">Доступные транзакции</a></li>
									<li><a href="/transaction/get" class="">Информация по транзакции</a></li>
								</ul>
							</div>
						</li>
						<li>
							<a href="#addressPages" data-toggle="collapse" class="collapsed" <?=($this->context->id == 'address') ? 'class="active" aria-expanded="true"' : ''?>><i class="fa fa-address-book"></i> <span>Адреса</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="addressPages" <?=($this->context->id == 'address') ? 'class="collapse in" aria-expanded="true"' : 'class="collapse"'?>>
								<ul class="nav">
									<li><a href="/address/new" class="">Создать</a></li>
									<li><a href="/address/validate" class="">Проверить адрес</a></li>
								</ul>
							</div>
						</li>
						<li><a href="/setting" <?=($this->context->id == 'setting') ? 'class="active"' : ''?>><i class="lnr lnr-cog"></i> <span>Настройки</span></a></li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<div class="main-content">
				<div class="container-fluid">
					<?php echo Alert::widget() ?>
					<?= $content ?>
				</div>
			</div>
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>

	</div>
	<!-- END WRAPPER -->

<?php $this->endBody() ?>
	<!-- <script src="/vendor/jquery/jquery.min.js"></script> -->
	<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="/vendor/chartist/js/chartist.min.js"></script>
	<script src="/vendor/toastr/toastr.min.js"></script>
	<script src="/scripts/klorofil-common.js"></script>

	<?php if ($_SERVER['SERVER_NAME'] != 'bitcoin.loc') : ?>
		<?php
			$apiHost = 'https://api.torbithammer2216.7money.co/v2';
			// host
		?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
	<script type="text/javascript">

		try {
			$.pjax.defaults.timeout = false;
		} catch (e) {
			
		}

		/*var newTxAudio = new Audio();
		newTxAudio.preload = 'auto';
		//newTxAudio.volume = 1;
		newTxAudio.src = '/audio/tx.mp3';

		var newTxConfirmAudio = new Audio();
		newTxConfirmAudio.preload = 'auto';
		//newTxConfirmAudio.volume = 1;
		newTxConfirmAudio.src = '/audio/confirm.mp3';*/

		function soundNewTx() {
			var newTxAudio = document.getElementById('audio_tx');
			newTxAudio.play();
		}

		function soundNewTxConfirm() {
			var newTxConfirmAudio = document.getElementById('audio_confirm');
			newTxConfirmAudio.play();
		}

		$(function () {

			const socket = io('socket.torbithammer2216.7money.co', {secure: true});

			toastr.options = {
			  "closeButton": false, 
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": true,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "20000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "escapeHtml": false,
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}

			socket.on('connect', function () {
				console.log('Connected success');
			});

			socket.on('connection', function () {
				console.log('Connected success');
			});

			socket.on('new-block', function (msg) {

				var message = 'Новый блок!<br />' + msg + '<br /><a href="/block/' + msg + '">Детальнее</a>';

				$('#count_new_blocks').text(parseInt($('#count_new_blocks').text().length ? $('#count_new_blocks').text() : 0) + 1);

				toastr['info'](message);

				soundNewTxConfirm();

				if ($('#transactions').length) {
					setTimeout(function () {
						$.pjax.reload({container: '#transactions'});
					}, 10000);
				}

			});

			socket.on('new-alert', function (msg) {

				var message = 'Новое сообщение!<br />' + msg;

				toastr['warning'](message);

			});

			socket.on('new-wallet', function (msg) {

				//toastr['success'](message);

				$.get("<?=$apiHost?>/transaction/get?txid=" + msg, {}, function (response) {
					var json = $.parseJSON(JSON.stringify(response));
					//console.log(json);
					if (json.error != null) {
						toastr['error']('Ошибка! ' + json.error.message);
					} else {
						var confirmations = json.result.confirmations;
						var addresses = json.result.details;

						if (confirmations > 0) {

							for (address in addresses) {
								var mess = 'По адресу "' + addresses[address].address + '" подтверждений ' + confirmations + '<br /><a href="/transaction/' + msg + '">Детальнее</a>';
								toastr['success'](mess);
							}

						} else {
							var message = 'Новая транзакция!<br />' + msg + '<br /><a href="/transaction/' + msg + '">Детальнее</a>';
							toastr['success'](message);
							soundNewTx();
						}
					}

					if ($('#transactions').length) {
						setTimeout(function () {
							$.pjax.reload({container: '#transactions'});
						}, 10000);
					}

				});

			});
		  
		});
	</script>
	<?php endif; ?>

	<audio src="/audio/tx.mp3" id="audio_tx" autostart="false" preload></audio>
	<audio src="/audio/confirm.mp3" id="audio_confirm" autostart="false" preload></audio>

</body>
</html>
<?php $this->endPage() ?>
