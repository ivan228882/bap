<?php

// подключаем виджет постраничной разбивки
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = 'Отправить на адрес';

/*$this->registerJs(<<<JS

$(document).on('input change paste', '#fee', function (e) {
	if ($('#fee_in_bytes').is(':checked')) {
		$('#fee_in_kb').text('За килобайт: ' + $(this).val() / 100000);
	}
});

JS
);*/

?>

<?php Pjax::begin(['id' => 'send']) ?>

<h1><?=$this->title?></h1>

<div class="row">
	<?php foreach ($fees as $fee) : ?>
	<div class="col-md-<?=floor(12 / count($fees))?>">
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title"><?= \Yii::t('app',
    '{n, plural,
     =0{Нет блоков}
     =1{# блок}
     =2{# блока}
     =3{# блока}
     one{# блок}
     few{# блока}
     many{# блоков}
     other{# блоков}}!',
     ['n' => $fee['blocks']]
) ?></h3>
				<div class="right">
					<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
					<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
				</div>
			</div>
			<div class="panel-body">
				<?=printf('%f', $fee['fee'])?>
			</div>
		</div>
	</div>
	<?php endforeach; ?>
</div>

<form action="" method="POST" class="form">

	<?php echo Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>

	<div class="row">

		<div class="form-group col-md-4">
			<label for="to">Куда (Адрес)</label>
			<input type="text" name="to" id="to" class="form-control" required />
		</div>

		<div class="form-group col-md-2">
			<label for="amount">Сумма</label>
			<input type="text" name="amount" id="amount" class="form-control" required />
		</div>

		<!-- <div class="form-group col-md-3">
			<label for="fee" style="display: block">
				Комиссия 
				<a href="#" data-toggle="popover" title="Комиссия" data-content="Указывается комиссия в килобайтах! Например: 0.00100000. Чтобы указать комиссию в байтах - активируйте чекбокс справа, и указанная комиссия в байтах переведётся в килобайты при отправке, также сохранится в настройках, и обновится по панели!">
					<i class="glyphicon glyphicon-question-sign"></i>
				</a>
				<span class="pull-right" id="fee_in_kb" style="display: inline-block;"></span>
			</label>
			<input type="text" name="fee" id="fee" class="form-control" value="<?=Yii::$app->session->get('setting.fee')?>" />
		</div>
		
		<div class="form-group col-md-2">
			<label for="fee_in_bytes">В байтах?</label>
			<input type="checkbox" name="fee_in_bytes" id="fee_in_bytes" class="checkbox" value="1" />
		</div> -->

		<div class="form-group col-md-3">
			<label for="comment">
				Комментарий транзакции 
				<a href="#" data-toggle="popover" title="Комментарий транзакции" data-content="Комментарий локального хранения (не широковещательного), назначенный этой транзакции. По умолчанию нет комментариев">
					<i class="glyphicon glyphicon-question-sign"></i>
				</a>
			</label>
			<textarea name="comment" id="comment" class="form-control"></textarea>
		</div>

		<div class="form-group col-md-3">
			<label for="comment_to">
				Комментарий транзакции (кому отправлено) 
				<a href="#" data-toggle="popover" title="Комментарий транзакции (кому отправлено)" data-content="Комментарий локального хранения (не широковещательного), назначенный этой транзакции. Значение, которое используется для описания того, кому был отправлен платеж. По умолчанию нет комментариев">
					<i class="glyphicon glyphicon-question-sign"></i>
				</a>
			</label>
			<textarea name="comment_to" id="comment_to" class="form-control"></textarea>
		</div>

		<div class="form-group col-md-2">
			<label for="to">&nbsp;</label>
			<input type="submit" name="send" class="btn btn-primary btn-flat btn-block">
		</div>

	</div>
	
</form>

<?php Pjax::end() ?>