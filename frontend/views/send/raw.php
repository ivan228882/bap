<?php

// подключаем виджет постраничной разбивки
//use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
//use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use common\models\Setting;
use common\components\RecommendFee;

$high_fee = RecommendFee::getHigh(); // high priority fee
$medium_fee = RecommendFee::getMedium(); // medium priority fee
$low_fee = RecommendFee::getLow(); // low priority fee

$this->title = 'Ручное создание транзакции (RAW)';
//$token

usort($unspent, function ($a, $b) {
	return ($a->amount < $b->amount) ? 1 : 0;
});

$limitSmallChange = '';
$addressSmallChange = '';
$addressBigChange = '';

if ($value = Setting::getValue('limit_small_change')) {
	$limitSmallChange = $value;
}
if ($value = Setting::getValue('address_small_change')) {
	$addressSmallChange = $value;
}
if ($value = Setting::getValue('address_big_change')) {
	$addressBigChange = $value;
}

?>

<?php Pjax::begin(['id' => 'raw']) ?>

<h3><?=$this->title?></h3>
<?php $form = ActiveForm::begin([
	'id' => 'raw-form'
]) ?>

	<div class="col-md-4 pull-left">
		<div class="form-group">
			<label>
				Режим: 
				<select id="selection_method" class="form-control">
					<option value="manual">Ручной</option>
					<option value="auto_min_change">Автоматически (минимум сдачи)</option>
					<option value="auto_small_piece">Автоматически (маленькие куски)</option>
					<option value="auto_big_piece">Автоматически (большие куски)</option>
				</select>
			</label>
		</div>
	</div>

	<div class="clearfix"></div>

	<?= $form->field(
			$model, 
			'fee', 
			[
				'options' => [
					'class' => 'col-md-2'
				]
			]
		)->textInput(
			[
				'value' => $model->fee
			]
		)
	?>

	<?= $form->field(
			$model, 
			'feeSatoshi', 
			[
				'options' => [
					'class' => 'col-md-2'
				]
			]
		)->textInput(
			[
				'value' => $model->feeSatoshi
			]
		)
	?>

	<?= $form->field(
			$model, 
			'limitSmallChange', 
			[
				'options' => [
					'class' => 'col-md-2'
				]
			]
		)->textInput(
			[
				'value' => $limitSmallChange
			]
		)
	?>
	<?= $form->field(
			$model, 
			'addressSmallChange', 
			[
				'options' => [
					'class' => 'col-md-4'
				]
			]
		)->textInput(
			[
				'value' => $addressSmallChange
			]
		)
	?>
	<?= $form->field(
			$model, 
			'addressBigChange', 
			[
				'options' => [
					'class' => 'col-md-4'
				]
			]
		)->textInput(
			[
				'value' => $addressBigChange
			]
		)
	?>
	
	<div style="overflow: auto; height: 300px; display: inline-block; width: 100%">
	<?php
		
		echo GridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => [
				'class' => 'table table-striped table-bordered table-responsive',
				'id' => 'table-ins'
			],
			'columns' => [
				[
					'content' => function ($row) use ($form, $model) {
						return $form
								->field(
									$model, 
									'ins['.$row['txid'].':'.$row['vout'].']',
									[
										'template' => '<div class="form-group">
											<label class="fancy-checkbox">
												{input}
												<span></span>
											</label>
											<div class="help-block"></div>
										</div>',
									]
								)
								->checkbox(['data-txid' => $row['txid'].':'.$row['vout'] ], false)
								->label(false);
					}
				],
				[
					'attribute' => 'address',
					'label' => 'Адрес',
					'format' => 'raw',
					'value' => function ($row) use ($addressSmallChange, $addressBigChange) {
						$html = $row['address'];
						if ($row['address'] == $addressSmallChange) {
							$html .= Html::tag('br') . Html::tag('span', 'Адрес маленькой сдачи', ['class' => 'label label-info']);
						}
						if ($row['address'] == $addressBigChange) {
							$html .= Html::tag('br') . Html::tag('span', 'Адрес большой сдачи', ['class' => 'label label-primary']);
						}
						if ($row['confirmations'] == 0) {
							$html .= Html::tag('br') . Html::tag('span', 'Неподтверждённый', ['class' => 'label label-danger']);
						}
						return $html;
					}
				],
				[
					'attribute' => 'txid',
					'label' => 'Транзакция'
				],
				[
					'attribute' => 'confirmations',
					'label' => 'Подтверждения'
				],
				[
					'attribute' => 'amount',
					'label' => 'Баланс',
					'value' => function ($row) {
						return number_format($row['amount'], 8);
					}
				],
				[
					'attribute' => 'vout',
					'label' => 'Индекс выхода'
				]
			]
		]);

	?>
	</div>

	<br />
	<br />

	<div class="outs">
		<button id="add-out-address" class="btn btn-primary btn-flat" type="button">Добавить адрес выхода</button>
		<div class="row out">
			<?= $form->field($model, 'outAddresses[]', ['options' => ['class' => 'col-md-7 out-address']])->textInput() ?>
			<?= $form->field($model, 'outAmounts[]', ['options' => ['class' => 'col-md-4 out-amount']])->textInput() ?>
			<?= $form->field($model, 'redirectUri')->hiddenInput(['value' => '/send/raw']) ?>
			<div class="form-group col-md-1">
				<label>&nbsp;</label>
				<br />
				<button type="button" class="btn btn-danger remove-out"><i class="fa fa-times-circle"></i></button>
			</div>
		</div>
	</div>
	<hr id="end-outs" />

	<br />

	<div class="row">
		<div class="col-md-3">
			<p>Баланс: <span id="balance"><?=$model->balance?></span> BTC</p>
			<p>Комиссия: <span id="fee"><?=$model->fee?></span></p>
			<p>Сатоши: <span id="satoshi">0</span></p>
			<p>Размер: <span id="size">0</span></p>
		</div>
		<div class="col-md-3">
			<p>Сумма входов:  <span id="inSum">0</span> BTC</p>
			<p>Сумма выходов:  <span id="outSum">0</span> BTC</p>
			<hr />
			<p>Количество входов:  <span id="countIns">0</span></p>
			<p>Количество выходов:  <span id="countOuts">0</span></p>
		</div>
		<div class="col-md-3">
			<p>Сдача:  <span id="changeSum">0</span> BTC</p>
			<p>Сдача с вычетом комиссии:  <span id="changeSumWithFee">0</span> BTC</p>
		</div>
		<div class="col-md-3">
			<p>
				Рекоммендуемая комиссия:
				<br />
				<span id="recommendFee">0</span> satoshi/byte
			</p>
		</div>
	</div>

	<br />
	<input type="text" name="transactionSubcribe" value="" class="hidden">
	<div class="form-group">
		<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary send0btn']) ?>
	</div>
	
<?php ActiveForm::end() ?>

<?php Pjax::end() ?>

<!-- HTML-код модального окна -->
<div id="myModalBox" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Заголовок модального окна -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Информация</h4>
      </div>
      <!-- Основное содержимое модального окна -->
      <div class="modal-body">
        <table class="table table-striped ins-block">
        	<thead>
        		<tr>
        			<th>
        				Адреса с которых отправляется
        			</th>
        		</tr>
        	</thead>
        	<tbody>
        	</tbody>
        </table>
         <table class="table table-striped outs-block">
        	<thead>
        		<tr>
        			<th>
        				Адреса на которые отправляем
        			</th>
        		</tr>
        	</thead>
        	<tbody>
        	</tbody>
        </table>
        <table class="table table-striped">
        	<thead>
        		<tr>
        			<th>
        				Сумма
        			</th>
        			<th>
        				Коммисия
        			</th>
        			<th>
        				Коммисия<br />(сатоши/байт)
        			</th>
        			<th>
        				Сдача
        			</th>
        			<th>
        				Сдача с вычетом комиссии
        			</th>
        			<th>
        				Размер (примерный)
        			</th>
        		</tr>
        	</thead>
        	<tbody>
        		<td class="outSum-m"></td>
        		<td class="fee-m"></td>
        		<td class="satoshi-m"></td>
        		<td class="changeSum-m"></td>
        		<td class="changeSumWithoutFee-m"></td>
        		<td class="size-m"></td>
        	</tbody>
        </table>
      </div>
      <!-- Футер модального окна -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="send-btn">Отправить транзакцию</button>
      </div>
    </div>
  </div>
</div>
