<?php

// подключаем виджет постраничной разбивки
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = 'Отправка';

?>

<a href="/send/to-address" class="btn btn-link">На адрес</a>
<a href="/send/move" class="btn btn-link">На аккаунт</a>
<a href="/send/raw" class="btn btn-link">Ручное создание</a>