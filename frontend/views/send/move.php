<?php

// подключаем виджет постраничной разбивки
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = 'Отправить с аккаунта на аккаунт';

?>

<?php Pjax::begin(['id' => 'move']) ?>

<h1><?=$this->title?></h1>

<form action="" method="POST" class="form">

	<?php echo Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>

	<div class="row">

		<div class="form-group col-md-4">
			<label for="from">Откуда</label>
			<select name="from" id="from" class="form-control" required>
				<option> -- Выберите аккаунт -- </option>
			<?php foreach ($accounts as $account => $balance) : ?>
				<option value="<?=$account?>"><?=(empty($account)) ? 'Основной' : $account?> - <?=$balance?></option>
			<?php endforeach; ?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label for="to">Куда</label>
			<select name="to" id="to" class="form-control" required>
				<option> -- Выберите аккаунт -- </option>
			<?php foreach ($accounts as $account => $balance) : ?>
				<option value="<?=$account?>"><?=(empty($account)) ? 'Основной' : $account?> - <?=$balance?></option>
			<?php endforeach; ?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label for="amount">Сумма</label>
			<input type="text" name="amount" id="amount" class="form-control" required />
		</div>

		<div class="form-group col-md-12">
			<label for="comment">
				Комментарий 
			</label>
			<textarea name="comment" id="comment" class="form-control"></textarea>
		</div>

		<div class="form-group col-md-2">
			<label for="to">&nbsp;</label>
			<input type="submit" name="send" class="btn btn-primary btn-flat btn-block">
		</div>

	</div>
	
</form>

<?php Pjax::end() ?>