<?php

// подключаем виджет постраничной разбивки
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Блоки';

?>

<?php Pjax::begin(['id' => 'blocks', 'timeout' => 5000]) ?>

<h1><?=$this->title?></h1>
<?= GridView::widget([
	'dataProvider' => $providerBlocks,
	'columns' => [
		[
			'label' => 'Высота',
			'attribute' => 'height'
		],
		[
			'label' => 'Дата',
			'attribute' => 'date',
			'value' => function ($model, $key, $index, $grid) {
				return ($model['date'] < 60*60*24*2) ? Yii::$app->formatter->asRelativeTime($model['blocktime']) : Yii::$app->formatter->asDateTime($model['blocktime']);
			}	
		],
		[
			'label' => 'Транзакции',
			'attribute' =>'tx'
		],
		[
			'label' => 'Размер',
			'attribute' =>'size'
		],
		[
			'label'=> 'Вес',
			'attribute' => 'weight'
		],
		[
			'label' => 'Просмотр',
			'attribute' => 'link',
			'format' => 'raw',
			'value' => function ($model, $key, $index, $grid) {
				$links = Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['/block/' . $model['hash']]);
				$links .= Html::a('&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-star"></span>', '//blockchain.info/ru/block/' . $model['hash'], ['target' => '_blank']);
				return $links;
			}
		],
	],
]) ?>

<?php
// отображаем постраничную разбивку
echo LinkPager::widget([
	'pagination' => $pages,
]);
?>

<?php Pjax::end() ?>