<?php

$this->title = 'Блок: ' . $block->hash;

$blockHash = $block->hash;

$this->registerJs(<<<JS

$(function () {
	$.get('/block/list-since-block?hash=$blockHash', {}, function (response) {
		$('#transactions').replaceWith(response);
	});
});

JS
);

?>

<div class="">
	<?php if (isset($block->previousblockhash)) : ?>
	<a href="/block/<?=$block->previousblockhash?>" class="btn btn-success pull-left">Предыдущий блок</a>
	<?php endif; ?>
	<?php if (isset($block->nextblockhash)) : ?>
	<a href="/block/<?=$block->nextblockhash?>" class="btn btn-success pull-right">Следующий блок</a>
	<?php endif; ?>
</div>

<div class="clearfix"></div>

<div class="panel panel-headline">
	<div class="panel-heading">
		<h3 class="panel-title"><?=$this->title?></h3>
		<?php if (isset($block->previousblockhash)) : ?>
		<p class="panel-subtitle">Предыдущий блок: <?=$block->previousblockhash?></p>
		<?php endif; ?>
		<?php if (isset($block->nextblockhash)) : ?>
		<p class="panel-subtitle">Следующий блок: <?=$block->nextblockhash?></p>
		<?php endif; ?>
		<p class="panel-subtitle">Время: <?=Yii::$app->formatter->asDateTime($block->time, 'short')?></p>
		<p class="panel-subtitle">Среднее время: <?=Yii::$app->formatter->asDateTime($block->mediantime, 'short')?></p>
		<p class="panel-subtitle">Размер: <?=$block->size?> (в байтах)</p>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="metric">
					<span class="icon"><i class="fa fa-bell"></i></span>
					<p>
						<span class="number"><?=$block->confirmations?></span>
						<span class="title">Подтверждения</span>
					</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="metric">
					<span class="icon"><i class="fa fa-shopping-bag"></i></span>
					<p>
						<span class="number"><?=count($block->tx) ?></span>
						<span class="title">Транзакции</span>
					</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="metric">
					<span class="icon"><i class="fa fa-eye"></i></span>
					<p>
						<span class="number"><?=$block->weight?></span>
						<span class="title">Вес</span>
					</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="metric">
					<span class="icon"><i class="fa fa-bar-chart"></i></span>
					<p>
						<span class="number"><?=$block->height?></span>
						<span class="title">Высота</span>
					</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="metric">
					<span class="icon"><i class="fa fa-bar-chart"></i></span>
					<p>
						<span class="number"><?=$block->size?></span>
						<span class="title">Размер</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="transactions">
	<div class="alert alert-info loading">
		Загрузка транзакций...
	</div>
</div>