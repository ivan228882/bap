<?php if (isset($walletTransactions) and !empty($walletTransactions)) : ?>
<div class="panel panel-headline full-width" style="width: 100%">
	<div class="panel-heading">
		<h3 class="panel-title">Транзакции нашего кошелька</h3>
	</div>
	<div class="panel-body">
		<table class="table table-hovered table-striped">
			<thead>
				<tr>
					<th>
						Дата
					</th>
					<th>
						Адрес
					</th>
					<th>
						Подтверждения
					</th>
					<th>
						Тип
					</th>
					<th>
						Сумма
					</th>
					<th>
						Метка
					</th>
					<th>
						
					</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($walletTransactions->transactions as $transaction) : ?>
				<?php
					$diff = strtotime('now') - $transaction->time;
				?>
				<tr style="background: <?=($transaction->category == 'receive') ? '#7DE2EA' : '#E8A39E'?>">
					<td>
						<?=($diff < 60*60*24*2) ? Yii::$app->formatter->asRelativeTime($transaction->time) : Yii::$app->formatter->asDateTime($transaction->time)?>
					</td>
					<td>
						<?=$transaction->address?>
					</td>
					<td>
						<?=$transaction->confirmations?>
					</td>
					<td>
						<?=($transaction->category == 'receive') ? 'Получено' : 'Отправлено'?>
					</td>
					<td>
						<?=Yii::$app->formatter->asDecimal($transaction->amount, 8)?>
					</td>
					<td>
						<?=(isset($transaction->label)) ? $transaction->label : ''?>
					</td>
					<td>
						<a href="/transaction/<?=$transaction->txid?>" title="Просмотр" aria-label="Просмотр" data-pjax="false">
							<span class="glyphicon glyphicon-eye-open"></span>
						</a>
						<a href="/transaction/archive?txid=<?=$transaction->txid?>" title="Заархивировать" aria-label="Заархивировать" data-pjax="false">
							<span class="glyphicon glyphicon-trash"></span>
						</a>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif; ?>

<?php if (isset($transactions) and !empty($transactions)) : ?>
<div class="panel panel-headline full-width">
	<div class="panel-heading">
		<h3 class="panel-title">Транзакции с этого блока</h3>
	</div>
	<div class="panel-body">
		<table class="table table-hovered table-striped">
			<thead>
				<tr>
					<th>
						TXID
					</th>
					<th>
						Размер
					</th>
					<th>
						Виртуальный размер
					</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($transactions as $transaction) : ?>
				<tr>
					<td>
						<?=$transaction->txid?>
					</td>
					<td>
						<?=$transaction->size?>
					</td>
					<td>
						<?=$transaction->vsize?>
					</td>
					<td>
								<a href="https://www.blocktrail.com/tBTC/tx/<?=$transaction->txid?>" title="Просмотр" aria-label="Просмотр" data-pjax="0">
							<span class="glyphicon glyphicon glyphicon-star"></span>
						</a>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif; ?>