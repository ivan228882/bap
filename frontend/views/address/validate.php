<?php

// подключаем виджет постраничной разбивки
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = 'Валидация адреса';

$map = [
	'isvalid' => [
		'text' => 'Валиден?',
		'format' => 'boolean'
	],
	'address' => [
		'text' => 'Адрес',
		'format' => 'raw'
	],
	'addresses' => [
		'text' => 'Адреса',
		'format' => 'raw'
	],
	'embedded' => [
		'text' => 'Дополнительно',
		'format' => 'raw'
	],
	'scriptPubKey' => [
		'text' => 'Публичный ключ сценария',
		'format' => 'raw'
	],
	'hex' => [
		'text' => 'Сценарий восстановления',
		'format' => 'raw'
	],
	'ismine' => [
		'text' => 'Наш адрес?',
		'format' => 'boolean'
	],
	'iswatchonly' => [
		'text' => 'Только для просмотра?',
		'format' => 'boolean'
	],
	'isscript' => [
		'text' => 'Скрипт?',
		'format' => 'boolean'
	],
	'script' => [
		'text' => 'Тип скрипта',
		'format' => 'raw'
	],
	'pubkey' => [
		'text' => 'Публичный ключ',
		'format' => 'raw'
	],
	'iscompressed' => [
		'text' => 'Сжимается?',
		'format' => 'boolean'
	],
	'iswitness' => [
		'text' => 'Сжимается?',
		'format' => 'boolean'
	],
	'account' => [
		'text' => 'Аккаунт',
		'format' => 'raw'
	],
	'timestamp' => [
		'text' => 'Дата создания',
		'format' => 'datetime'
	],
	'hdkeypath' => [
		'text' => 'HD путь',
		'format' => 'raw'
	],
	'hdmasterkeyid' => [
		'text' => 'Идентификатор ключа HD',
		'format' => 'raw'
	],
];

?>

<div class="panel">
	<div class="panel-heading">
		<h3><?=$this->title?></h3>
	</div>
	<div class="panel-body">
		<form action="" method="POST">

			<?php echo Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>

			<div class="row">

				<div class="form-group col-md-4">
					<label for="name">Адрес</label>
					<input type="text" name="address" id="address" class="form-control" value="<?=($address) ?: ''?>" required />
				</div>

			</div>

			<button type="submit" class="btn btn-success btn-flat">Проверить</button>
			
		</form>
	</div>
</div>

<?php if (isset($address) and $address !== null and $result) : ?>
<div class="panel">
	<div class="panel-heading">
		<h3>Адрес успешно проверен!</h3>
	</div>
	<div class="panel-body">
		
		<?php if ($result->error != null) : ?>
			Код ошибки: <?= $result->error->code ?>
			<br />
			Текст ошибки: <?= $result->error->message ?>
		<?php else : ?>
		<table class="table table-striped table-hovered table-bordered">
			<?php foreach ($result->result as $key => $value) : ?>
			<tr>
				<?php
					$field = isset($map[$key]) ? $map[$key] : [];
					if (!empty($field) and isset($field['format']) and is_string($field['format'])) :
						$format = 'as' . ucfirst($field['format']);
				?>
				<td><?= $field['text'] ?></td>
				<td><?= Yii::$app->formatter->$format(json_encode($value)) ?></td>
				<?php else : ?>
				<td><?= $key ?></td>
				<td><?php print_r($value) ?></td>
				<?php endif; ?>
			</tr>
			<?php endforeach; ?>
		</table>
		<?php endif; ?>
	</div>
</div>
<?php endif; ?>