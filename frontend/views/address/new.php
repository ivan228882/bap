<?php

// подключаем виджет постраничной разбивки
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = 'Создание нового адреса';

$this->registerJs(<<<JS

$('input[name="as_wallet"]').on('click', function (e) {
	if ($(this).is(':checked')) {
		$('input[name="wallet_name"]').attr('type', 'text');
	} else {
		$('input[name="wallet_name"]').attr('type', 'hidden').val('');
	}
});

JS
);

?>

<div class="panel">
	<div class="panel-heading">
		<h3><?=$this->title?></h3>
	</div>
	<div class="panel-body">
		<form action="" method="POST">

			<?php echo Html::hiddenInput(\Yii::$app->getRequest()->csrfParam, \Yii::$app->getRequest()->getCsrfToken(), []); ?>

			<label class="fancy-checkbox">
				<input type="checkbox" name="as_wallet" value="1">
				<span>Как кошелёк</span>
			</label>

			<input type="hidden" name="wallet_name" class="form-control" placeholder="Введите желаемое название кошелька">

			<br>

			<button type="submit" class="btn btn-success btn-flat">Создать</button>
			
		</form>
	</div>
</div>

<?php if (isset($address) and $address !== null) : ?>
<div class="panel">
	<div class="panel-heading">
		<h3>Адрес успешно создан</h3>
	</div>
	<div class="panel-body">
		<p><?=$address?> <button class="btn btn-xs" data-clipboard-text="<?=$address?>"><i class="fa fa-copy"></i></button></p>
	</div>
</div>
<?php endif; ?>