<?php

// подключаем виджет постраничной разбивки
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\grid\GridView;

use common\models\Setting;

use yii\data\ArrayDataProvider;

use common\components\RecommendFee;
use common\components\Transaction;

/* @var $this yii\web\View */

$this->title = 'Главная';

$addressSmallChange = '';
$addressBigChange = '';
if ($value = Setting::getValue('address_small_change')) {
	$addressSmallChange = $value;
}
if ($value = Setting::getValue('address_big_change')) {
	$addressBigChange = $value;
}

$command = new \Nbobtc\Command\Command('getwalletinfo');
$response = Yii::$app->bitcoinClient->sendCommand($command);
$walletInfo = json_decode($response->getBody()->getContents());
$balance = $walletInfo->result->balance;

$confirmedBalance = $balance;
$unconfirmedBalance = $walletInfo->result->unconfirmed_balance - $walletInfo->result->immature_balance;

if ($confirmedBalance < 0) {
	$confirmedBalance = 0;
}
$balance = $confirmedBalance + $unconfirmedBalance;

$command = new \Nbobtc\Command\Command('getreceivedbyaccount', ['']);
$response = Yii::$app->bitcoinClient->sendCommand($command);
$received = json_decode($response->getBody()->getContents());

$command = new \Nbobtc\Command\Command('getblockcount');
$response = Yii::$app->bitcoinClient->sendCommand($command);
$blockCount = json_decode($response->getBody()->getContents());

$command = new \Nbobtc\Command\Command('getmempoolinfo');
$response = Yii::$app->bitcoinClient->sendCommand($command);
$memPoolInfo = json_decode($response->getBody()->getContents());

/*$high_fee = RecommendFee::getHigh(); // high priority fee
$medium_fee = RecommendFee::getMedium(); // medium priority fee
$low_fee = RecommendFee::getLow(); // low priority fee*/

$count = $walletInfo->result->txcount;
$pageSize = 10;
$skip = ((int) Yii::$app->request->get('page', 1)) * $pageSize - $pageSize;

$command = new \Nbobtc\Command\Command('listtransactions', ['*', $pageSize, $skip]);
$response = Yii::$app->bitcoinClient->sendCommand($command);
$listtransactions = json_decode($response->getBody()->getContents());

$allTransactions = $listtransactions->result;

$txs = [];

if ($allTransactions) {
	foreach ($allTransactions as $i => &$transaction) {
		$command = new \Nbobtc\Command\Command('getrawtransaction', [$transaction->txid, true]);
		$response = Yii::$app->bitcoinClient->sendCommand($command);
		$getrawtransaction = json_decode($response->getBody()->getContents());

		$transaction->raw = $getrawtransaction->result;

		if ($transaction->raw !== null and $transaction->raw instanceof \stdClass) {
			$tx = new Transaction($transaction->raw);
			$txs[$transaction->txid] = $tx;
			$fee = $tx->getFee();
			$html = '';
			if (isset($fee['fee'])) {
				$html = number_format($fee['fee'], 8);
			} else {
				$html = isset($transaction->fee) ? $transaction->fee : '';
			}
			$transaction->fee = $html;
		} else {
			$transaction->fee = isset($transaction->fee) ? number_format($transaction->fee, 8) : '';
		}

		//$transaction->fee = isset($transaction->fee) ? number_format($transaction->fee, 8) : '';

		$transaction->label = null;
		if ($order = Yii::$app->db->createCommand('SELECT * FROM `orders` WHERE `txid` = :txid')->bindValue(':txid', $transaction->txid)->queryOne()) {
			$transaction->label = $order['order_codes'];
		}
	}
}


$txid = Yii::$app->request->get('txid');
$address = Yii::$app->request->get('address');
$label = Yii::$app->request->get('label');
$only = Yii::$app->request->get('only');

if ($txid or $address or $label or $only) {
	foreach ($allTransactions as $i => $transaction) {
		if ($only) {
			if ($only == 'change' and ($addressSmallChange == $transaction->address or $addressBigChange == $transaction->address)) {
				continue;
			}
			if ($only == 'receive' and $transaction->category == 'receive') {
				continue;
			}
			if ($only == 'send' and $transaction->category == 'send') {
				continue;
			}
		}
		if (($txid and ($txid == $transaction->txid or mb_stripos($transaction->txid, $txid) !== false)) and ($address and ($address == $transaction->address or mb_stripos($transaction->address, $address) !== false))) {
			continue;
		}
		if ($txid and ($txid == $transaction->txid or mb_stripos($transaction->txid, $txid) !== false)) {
			continue;
		}
		if ($address and ($address == $transaction->address or mb_stripos($transaction->address, $address) !== false)) {
			continue;
		}
		if ($label and ($label == $transaction->label or mb_stripos($transaction->label, $label) !== false)) {
			continue;
		}
		unset($allTransactions[$i]);
	}
}

$provider = new ArrayDataProvider([
	'allModels' => $allTransactions,
	'pagination' => [
		'totalCount' => $count,
		'pageSize' => $pageSize,
	],
	'sort' => [
		'defaultOrder' => [
			'time' => SORT_DESC,
		],
		'attributes' => [
			'time',
			'amount',
			'fee',
			'address',
			'label',
			'confirmations',
		],
	]
]);
// get the posts in the current page
$transactions = $provider->getModels();

$pagination = new yii\data\Pagination(['totalCount' => $count, 'pageSize' => $pageSize]);

/*try {

	$rates = Yii::$app->cache->get('rates');

	if ($rates === false) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://blockchain.info/ru/ticker');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $rates = curl_exec($ch);  
        curl_close($ch);    
		//$rates = file_get_contents('https://blockchain.info/ru/ticker');
		$rates = json_decode($rates);
		Yii::$app->cache->set('rates', $rates, 15);
	}

	$rates = (array) $rates;
	$col = 4;
	$rub = $rates['RUB'];
	unset($rates['RUB']);
	$rates = array_merge(
				array_slice($rates, 0, 1),
				['RUB' => $rub],
				array_slice($rates, 1)
			);
	$eur = $rates['EUR'];
	unset($rates['EUR']);
	$rates = array_merge(
				array_slice($rates, 0, 2),
				['EUR' => $eur],
				array_slice($rates, 2)
			);

	$availableCurrencies = array_keys((array)$rates);

	$rates = array_slice($rates, 0, 3);
	$rates = (object) $rates;
} catch (\Exception $e) {

}*/

function convertToReadableSize($size)
{
	if ($size == 0)
		return 0;
	$base = log($size) / log(1024);
	$suffix = array("", "KB", "MB", "GB", "TB");
	$f_base = floor($base);
	return round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
}

?>

<div class="site-index">

	<div class="row">

		<div class="col-md-3">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Баланс</h3>
					<div class="right">
						<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
						<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
					</div>
				</div>
				<div class="panel-body">
					<p>Всего: <span data-id="balance"><strong style="font-size: 1.2em;"><?=Yii::$app->formatter->asDecimal($balance, 8)?></strong></span></p>
					<p>Подтверждённый: <span data-id="confirmed_balance"><strong style="font-size: 1.2em; color: #32CD32"><?=Yii::$app->formatter->asDecimal($confirmedBalance, 8)?></strong></span></p>
					<p>Не подтверждённый: <span data-id="unconfirmed_balance"><strong style="font-size: 1.2em; color: #CD5C5C"><?=Yii::$app->formatter->asDecimal($unconfirmedBalance, 8)?></strong></span></p>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Информация</h3>
					<div class="right">
						<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up lnr-chevron-down"></i></button>
						<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
					</div>
				</div>
				<div class="panel-body" style="display: none;">
					<p>Получено: <span data-id="received"><?=Yii::$app->formatter->asDecimal($received->result, 8)?></span></p>
					<p>Количество блоков: <span data-id="count_blocks"><?=Yii::$app->formatter->asInteger($blockCount->result)?></span></p>
					<?php if (isset($memPoolInfo) and isset($memPoolInfo->result)) : ?>
						<p><span class="label label-primary">Сеть</span></p>
						<p>Транзакций: <span data-id="mempool-trnsactions"><?=$memPoolInfo->result->size?></span></p>
						<p>Байтов: <span data-id="mempool-bytes"><?=$memPoolInfo->result->bytes?></span></p>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Кошелёк</h3>
					<div class="right">
						<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up lnr-chevron-down"></i></button>
						<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
					</div>
				</div>
				<div class="panel-body" style="display: none;">
					<p>Транзакций: <span data-id="txcount"><?=Yii::$app->formatter->asInteger($walletInfo->result->txcount)?></span></p>
					<p>Ключей: <span data-id="keypoolsize"><?=Yii::$app->formatter->asInteger($walletInfo->result->keypoolsize)?></span></p>
				</div>
			</div>
		</div>
		<!-- <div class="col-md-3">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Рекоммендуемая комиссия</h3>
					<div class="right">
						<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up lnr-chevron-down"></i></button>
						<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
					</div>
				</div>
				<div class="panel-body" style="display: none;">
					<p>Рекоммендуемая: <span data-id="fee"><span style="color: #6299D3"><?php //echo $high_fee ?></span> / <span style="color: #4CAF50"><?php //echo $medium_fee ?></span> / <?php //echo $low_fee ?></span></p>
				</div>
			</div>
		</div> -->

		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Последние транзакции</h3>
					<div class="right">
						<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
						<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
					</div>
				</div>
				<div class="panel-body">
				<?php Pjax::begin(['id' => 'transactions', 'timeout' => 5000]) ?>

					<?php
						$this->registerJs(<<<JS
							$.pjax.defaults.timeout = false;
							/*var interval = false;
							if (!interval) {
								interval = setInterval(function () {
									$.pjax.reload({container: '#transactions'});
								}, 10000);
							}*/

							$('.btn-only').on('click', function (e) {
								$('input[name="only"]').val($(this).data('only'));
								$('#filter-form').submit();
							});
JS
						);
					?>

					<div class="row">
						<?= Html::beginForm('/', 'get', ['id' => 'filter-form']) ?>

						<div class="form-group has-success col-md-4">
							<label>TXID</label>
							<?= Html::input('text', 'txid', Yii::$app->request->get('txid', ''), ['class' => 'form-control']) ?>
						</div>

						<div class="form-group has-success col-md-4">
							<label>Адрес</label>
							<?= Html::input('text', 'address', Yii::$app->request->get('address', ''), ['class' => 'form-control']) ?>
						</div>

						<div class="form-group has-success col-md-4">
							<label>Метка</label>
							<?= Html::input('text', 'label', Yii::$app->request->get('label', ''), ['class' => 'form-control']) ?>
						</div>

						<?= Html::input('hidden', 'only', Yii::$app->request->get('only', '')) ?>

						<div class="form-group col-md-1">
							<?= Html::submitButton('Искать', ['class' => 'btn btn-success btn-flat']) ?>
						</div>
						<div class="form-group col-md-6">
							<div class="btn-group">
								<?= Html::button('Все', ['class' => 'btn btn-primary btn-flat btn-only', 'data-only' => '']) ?>
								<?= Html::button('Получение', ['class' => 'btn btn-primary btn-flat btn-only', 'data-only' => 'receive']) ?>
								<?= Html::button('Отправка', ['class' => 'btn btn-primary btn-flat btn-only', 'data-only' => 'send']) ?>
								<?= Html::button('Сдача', ['class' => 'btn btn-primary btn-flat btn-only', 'data-only' => 'change']) ?>
							</div>
						</div>

						<?= Html::endForm() ?>
					</div>

					<?= GridView::widget([
						'dataProvider' => $provider,
						'summary' => '',
						'rowOptions' => function ($model) use ($addressSmallChange, $addressBigChange) {
							$color = (($model->category == 'receive') ? 'rgba(125, 226, 234, .5)' : 'rgba(232, 163, 158, .5)');
							
							if ($addressSmallChange == $model->address) {
								$color = '#eee';
							}
							if ($addressBigChange == $model->address) {
								$color = '#eee';
							}

							return ['style' => 'background: ' . $color];
						},
						'columns' => [
							//['class' => 'yii\grid\SerialColumn'],
							[
								'attribute' => 'time',
								'format' => 'raw',
								'label' => 'Время',
								'value' => function ($model) {
									$html = '<span title="' . Yii::$app->formatter->asDateTime($model->time) . '">' . Yii::$app->formatter->asRelativeTime($model->time) . '</span>';
									$html .= '<br />';
									$html .= ($model->category == 'receive') ? '<span class="label label-primary">Получение</span>' : '<span class="label label-warning">Отправка</span>';
									return $html;
								}
							],
							[
								'attribute' => 'address',
								'label' => 'Адрес и TXID',
								'format' => 'raw',
								'value' => function ($model) {
									$address = $model->address;
									$txid = $model->txid;
									if ($addr = Yii::$app->request->get('address')) {
										$address = str_ireplace($addr, '<span class="label label-success">'.$addr.'</span>', $address);
									}
									if ($tx = Yii::$app->request->get('txid')) {
										$txid = str_ireplace($tx, '<span class="label label-success">'.$tx.'</span>', $txid);
									}

									$html = $address;
									$html .= '<br />';
									$html .= '<small>'.$txid.'</small>';
									return $html;
								}
							],
							[
								'attribute' => 'confirmations',
								'label' => 'Подтверждения',
								'value' => function ($model) {
									return $model->confirmations;
								}
							],
							[
								'attribute' => 'amount',
								'label' => 'Сумма',
								'value' => function ($model) {
									return number_format($model->amount, 8);
								}
							],
							[
								'attribute' => 'label',
								'label' => 'Метка',
								'format' => 'raw',
								'value' => function ($model) {
									$label = $model->label;
									if ($lb = Yii::$app->request->get('label')) {
										$label = str_ireplace($lb, '<span class="label label-success">'.$lb.'</span>', $label);
									}
									return $label;
								}
							],
							[
								'attribute' => 'fee',
								'label' => 'Комиссия',
								'format' => 'raw',
								'value' => function ($model) use ($txs) {
									if ($model->raw !== null and $model->raw instanceof \stdClass) {
										if (isset($txs[$model->txid])) {
											$transaction = $txs[$model->txid];
										} else {
											$transaction = new Transaction($model->raw);
										}
										$fee = $transaction->getFee();
										if (isset($fee['fee'])) {
											$html = number_format($fee['fee'], 8);
											$html .= '<br />';
											$html .= '<strong>' . number_format((($fee['fee'] * 100000000) / $transaction->getSize()), 2) . ' сатоши/байт</strong>';
											$html .= '<br />';
											$html .= 'Размер: ' . $transaction->getSize();
										} else {
											$html .= isset($model->fee) ? $model->fee : '';
										}
										return $html;
									} else {
										return isset($model->fee) ? number_format($model->fee, 8) : '';
									}
								}
							],
							// 'created_at',
							// 'updated_at',
				 
							[
								'class' => 'yii\grid\ActionColumn',
								'template' => '{view} {viewInExplorer}',
								'buttons' => [
									'view' => function ($url, $model) {
										return Html::a(
											'<span class="glyphicon glyphicon-eye-open"></span>', 
											'/transaction/'.$model->txid,
											[
												'data' => [
													'pjax' => 0
												],
												'aria' => [
													'label' => 'Просмотр',
												],
												'title' => 'Просмотр',
											]
										);
									},
									'viewInExplorer' => function ($url, $model) {
										return Html::a(
											'<span class="glyphicon glyphicon glyphicon-star"></span>', 
											'https://blockchain.info/tx/'.$model->txid,
											[
												'data' => [
													'pjax' => 0
												],
												'aria' => [
													'label' => 'Просмотр на внешнем сайте',
												],
												'title' => 'Просмотр на внешнем сайте',
												'target' => '_blank'
											]
										);
									}
								],
							],
						],
					]); ?>

					<?php  // пагинация
						echo LinkPager::widget([
							'pagination' => $pagination,
						]);
					?>

				<?php Pjax::end() ?>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<!-- <div class="col-md-6">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Курсы</h3>
					<div class="right">
						<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up lnr-chevron-down"></i></button>
						<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
					</div>
				</div>
				<div class="panel-body" style="display: none;">
				<?php //foreach ($rates as $currency => $rate) : ?>
					<div class="col-md-<?php //echo $col ?>" style="padding: 10px">
						BTC/<?php //echo $currency ?>
						<br />
						<?php //echo $rate->last ?> <?php //echo $rate->symbol ?>
					</div>
				<?php //endforeach; ?>
				</div>
			</div>
		</div> -->

		<!-- <div class="col-md-6">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Калькулятор</h3>
					<div class="right">
						<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up lnr-chevron-down"></i></button>
						<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
					</div>
				</div>
				<div class="panel-body" style="display: none;" id="calculate_rates">
					<div class="form-group col-md-3 has-success">
						<label>
							Валюта
							<select id="currency" class="form-control">
							<?php //foreach ($availableCurrencies as $currency) : ?>
								<option value="<?php //echo $currency ?>"><?php //echo $currency ?></option>
							<?php //endforeach; ?>
							</select>
						</label>
					</div>
					<div class="form-group col-md-9 has-success">
						<label>
							Сумма
							<input type="text" id="amount" class="form-control">
						</label>
					</div>
					<div class="form-group col-md-12 has-success">
						<label>
							BTC
							<span class="label label-success" style="font-size: 13px" id="inBTC"></span>
						</label>
					</div>
				</div>
			</div>
		</div> -->

	</div>
</div>
