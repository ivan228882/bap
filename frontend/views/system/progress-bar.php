<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
?>
<?php Pjax::begin(['id' => $id, 'enablePushState' => false]) ?>
	<div class="progress">
		<div class="progress-bar" role="progressbar" aria-valuenow="<?=$percent?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$percent?>%;">
			<?=$percent?>%
		</div>
	</div>
	<?= Html::a('Обновить', ['system/cpu-load'], ['class' => 'btn btn-success btn-flat', 'id' => 'cpu_load_link']) ?>
<?php Pjax::end() ?>