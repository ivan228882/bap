<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;

class SocketController extends Controller
{
	
	public $message;
	
	public function options($actionID)
	{
		return ['message'];
	}
	
	public function optionAliases()
	{
		return [
			'm' => 'message',
		];
	}
	
	public function actionIndex()
	{
		echo $this->message . "\n";
	}

	public function actionNewBlock()
	{
		Yii::$app->socketEmitter->emit('block-event', ['message' => $this->message]);
		echo 'New Block Event. Message: ' . $this->stdout($this->message, Console::BOLD) . "\n";
	}

	public function actionNewWallet()
	{
		Yii::$app->socketEmitter->emit('wallet-event', ['message' => $this->message]);
		echo 'New Wallet Event. Message: ' . $this->stdout($this->message, Console::BOLD) . "\n";
	}

}