<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;

class SyncController extends Controller
{
	
	public function actionIndex()
	{
		echo "Welcome to Sync controller!\n";
	}

	public function actionOrders()
	{
		$db = Yii::$app->db;
		$db2 = Yii::$app->db2;

		$maxPaymentRecordId = $db->createCommand("SELECT `record_id` FROM `orders` WHERE `type` = 'payment' ORDER BY `record_id` DESC LIMIT 1")->queryScalar() ?: 0;
		$maxSendRecordId = $db->createCommand("SELECT `record_id` FROM `orders` WHERE `type` = 'send' ORDER BY `record_id` DESC LIMIT 1")->queryScalar() ?: 0;

		echo 'maxPaymentRecordId: ' . $maxPaymentRecordId . PHP_EOL;
		echo 'maxSendRecordId: ' . $maxSendRecordId . PHP_EOL;

		$payments = $db2
					->createCommand('SELECT * FROM `bitcoin_payments`WHERE `id` > :id')
					->bindValue(':id', $maxPaymentRecordId)
					->queryAll();

		echo 'payments: ' . count($payments) . PHP_EOL;

		if (!empty($payments)) {
			foreach ($payments as $payment) {
				if (empty($payment['txid'])) {
					echo $payment['txid'];
					continue;
				}

				$order = $db2
							->createCommand('SELECT * FROM `orders` WHERE `id` = :id')
							->bindValue(':id', $payment['order_id'])
							->queryOne();

				if ($order) {
					$row = [
						'order_ids' => $payment['order_id'],
						'order_codes' => $order['code'],
						'txid' => $payment['txid'],
						'record_id' => $payment['id'],
						'type' => 'payment',
					];

					$db->createCommand()->insert('orders', $row)->execute();
				}
			}
		} else {
			echo 'payments empty' . PHP_EOL;
		}

		$sendings = $db2
					->createCommand('SELECT * FROM `bitcoin_sendings` WHERE `id` > :id')
					->bindValue(':id', $maxSendRecordId)
					->queryAll();

		echo 'sendings: ' . count($sendings) . PHP_EOL;

		if (!empty($sendings)) {
			foreach ($sendings as $sending) {
				if (empty($sending['txid'])) {
					continue;
				}

				$orders = $db2
						->createCommand('SELECT * FROM `bitcoin_sending_orders` LEFT JOIN `orders` ON `bitcoin_sending_orders`.`order_id` = `orders`.`id` WHERE `bitcoin_sending_orders`.`bitcoin_sending_id` = :id')
						->bindValue(':id', $sending['id'])
						->queryAll();

				if (!empty($orders)) {
					$order_ids = [];
					$order_codes = [];
					foreach ($orders as $order) {
						$order_ids[] = $order['id'];
						$order_codes[] = $order['code'];
					}

					$row = [
						'order_ids' => implode(',', $order_ids),
						'order_codes' => implode(',', $order_codes),
						'txid' => $sending['txid'],
						'record_id' => $sending['id'],
						'type' => 'send',
					];

					$db->createCommand()->insert('orders', $row)->execute();
				}
				
			}
		} else {
			echo 'sendings empty' . PHP_EOL;
		}
	}

}