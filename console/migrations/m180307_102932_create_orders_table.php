<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m180307_102932_create_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey()->notNull()->unsigned(),
            'order_ids' => $this->string(255)->notNull(),
            'order_codes' => $this->string(255)->notNull(),
            'txid' => $this->string(255)->notNull(),
            'record_id' => $this->integer(11)->notNull()->unsigned(),
            'type' => "ENUM('send', 'payment')",
        ]);

        $this->addCommentOnTable('orders', 'Заявки');

        // creates index for column `record_id`
        $this->createIndex(
            'idx-orders-record_id',
            'orders',
            'record_id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops index for column `record_id`
        $this->dropIndex(
            'idx-orders-record_id',
            'orders'
        );

        $this->dropTable('orders');
    }
}
