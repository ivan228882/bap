<?php

use yii\db\Migration;

class m171208_202936_insert_settings extends Migration
{
	public function safeUp()
	{
		$this->insert('settings', [
			'name' => 'fee',
			'value' => '0.0001',
			'active' => 1,
			'is_system' => 1,
			'created_at' => time(),
			'updated_at' => time(),
		]);
		$this->insert('settings', [
			'name' => 'fee_satoshi',
			'value' => '20',
			'active' => 1,
			'is_system' => 1,
			'created_at' => time(),
			'updated_at' => time(),
		]);
		$this->insert('settings', [
			'name' => 'limit_small_change',
			'value' => '0.005',
			'active' => 1,
			'is_system' => 1,
			'created_at' => time(),
			'updated_at' => time(),
		]);
		$this->insert('settings', [
			'name' => 'address_small_change',
			'value' => '',
			'active' => 1,
			'is_system' => 1,
			'created_at' => time(),
			'updated_at' => time(),
		]);
		$this->insert('settings', [
			'name' => 'address_big_change',
			'value' => '',
			'active' => 1,
			'is_system' => 1,
			'created_at' => time(),
			'updated_at' => time(),
		]);
	}

	public function safeDown()
	{
		echo "m171208_202936_insert_settings cannot be reverted.\n";

		return false;
	}

	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m171208_202936_insert_settings cannot be reverted.\n";

		return false;
	}
	*/
}
