<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wallets`.
 */
class m171129_165720_create_wallets_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('wallets', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('wallets');
    }
}
