<?php

use yii\db\Migration;

class m171208_202936_insert_users extends Migration
{
	public function safeUp()
	{
		$this->insert('user', [
			'username' => 'admin',
			'auth_key' => 'admin',
			'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
			'email' => 'admin@7money.co',
			'status' => '10',
			'created_at' => time(),
			'updated_at' => time(),
		]);
	}

	public function safeDown()
	{
		echo "m171208_202936_insert_users cannot be reverted.\n";

		return false;
	}

	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m171208_202936_insert_settings cannot be reverted.\n";

		return false;
	}
	*/
}
