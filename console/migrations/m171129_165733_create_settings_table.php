<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m171129_165733_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'value' => $this->text()->notNull(),
            'active' => $this->boolean()->defaultValue(1),
            'is_system' => $this->boolean()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings');
    }
}
