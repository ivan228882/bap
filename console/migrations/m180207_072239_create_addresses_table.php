<?php

use yii\db\Migration;

/**
 * Handles the creation of table `address`.
 */
class m180207_072239_create_addresses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('addresses', [
            'id' => $this->primaryKey(),
            'address' => $this->string(),
            'created_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('addresses');
    }
}
